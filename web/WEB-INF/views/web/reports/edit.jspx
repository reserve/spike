<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="2.0"
          xmlns="http://www.w3.org/1999/xhtml"
          xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:tiles="http://tiles.apache.org/tags-tiles"
          xmlns:form="http://www.springframework.org/tags/form"
          xmlns:spring="http://www.springframework.org/tags">
    <jsp:output omit-xml-declaration="yes"/>

    <spring:url value="/images/delete.png" var="delete_png_url"/>

    <div class="ui_widget borderlessPanel">
        <spring:url value="/web/reports/manage" var="manage_reports_url"/>
        <form:form id="headerData" method="post" modelAttribute="reportEditView" action="${manage_reports_url}">
            <form:errors path="*" cssClass="errorBox" element="div"/>
            <div class="ui-widget-header ui-corner-top">
                <h1>Report Properties</h1>
            </div>
            <div class="ui-widget-content ui-corner-bottom">
                <div class="headerDetails">
                    <div class="field">
                        <span class="label">
                            <form:label path="name" for="name" cssErrorClass="error">
                                <spring:message code="spike.label.edit.report"/>
                            </form:label>
                        </span>
                        <span class="input">
                            <form:input path="name"/>
                            <form:errors path="name" cssClass="error"/>
                        </span>
                    </div>
                    <div class="field">
                        <span class="label">
                            <form:label path="dateRange" for="dateRange" cssErrorClass="error">
                                ${reportEditView.dateRangeFieldName}
                            </form:label>
                        </span>
                        <span class="input">
                            <form:select path="dateRange">
                                <form:options items="${reportEditView.allRanges}"/>
                            </form:select>
                        </span>
                    </div>
                    <div class="field">
                        <span class="label">
                            <form:label path="startDate" for="startDate" cssErrorClass="error">
                                <spring:message code="spike.label.edit.start"/>
                            </form:label>
                        </span>
                        <span class="input">
                            <form:input path="startDate"/>
                            <form:errors path="startDate" cssClass="error"/>
                        </span>
                    </div>
                    <div class="field">
                        <span class="label">
                            <form:label path="endDate" for="endDate" cssErrorClass="error">
                                <spring:message code="spike.label.edit.end"/>
                            </form:label>
                        </span>
                        <span class="input">
                            <form:input path="endDate"/>
                            <form:errors path="endDate" cssClass="error"/>
                            <form:hidden path="columnIDs" value="${reportEditView.columnIDs}"/>
                        </span>
                    </div>
                </div>
                <div class="submitButtons">
                    <spring:message code="spike.buttons.save.report" var="saveLabel"/>
                    <input type="submit" id="saveReportButton" name="saveReportButton" value="${saveLabel}" class="button"/>
                </div>
            </div>
        </form:form>
    </div>

    <div class="ui_widget borderlessPanel splitViewAvailable">
        <div class="ui-widget-header ui-corner-top">
            <h1>Available Fields</h1>
        </div>
        <div class="ui-widget-content ui-corner-bottom" id="availableFields">
            <c:forEach var="fieldGroup" items="${reportEditView.availableFieldGroups}">
                <h2><a href="#"><c:out value="${fieldGroup.name}"/></a></h2>
                <div>
                    <c:forEach var="reportField" items="${fieldGroup.availableFields}">
                        <div id="${reportField.fieldID}" class="availableField ${reportField.availableColumn == 'true' ? 'potentialColumn' : ''} ${reportField.availableFilter == 'true' ? 'potentialFilter' : ''} ">
                            <p><c:out value="${reportField.name}"/></p>
                        </div>
                    </c:forEach>
                </div>
            </c:forEach>
        </div>
    </div>

    <div class="ui_widget borderlessPanel splitViewSelected">
        <div class="ui-widget-header ui-corner-top">
            <h1>Selected Columns</h1>
        </div>
        <div class="ui-widget-content ui-corner-bottom" id="selectedColumns">
            <c:forEach var="reportColumn" items="${reportEditView.columns}">
                <div id="col-${reportColumn.fieldID}" class="selectedColumn">
                    <p><a href="#"><img src="${delete_png_url}"/></a><c:out value="${reportColumn.name}"/></p>
                </div>
            </c:forEach>

        </div>
    </div>

    <div class="ui_widget borderlessPanel splitViewFilters">
        <div class="ui-widget-header ui-corner-top">
            <h1>Filters</h1>
        </div>
        <div class="ui-widget-content ui-corner-bottom" id="selectedFilters">
            <table id="filtersTable">
            <thead>
            <tr>
                <th><spring:message code="spike.label.filter.name"/></th>
                <th><spring:message code="spike.label.filter.comparator"/></th>
                <th><spring:message code="spike.label.filter.value"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="reportFilter" items="${reportEditView.filters}">
                <tr>
                    <td><c:out value="${reportFilter.name}"/></td>
                    <td><c:out value="${reportFilter.operation}"/></td>
                    <td><c:out value="${reportFilter.filterValue}"/></td>
                </tr>
            </c:forEach>
            </tbody>
            </table>
        </div>
    </div>

    <spring:url value="/web/reports/dates" var="dates_for_range_url"/>
    <spring:message code="spike.label.edit.custom.range" var="custom_range"/>

    <script type="text/javascript">
        <![CDATA[
        $( document ).ready( function () {
            $( "#saveReportButton" ).button().click( function() {
                $( "#columnIDs" ).val( $( "#selectedColumns" ).sortable( "toArray" ) );
            });

            var pickerOpts = {
                showButtonPanel: true,
                showAnim: "slideDown",
                changeMonth: true,
                changeYear: true,
                onSelect: function ( selectedDate ) {
                    $( "#dateRange" ).val( '${custom_range}' );
                }
            };
            $.datepicker.setDefaults( pickerOpts );

            var startOpts = {
                onClose: function ( selectedDate ) {
                    $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
                }

            };
            var endOpts = {
                onClose: function ( selectedDate ) {
                    $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
                }

            };
            $( "#startDate" ).datepicker( startOpts );
            $( "#endDate" ).datepicker( endOpts );

            $( "#dateRange" ).change(
                function () {
                    $.getJSON( '${dates_for_range_url}', {
                        range: $( this ).val(),
                        ajax: 'true'
                    }, function ( data ) {
                        $( "#startDate" ).val( data[0] );
                        $( "#endDate" ).val( data[1] );
                        var start = $.datepicker.parseDate( "mm/dd/yy", data[0] );
                        var end = $.datepicker.parseDate( "mm/dd/yy", data[1] );
                    } );
                } );

            $( "#availableFields" ).accordion();

            var colDropOpts = {
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: function( draggable ) {
                    if (!draggable.hasClass("potentialColumn")) {
                        return false;
                    };
                    return $(this).find("#col-" + draggable.attr("id")).length == 0;
                },
                drop: function ( event, ui ) {
                    var newDiv = $( "<div />" ).attr( 'id', "col-" + ui.draggable.attr("id") ).addClass( "selectedColumn" );
                    var newP = $( "<p />" ).text( ui.draggable.text() );
                    var newA = $( "<a />" ).attr( 'href', '#' );
                    var newImg = $( "<img />" ).attr( 'src', '${delete_png_url}' );
                    newA.append( newImg );
                    newP.prepend( newA );
                    newDiv.append( newP );
                    newDiv.appendTo( this );
                }
            };
            $( "#selectedColumns" ).droppable( colDropOpts ).sortable();

            var availDragOpts = {
                revert: "invalid",
                appendTo: "body",
                helper: "clone"
            };
            $( ".availableField" ).draggable( availDragOpts );

            $( "#selectedColumns" ).on( "click", ".selectedColumn img", function() {
                $(this).parent().parent().parent().remove();
            });

        } );
        ]]>
    </script>

</jsp:root>