package efi.platform.dao;

import efi.platform.domain.BaseEntity;

import java.util.List;

/**
 * DAO stands for Data Access Object.
 * This DAO provides the basic DAO services that all DAOs will use.
 */
public interface GenericDao {

    public void flushEntities();

    public void deleteEntity( BaseEntity entity );
    public void evictEntity( BaseEntity entity );
    public void saveEntity( BaseEntity entity );

    public <T extends BaseEntity> void removeEntity( Class<T> classObject, Long id );

    public <T extends BaseEntity> T getEntity( Class<T> classObject, Long id );
    public <T extends BaseEntity> T refreshEntity( T entity );
    public <T extends BaseEntity> T refreshEntity( T entity, boolean flushBeforeGet );

    public <T extends BaseEntity> List<T> loadAllEntities( Class<T> classObject );
}
