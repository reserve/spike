package efi.platform.dao.impl;

import efi.platform.core.DevelopmentException;
import efi.platform.dao.GenericDao;
import efi.platform.domain.BaseEntity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is the base class for all DAOs.
 */
public class GenericDaoImpl extends HibernateDaoSupport implements GenericDao {

    protected final Log log = LogFactory.getLog( getClass() );

    public void flushEntities() {
        getHibernateTemplate().flush();
    }

    public void deleteEntity( BaseEntity entity ) {
        getHibernateTemplate().delete( entity );
    }

    public void evictEntity( BaseEntity entity ) {
        getHibernateTemplate().evict( entity );
    }

    public void saveEntity( BaseEntity entity ) {
        getHibernateTemplate().saveOrUpdate( entity );
    }

    public <T extends BaseEntity> void removeEntity( Class<T> classObject, Long id ) {
        getHibernateTemplate().delete( getEntity( classObject, id ) );
    }

    public <T extends BaseEntity> T getEntity( Class<T> classObject, Long id ) {
        return classObject.cast( getHibernateTemplate().get( classObject, id ) );
    }

    public <T extends BaseEntity> T refreshEntity( T entity ) {
        return refreshEntity( entity, true );
    }

    public <T extends BaseEntity> T refreshEntity( T entity, boolean flushBeforeGet ) {
        Class<T> entityClass = getEntityClassObject( entity );
        Long entityId = entity.getId();

        if ( flushBeforeGet ) {
            flushEntities();
        }

        getHibernateTemplate().refresh( entity );
        return getHibernateTemplate().get( entityClass, entityId );
    }

    @SuppressWarnings( "unchecked" )
    public <T extends BaseEntity> List<T> loadAllEntities( Class<T> classObject ) {
        return getHibernateTemplate().loadAll( classObject );
    }

    // Should probably return T, since we should only get back a BaseEntity.
    protected Object findEntity( String queryName, Object... parameters ) {
        List entities = findEntities( queryName, parameters );
        if ( entities == null || entities.isEmpty() ) {
            return null;
        }
        if ( entities.size() > 1 ) {
            log.error( "More than one record returned for query [" + queryName + "]; parameters: " +
                       Arrays.asList( parameters ) );
            throw new DevelopmentException( "More than one record returned for query [" + queryName + "]" );
        }
        return entities.get( 0 );
    }

    protected List findEntities( String queryName, Object... parameters ) {
        if ( parameters == null || parameters.length == 0 ) {
            return getHibernateTemplate().findByNamedQuery( queryName );
        }
        if ( parameters.length % 2 != 0 ) {
            log.error( "Unmatched query parameters for query [" + queryName + "]; parameters: " +
                       Arrays.asList( parameters ) );
        }
        String[] paramNames = new String[parameters.length / 2];
        Object[] values = new Object[parameters.length / 2];
        for ( int i = 0; i < parameters.length; i += 2 ) {
            int parameter = ( i + 1 ) / 2;
            paramNames[parameter] = (String) parameters[i];
            values[parameter] = parameters[i + 1];
        }
        return getHibernateTemplate().findByNamedQueryAndNamedParam( queryName, paramNames, values );
    }

    protected <T> List<T> findEntities( Class<T> classObject, String queryName, Object... parameters ) {
        List<T> results = new ArrayList<T>();
        results.addAll( findEntities( queryName, parameters ) );
        return results;
    }

    // Handle proxy objects.
    @SuppressWarnings( "unchecked" )
    private <T extends BaseEntity> Class<T> getEntityClassObject( T entity ) {
        String entityClassName = entity.getClass().getName();
        if ( entityClassName.contains( "javassist" ) || entityClassName.contains( "CGLIB" ) ) {
            return (Class<T>) entity.getClass().getSuperclass();
        }
        return (Class<T>) entity.getClass();
    }
}
