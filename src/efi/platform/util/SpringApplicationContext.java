package efi.platform.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

/**
 * This class is used to get a reference to the Spring Application Context from classes that aren't Spring-enabled.
 * For example, a non-Spring web service class or a JMX bean can access a bean using the getBean() method.
 */
@Configuration
public class SpringApplicationContext implements ApplicationContextAware {
    
    private static ApplicationContext APPLICATION_CONTEXT;

    @Override
    public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException {
        APPLICATION_CONTEXT = applicationContext;
    }

    /**
     * Similar to context.getBean("someBeanName"), except this class has its own handle to the context.
     * The caller must cast the returned object to something appropriate.
     * If the bean does not exist, throws a BeansException.
     * @param name the name of the bean to retrieve.
     * @return an Object reference to the named bean.
     */
    public static Object getBean( String name ) {
        return APPLICATION_CONTEXT.getBean( name );
    }
}
