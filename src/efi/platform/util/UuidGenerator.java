package efi.platform.util;

import java.util.UUID;

/**
 * This class is used to generate a UUID.
 */
public final class UuidGenerator {

    private UuidGenerator() {}

    public static String generate() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
