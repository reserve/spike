package efi.platform.core;

/**
 * This exception can be thrown when detecting an error condition that should be caught during the development process.
 * Since it is a runtime exception, it does not need to be declared in the method signature.
 */
public class DevelopmentException extends RuntimeException {

    public DevelopmentException() {
        super();
    }

    public DevelopmentException( String message ) {
        super( message );
    }

    public DevelopmentException( String message, Throwable cause ) {
        super( message, cause );
    }

    public DevelopmentException( Throwable cause ) {
        super( cause );
    }
}
