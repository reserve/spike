@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "MAIN_SEQ")})

package efi.platform.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;