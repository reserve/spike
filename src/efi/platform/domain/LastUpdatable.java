package efi.platform.domain;

import org.joda.time.DateTime;

/**
 * This interface is used with a Hibernate listener to update the last modified time on an entity.
 */
public interface LastUpdatable {

    public static final String LAST_UPDATED_PROPERTY_NAME = "lastUpdated";

    void setLastUpdated( DateTime dateTime );
}
