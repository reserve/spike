package efi.platform.domain;

import efi.platform.util.UuidGenerator;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * BaseObject is the root of the domain object model.
 * It defines a special unique ID that is used for object comparison.  Subclasses do not need to implement their own
 * equals() and hashCode() methods.  They probably should implement their own toString() method, though.
 */
@MappedSuperclass
public abstract class BaseEntity implements LastUpdatable, Serializable {
    
    private Long id;
    private int version = 0;
    private DateTime lastUpdated;
    private String uniqueId = UuidGenerator.generate();

    @Id
    @GeneratedValue( generator = "SequenceGenerator" )
    @Column( name = "ID", nullable = false, updatable = false )
    public Long getId() {
        return id;
    }

    public void setId( Long id ) {
        this.id = id;
    }

    @Version
    @Column( name = "VERSION" )
    public int getVersion() {
        return version;
    }

    public void setVersion( int version ) {
        this.version = version;
    }

    @Column( name = "LAST_UPDATED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated( DateTime lastUpdated ) {
        this.lastUpdated = lastUpdated;
    }

    @Column( name = "UNIQUE_ID", length = 64, nullable = false, updatable = false )
    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId( String uniqueId ) {
        this.uniqueId = uniqueId;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( o == null || !( o instanceof BaseEntity ) ) return false;

        BaseEntity that = (BaseEntity) o;

        if ( uniqueId != null ? !uniqueId.equals( that.uniqueId ) : that.uniqueId != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return uniqueId != null ? uniqueId.hashCode() : super.hashCode();
    }

    @Override
    public String toString() {
        return "BaseEntity {" +
               "id=" + id +
               ", lastUpdated=" + lastUpdated +
               ", uniqueId='" + uniqueId + '\'' +
               ", version=" + version +
               '}';
    }
}
