package efi.platform.service;

import org.springframework.validation.Errors;

import java.util.List;

/**
 * This interface defines the service layer for interacting with the Spring Error API.
 */
public interface ErrorMessageService {

    List<String> getDisplayableErrors( Errors errors );
}
