package efi.platform.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.domain.BaseEntity;
import efi.platform.service.GenericService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * This class is the base class for all Services.
 * It provides a logging instance and requires subclasses to define the default DAO instance.
 */
public abstract class GenericServiceImpl implements GenericService {

    protected final Log log = LogFactory.getLog( getClass() );

    public abstract GenericDao getDefaultDao();

    @Override
    public <T extends BaseEntity> T getEntity( Class<T> classObject, Long id ) {
        return getDefaultDao().getEntity( classObject, id );
    }

    @Override
    public <T extends BaseEntity> List<T> loadAllEntities( Class<T> classObject ) {
        return getDefaultDao().loadAllEntities( classObject );
    }
}
