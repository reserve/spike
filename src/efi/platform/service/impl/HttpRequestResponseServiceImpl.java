package efi.platform.service.impl;

import efi.platform.service.HttpRequestResponseService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.cert.Certificate;
import java.text.MessageFormat;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

/**
 * This class implements the request/response HTTP service API.
 */
@Service
public class HttpRequestResponseServiceImpl implements HttpRequestResponseService {

    private static final Logger LOGGER = Logger.getLogger( HttpRequestResponseServiceImpl.class.getName() );

    private static final int CONNECT_TIMEOUT_MILLIS = 15000; // 15 seconds.
    private static final int READ_TIMEOUT_MILLIS = 180000; // 3 minutes.

    /**
     * The error message to use for a problem communicating to a generic server.
     */
    private static final MessageFormat generalConnectionProblem =
            new MessageFormat( "Failed to post the request to the server: [{0}]" );

    /**
     * The error message to use for a timed-out.
     */
    private static final MessageFormat timeoutProblem =
            new MessageFormat( "The request or response timed out on server: [{0}]" );

    /**
     * The error message to use for no certificate found.
     */
    private static final MessageFormat noCertProblem =
            new MessageFormat( "No certificates available to verify hostname: [{0}]" );

    /**
     * The error message to use for an invalid certificate.
     */
    private static final MessageFormat badCertProblem =
            new MessageFormat( "Invalid certificate for hostname: [{0}]; certificate info: [{1}]" );

    @Override
    public String postXmlRequest( String xmlRequest, String requestUrl ) {
        return postXmlRequest( xmlRequest, requestUrl, false );
    }

    @Override
    public String postXmlRequest( String xmlRequest, String requestUrl, boolean verifyCertificate ) {
            String response = "";
            HttpURLConnection connection = null;
            BufferedReader in = null;

            try {
                URL url = new URL( requestUrl );
                connection = (HttpURLConnection) url.openConnection();

                // Set some timeout values.
                connection.setConnectTimeout( CONNECT_TIMEOUT_MILLIS );
                connection.setReadTimeout( READ_TIMEOUT_MILLIS );

                // Add verification for SSL connections.
                if ( verifyCertificate && requestUrl.startsWith( "https" ) ) {
                    ( (HttpsURLConnection) connection ).setHostnameVerifier( getHostnameVerifier() );
                }

                // We want to POST our data.
                connection.setRequestMethod( "POST" );

                // We want to send and receive data.
                connection.setDoInput( true );
                connection.setDoOutput( true );

                // Make sure this URL is not a cached one.
                connection.setUseCaches( false );

                // Specify the content type and length.
                connection.setRequestProperty( "Content-Type", "text/xml" );
                connection.setRequestProperty( "Content-Length", Integer.toString( xmlRequest.length() ) );

                // Write the data to the output stream.
                OutputStreamWriter writer = new OutputStreamWriter( connection.getOutputStream(), "UTF-8" );
                writer.write( xmlRequest );
                writer.flush();
                writer.close();

                // Get the response.
                in = new BufferedReader( new InputStreamReader( connection.getInputStream() ) );
                StringBuilder buffer = new StringBuilder();
                String line;
                while ( ( line = in.readLine() ) != null ) {
                    buffer.append( line );
                }
                response = buffer.toString();
            }
            catch ( SocketTimeoutException ste ) {
                LOGGER.warn( timeoutProblem.format( new Object[] { requestUrl } ) );
            }
            catch ( Exception e ) {
                LOGGER.error( generalConnectionProblem.format( new Object[] { requestUrl } ), e );
            }
            finally {
                if ( in != null ) {
                    try {
                        in.close();
                    }
                    catch ( IOException e ) { /*Ignore*/}
                }
                if ( connection != null ) {
                    connection.disconnect();
                }
            }
            return response;
        }

    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            // Verify that the hostname is acceptable.
            public boolean verify( String hostName, SSLSession sslSession ) {
                try {
                    Certificate[] certs = sslSession.getPeerCertificates();
                    if ( certs.length == 0 ) {
                        LOGGER.error( noCertProblem.format( new Object[] { hostName } ) );
                        return false;
                    }
                    String certInfo = certs[0].toString();
                    if ( certInfo.indexOf( "Efficient Frontiers, Inc." ) < 0 ) {
                        LOGGER.error( badCertProblem.format( new Object[] { hostName, certInfo } ) );
                        return false;
                    }
                }
                catch ( SSLPeerUnverifiedException e ) {
                    LOGGER.error( "Unverified hostname: [" + hostName + "]", e );
                    return false;
                }
                return true;
            }
        };
    }
}
