package efi.platform.service.impl;

import efi.platform.service.ErrorMessageService;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

/**
 * This class implements the error message service API.
 */
@Service
public class ErrorMessageServiceImpl implements ErrorMessageService {
    
    @Resource
    MessageSource messageSource;

    @Override
    public List<String> getDisplayableErrors( Errors errors ) {
        List<String> errorList = new ArrayList<String>( errors.getErrorCount() );
        for ( ObjectError objectError : errors.getAllErrors() ) {
            String message = messageSource.getMessage( objectError, null );
            if ( objectError instanceof FieldError ) {
                FieldError fieldError = (FieldError) objectError;
                errorList.add( "Field error on field '" + fieldError.getField() + "': " + message );
            }
            else {
                errorList.add( "Error in object '" + objectError.getObjectName() + "': " + message );
            }
        }
        return errorList;
    }
}
