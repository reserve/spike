package efi.platform.web.mapping;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 * This class is used to customize the Jackson object mapper to write dates out as strings.
 */
public class CustomJacksonObjectMapper extends ObjectMapper {

    public CustomJacksonObjectMapper() {
        super.configure( SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false );
        this.toString();
    }
}
