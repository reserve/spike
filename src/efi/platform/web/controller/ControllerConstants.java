package efi.platform.web.controller;

/**
 * This class is used to hold constants that are used by the Spring MVC layer.
 */
public final class ControllerConstants {

	public static final String REDIRECT = "redirect:/";
}
