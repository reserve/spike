package efi.platform.web.controller;

import efi.platform.util.UuidGenerator;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.util.ClassUtils;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to override the default exception handler resolver in Spring.
 * This way it can be used for both exceptions that get thrown and for handling HTTP error codes.
 */
@Component( DispatcherServlet.HANDLER_EXCEPTION_RESOLVER_BEAN_NAME )
public class ApplicationExceptionResolver extends AbstractHandlerExceptionResolver {

        private static final Logger LOGGER = Logger.getLogger( ApplicationExceptionResolver.class.getName() );

        @Override
        protected ModelAndView doResolveException( HttpServletRequest httpServletRequest,
                                                   HttpServletResponse httpServletResponse,
                                                   Object handler,
                                                   Exception e ) {
            String pathInfo = httpServletRequest.getPathInfo();
            return resolve( pathInfo, e );
        }

        public ModelAndView resolve( String pathInfo, Throwable throwable ) {
            String generatedIdentifier = UuidGenerator.generate();
            LOGGER.error( "Path [" + pathInfo + "], Uncaught exception identified by: " + generatedIdentifier, throwable );
            return createModelAndView( "exception.default", throwable, generatedIdentifier );
        }

        private ModelAndView createModelAndView( String messageKey, Throwable throwable, String generatedIdentifier ) {
            ModelMap model = new ModelMap();
            WorkflowErrors errors = new WorkflowErrors( model );
            errors.reject( messageKey, new Object[] { generatedIdentifier }, null );
            model.addAttribute( "exceptionClass", ClassUtils.getShortName( throwable.getClass() ) );
            model.addAttribute( "exceptionMessage", throwable.getMessage() );
            StringWriter writer = new StringWriter();
            throwable.printStackTrace( new PrintWriter( writer ) );
            model.addAttribute( "exceptionStacktrace", writer.toString() );
            return new ModelAndView( "error", model );
        }
    }
