package efi.platform.web.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
public class HttpErrorController {

    private static final Logger LOGGER = Logger.getLogger( ApplicationExceptionResolver.class.getName() );

    @Resource( name = DispatcherServlet.HANDLER_EXCEPTION_RESOLVER_BEAN_NAME )
    ApplicationExceptionResolver resolver;

    @RequestMapping( value = "/errors/handle404" )
    public ModelAndView handle404() {
        ModelMap model = new ModelMap();
        WorkflowErrors errors = new WorkflowErrors( model );
        errors.reject( "notFound", "This page does not exist." );
        return new ModelAndView( "error", model );
    }

    @RequestMapping( value = "/errors/handle500" )
    public ModelAndView handle500( WebRequest webRequest, HttpServletRequest  httpServletRequest ) {
        Throwable throwable = (Throwable) webRequest.getAttribute( WebUtils.ERROR_EXCEPTION_ATTRIBUTE,
                                                                   RequestAttributes.SCOPE_REQUEST );
        return resolver.resolve( httpServletRequest.getRequestURL().toString(), throwable );
    }

}
