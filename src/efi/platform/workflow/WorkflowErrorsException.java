package efi.platform.workflow;

import efi.platform.web.controller.WorkflowErrors;

import org.springframework.validation.Errors;

/**
 * This exception signals a workflow error and can include a Spring Errors object.
 */
public class WorkflowErrorsException extends RuntimeException {

    private Errors errors;

    public WorkflowErrorsException( String message ) {
        super( message );
        this.errors = new WorkflowErrors();
    }

    public WorkflowErrorsException( String message,  Errors errors ) {
        super( message );
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
