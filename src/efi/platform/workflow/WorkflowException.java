package efi.platform.workflow;

/**
 * This class is used to signal that an error occurred during workflow processing.
 * Useful for ReST-ful web service processing.
 */
public class WorkflowException extends RuntimeException {

    public WorkflowException() {
        super();
    }

    public WorkflowException( String message ) {
        super( message );
    }

    public WorkflowException( String message, Throwable cause ) {
        super( message, cause );
    }

    public WorkflowException( Throwable cause ) {
        super( cause );
    }
}
