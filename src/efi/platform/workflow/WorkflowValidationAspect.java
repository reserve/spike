package efi.platform.workflow;

import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.lang.annotation.Annotation;
import java.util.Collection;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * This class is used to extend the JSR-303 bean validation aspects into the workflow layer.
 * Hints from:
 * http://nonrepeatable.blogspot.com/2010/05/validating-service-method-parameters.html
 * http://blog.newsplore.com/2010/02/23/spring-mvc-3-0-rest-rebuttal#jsr303
 *
 * The Spring validation layer must be declared in the Spring context via:
 * <bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean"/>
 * <aop:aspectj-autoproxy/>
 * <bean id="workflowValidator" class="efi.platform.workflow.WorkflowValidationAspect"/>
 */
@Aspect
public class WorkflowValidationAspect {

    private static final String NOT_NULL_PREAMBLE = "NotNull.";
    private static final String NOT_EMPTY_PREAMBLE = "NotEmpty.";
    
    @Autowired
    private Validator validator;

    @Resource
    ErrorMessageService errorMessageService;

    /**
     * Look for business methods annotated with the @Valid annotation and process them the same way that Spring MVC form
     * validation works.
     * @param joinPoint The method found by the aspect processing.
     * @param valid The @Valid annotation, used for the @Around advice.
     * @return The return value of the annotated method, if there are no validation errors.  Otherwise, null if there
     * was an available Spring Errors object in the argument list.
     * @throws Throwable if the method throws an exception.  Also, throws WorkflowValidationException if there was
     * not an available Spring Errors object in the argument list and there were validation errors.
     */
    @Around( "@annotation(valid)" )
    public Object businessValidation( ProceedingJoinPoint joinPoint, Valid valid ) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Errors errors = getErrorsFromArgs( joinPoint.getArgs() );
        if ( errors == null ) {
            errors = new WorkflowErrors();
            scanForAnnotations( joinPoint, signature, errors );
            if ( errors.hasErrors() ) {
                throw new WorkflowValidationException( "Error in automatic validation", errors );
            }
        }
        else {
            scanForAnnotations( joinPoint, signature, errors );
            if ( errors.hasErrors() ) {
                return null;
            }
        }
        return joinPoint.proceed( joinPoint.getArgs() );
    }

    /**
     * Scan through the argument list for the method, looking for parameters that have annotations.
     * Try to apply validation for any found annotations.
     * @param joinPoint The method found by the aspect processing.
     * @param signature The method signature of the method.
     * @param errors A Spring Errors object in which to record validation errors, if any.
     */
    private void scanForAnnotations( ProceedingJoinPoint joinPoint, MethodSignature signature, Errors errors ) {
        Annotation[][] annotationParameters = signature.getMethod().getParameterAnnotations();
        for ( int i = 0; i < annotationParameters.length; i++ ) {
            Annotation[] annotations = annotationParameters[i];
            for ( Annotation annotation : annotations ) {
                applyValidation( joinPoint, errors, annotation, i );
            }
        }
    }

    /**
     * Try to apply validation rules to the annotated argument at the given index.
     * If the annotation is @Valid, invoke the Spring validator on it (cascading down).
     * Otherwise, validate @NotNull and @NotEmpty scalar arguments by directly checking to see if they are valid.
     * If the validation fails, an error is recorded using the Spring conventions for message naming.
     * This is a key for which an error message can be defined in the message catalog.
     * Other annotations are ignored.
     * @param joinPoint The method found by the aspect processing.
     * @param errors A Spring Errors object in which to record validation errors, if any.
     * @param annotation The annotation to process.
     * @param index The index in the argument list the annotation applies to.
     */
    private void applyValidation( ProceedingJoinPoint joinPoint, Errors errors, Annotation annotation, int index ) {
        Object argument = joinPoint.getArgs()[index];
        if ( annotation instanceof Valid ) {
            validator.validate( argument, errors );
        }
        else if ( annotation instanceof NotNull ) {
            NotNull notNullAnnotation = (NotNull) annotation;
            if ( argument == null ) {
                errors.reject( NOT_NULL_PREAMBLE + notNullAnnotation.message() );
            }
        }
        else if ( annotation instanceof NotEmpty ) {
            NotEmpty notEmptyAnnotation = (NotEmpty) annotation;
            // Treat null argument as failing non-empty validation.
            if ( argument == null ) {
                errors.reject( NOT_EMPTY_PREAMBLE + notEmptyAnnotation.message() );
            }
            else if ( argument instanceof String && StringUtils.isBlank( (String) argument ) ) {
                errors.reject( NOT_EMPTY_PREAMBLE + notEmptyAnnotation.message() );
            }
            else if ( argument instanceof Collection && ( (Collection) argument ).isEmpty() ) {
                errors.reject( NOT_EMPTY_PREAMBLE + notEmptyAnnotation.message() );
            }
        }
    }

    /**
     * Search for a Spring binding object in the argument list, if one exists.
     * @param args The argument list.
     * @return A Spring Errors object in which to record validation errors, or null.
     */
    private Errors getErrorsFromArgs( Object[] args ) {
        for ( Object arg : args ) {
            if ( arg instanceof Errors ) {
                return (Errors) arg;
            }
        }
        return null;
    }
}
