package efi.unleashed.security;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * This class is filled in during authentication and has request scope.
 */
@Component
@Scope( value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS )
public class RequestUserContext extends UserContext {

    public RequestUserContext() {
        clear();
    }
}
