package efi.unleashed.security;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

/**
 * This class implements the Spring Security salt source API so that the same stored, hashed password in the database
 * can be used for both digest authentication and UI form-based authentication.
 */
public class DigestAuthenticationAwareSaltSource implements SaltSource, InitializingBean {
    
    private String digestRealm;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.hasText( digestRealm, "A Digest Realm must be set" );
    }

    @Override
    public Object getSalt( UserDetails user ) {
        return String.format( "%s:%s:", user.getUsername(), digestRealm );
    }

    public void setDigestRealm( String digestRealm ) {
        this.digestRealm = digestRealm;
    }
}
