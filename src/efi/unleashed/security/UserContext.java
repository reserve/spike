package efi.unleashed.security;

import java.io.Serializable;

/**
 * This class is used to define data that is available at various web-based scopes (request, session, etc.).
 */
public abstract class UserContext implements Serializable {

    public static final Long SYSTEM_USER_ID = 1L;
    public static final Long SYSTEM_MERCHANT_ID = 1L;

    private Long userId;
    private Long merchantId;

    public final void clear() {
        setUserId( SYSTEM_USER_ID );
        setMerchantId( null );
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId( Long userId ) {
        this.userId = userId;
        setMerchantId( null );
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId( Long merchantId ) {
        this.merchantId = merchantId;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( !( o instanceof UserContext ) ) return false;

        UserContext that = (UserContext) o;

        if ( merchantId != null ? !merchantId.equals( that.merchantId ) : that.merchantId != null ) return false;
        if ( userId != null ? !userId.equals( that.userId ) : that.userId != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + ( merchantId != null ? merchantId.hashCode() : 0 );
        return result;
    }

    @Override
    public String toString() {
        return "UserContext {" +
               "userId=" + userId +
               ", merchantId=" + merchantId +
               '}';
    }
}
