package efi.unleashed.security;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.codec.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class implements the Spring Security password encoder API so that the same stored, hashed password in the
 * database can be used for both digest authentication and UI form-based authentication.
 */
public class DigestAuthenticationAwarePasswordEncoder implements PasswordEncoder {

    @Override
    public String encodePassword( String rawPass, Object salt ) {
        String toEncode = salt + rawPass;
        return md5Hex( toEncode );
    }

    @Override
    public boolean isPasswordValid( String encPass, String rawPass, Object salt ) {
        if ( rawPass.equals( encPass ) ) return true;
        String calculatedPassword = md5Hex( salt + rawPass );
        return calculatedPassword.equals( encPass );
    }

    // Copied from DigestAuthUtils
    public String md5Hex(String data) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No MD5 algorithm available!");
        }

        return new String( Hex.encode( digest.digest( data.getBytes() ) ));
    }
}
