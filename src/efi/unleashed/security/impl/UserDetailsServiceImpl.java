package efi.unleashed.security.impl;

import efi.unleashed.dao.UserDao;
import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.domain.Right;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.security.UserDetailsAdapter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

/**
 * This class provides the interface between the application and the Spring Security layer.
 */
@Service( "userDetailsService" )
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    SessionUserContext sessionUserContext;

    @Resource
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
        ApplicationUser applicationUser = userDao.findByUsername( username );
        if ( applicationUser == null ) {
            throw new UsernameNotFoundException( "Username not found." );
        }

        // Get user rights as granted authorities (for Spring Security).
        Collection<GrantedAuthority> authorities = getGrantedAuthorities( applicationUser );

        //  Fill in a request user context.
        requestUserContext.setUserId( applicationUser.getId() );
        requestUserContext.setMerchantId( applicationUser.getPrimaryMerchant().getId() );

        // Fill in a session user context.
        sessionUserContext.setUserId( applicationUser.getId() );
        sessionUserContext.setMerchantId( applicationUser.getPrimaryMerchant().getId() );

        // Adapt the user object for Spring Security.
        final UserDetails springUser = new UserDetailsAdapter( applicationUser, authorities );
        
        return springUser;
    }

    private Collection<String> getUserRights( ApplicationUser applicationUser ) {
        Collection<String> rights = new ArrayList<String>();
        for ( Right right : applicationUser.getRole().getRights() ) {
            rights.add( right.getRightType().name() );
        }
        return rights;
    }

    private Collection<GrantedAuthority> getGrantedAuthorities( ApplicationUser applicationUser ) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for ( Right right : applicationUser.getRole().getRights() ) {
            authorities.add( new SimpleGrantedAuthority( right.getRightType().name() ) );
        }
        return authorities;
    }
}
