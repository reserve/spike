package efi.unleashed.security;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * This class is filled in during authentication and has session scope.
 */
@Component
@Scope( value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS )
public class SessionUserContext extends UserContext {

    public SessionUserContext() {
        clear();
    }
}
