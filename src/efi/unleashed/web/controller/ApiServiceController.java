package efi.unleashed.web.controller;

import efi.platform.web.controller.WorkflowErrors;
import efi.platform.workflow.WorkflowErrorsException;
import efi.unleashed.view.api.ApiResponseView;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.PrepareForServiceView;
import efi.unleashed.view.api.ServiceUpdateView;
import efi.unleashed.workflow.ApiServiceWorkflow;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * This class is used to accept ReST calls for the Day of Service domain.
 */
@Controller
@RequestMapping( value = ApiServiceController.API_SERVICE, produces = "application/json" )
public class ApiServiceController extends ApiBaseController {

    // Controller mapping name
    public static final String API_SERVICE = "api/service";

    // Request mapping names (please maintain alphabetic order)
    public static final String CLOSE = "close";
    public static final String DINING_SERVICE = "diningservice";
    public static final String PREPARE = "prepare";
    public static final String SITE_WITH_ID = "site/{siteId}";
    public static final String START = "start";
    public static final String UPDATE = "update";
    public static final String UPDATES = "updates";
    public static final String WAIT_LIST = "waitlist";

    @Resource
    ApiServiceWorkflow apiServiceWorkflow;

    @RequestMapping( value = SITE_WITH_ID + "/" + DINING_SERVICE + "/{date}", method = RequestMethod.GET )
    @ResponseBody
    public ApiResponseView serviceStatus( @PathVariable Long siteId,
                                          @PathVariable
                                          @DateTimeFormat( iso = DateTimeFormat.ISO.DATE )
                                          LocalDate date ) {
        WorkflowErrors errors = new WorkflowErrors();
        PrepareForServiceView view = apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error checking for service", errors );
        }
        if ( view == null ) {
            return buildResponse( view, HttpStatus.NOT_FOUND );
        }
        return buildResponse( view, HttpStatus.OK );
    }

    @RequestMapping( value = SITE_WITH_ID + "/" + PREPARE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView prepareForService( @RequestBody PrepareForServiceView prepareForServiceView,
                                              @PathVariable Long siteId ) {
        WorkflowErrors errors = new WorkflowErrors();
        PrepareForServiceView view = apiServiceWorkflow.prepareForService( prepareForServiceView, siteId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error preparing for service", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = SITE_WITH_ID + "/" + START, method = RequestMethod.GET )
    @ResponseBody
    public ApiResponseView startService( @PathVariable Long siteId ) {
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.startService( siteId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error starting service", errors );
        }
        return buildResponse( view, HttpStatus.OK );
    }

    @RequestMapping( value = SITE_WITH_ID + "/" + UPDATES, method = RequestMethod.GET )
    @ResponseBody
    public ApiResponseView getUpdates( @PathVariable Long siteId,
                                       @RequestParam( value = "since" )
                                       @DateTimeFormat( iso = DateTimeFormat.ISO.DATE_TIME )
                                       DateTime datetime ) {
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.getServiceUpdates( siteId, datetime, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error getting service updates", errors );
        }
        return buildResponse( view, HttpStatus.OK );
    }

    @RequestMapping( value = SITE_WITH_ID + "/" + CLOSE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView closeService( @RequestBody CloseServiceView closeServiceView, @PathVariable Long siteId ) {
        WorkflowErrors errors = new WorkflowErrors();
        PrepareForServiceView view = apiServiceWorkflow.close( closeServiceView, siteId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error closing service", errors );
        }
        return buildResponse( view, HttpStatus.OK );
    }

    @RequestMapping( value = UPDATE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView updateService( @RequestBody ServiceUpdateView serviceUpdateView ) {
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.update( serviceUpdateView, errors );
        return buildMixedResponse( view, errors );
    }

    @RequestMapping( value = SITE_WITH_ID + "/" + WAIT_LIST, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addToWaitList( @RequestBody DiningReservationView diningReservationView,
                                          @PathVariable Long siteId ) {
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.addToWaitList( diningReservationView, siteId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error adding to the wait list", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }
}
