package efi.unleashed.web.controller;

import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.view.api.ApiResponseView;
import efi.unleashed.view.api.ApplicationUserView;
import efi.unleashed.workflow.ApiUserWorkflow;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * This class is used to accept ReST calls for the User domain.
 */
@Controller
@RequestMapping( value = ApiUserController.API_USER, produces = "application/json" )
public class ApiUserController extends ApiBaseController {
    
    @Resource
    ApiUserWorkflow userWorkflow;

    // Controller mapping name
    public static final String API_USER = "api/user";

    // Request mapping names (please maintain alphabetic order)
    public static final String CHECK = "check";

    @RequestMapping( value = CHECK, method = RequestMethod.GET )
    @ResponseBody
    public ApiResponseView checkUser() {
        WorkflowErrors errors = new WorkflowErrors();
        ApplicationUserView view = userWorkflow.checkUser( errors );
        return buildResponse( view, errors );
    }
}
