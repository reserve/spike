package efi.unleashed.web.controller;

import efi.platform.service.ErrorMessageService;
import efi.platform.workflow.WorkflowErrorsException;
import efi.platform.workflow.WorkflowException;
import efi.unleashed.view.api.ApiResponseView;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.ResponseView;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

/**
 * This class is used as the base class for all the API (ReST) controllers.
 * It defines a number of ExceptionHandlers to respond with a reasonable view for clients.
 * Note that exception handling is not intended to fall through to the ApplicationExceptionHandler.
 * That is only for the web application layer.
 * If possible, determine new, unhandled exceptions and add handlers for them.
 */
public class ApiBaseController {

    private static final Logger LOGGER = Logger.getLogger( ApiBaseController.class.getName() );
    
    @Resource
    ErrorMessageService errorMessageService;

    @ExceptionHandler( { AccessDeniedException.class } )
    @ResponseBody
    public ApiResponseView handleAccessDeniedException( AccessDeniedException ex ) {
        return buildResponse( ex, HttpStatus.FORBIDDEN );
    }

    @ExceptionHandler( { WorkflowException.class, HttpMessageNotReadableException.class } )
    @ResponseBody
    public ApiResponseView handleWorkflowException( RuntimeException ex ) {
        return buildResponse( ex );
    }

    @ExceptionHandler( { WorkflowErrorsException.class } )
    @ResponseBody
    public ApiResponseView handleWorkflowErrorsException( WorkflowErrorsException ex ) {
        return buildResponse( ex, ex.getErrors() );
    }

    @ExceptionHandler( { IOException.class, IllegalArgumentException.class } )
    @ResponseBody
    public ApiResponseView handleJavaExceptions( Exception ex ) {
        return buildResponse( ex );
    }

    // Catch-all handler. TODO: should this stay?
    @ExceptionHandler( Throwable.class )
    @ResponseBody
    public ApiResponseView handleThrowable( Throwable ex ) {
        return buildResponse( ex );
    }

    protected ApiResponseView buildResponse( ApiView view ) {
        return buildResponse( view, null, HttpStatus.OK );
    }

    protected ApiResponseView buildResponse( ApiView view, HttpStatus goodCode ) {
        return buildResponse( view, null, goodCode);
    }

    protected ApiResponseView buildResponse( ApiView view, Errors errors ) {
        return buildResponse( view, errors, HttpStatus.OK );
    }

    protected ApiResponseView buildResponse( ApiView view, Errors errors, HttpStatus goodCode ) {
        ApiResponseView response = new ApiResponseView();
        ResponseView responseView = new ResponseView();
        response.setResponse( responseView );
        if ( errors != null && errors.hasErrors() ) {
            responseView.setCode( HttpStatus.BAD_REQUEST.value() );
            responseView.setMessages( errorMessageService.getDisplayableErrors( errors ) );
            LOGGER.error( responseView );
        }
        else {
            responseView.setCode( goodCode.value() );
            response.setContent( view );
        }
        return response;
    }

    protected ApiResponseView buildResponse( Throwable exception ) {
        return buildResponse( exception, null, HttpStatus.BAD_REQUEST );
    }

    protected ApiResponseView buildResponse( Throwable exception, HttpStatus status ) {
        return buildResponse( exception, null, status );
    }

    protected ApiResponseView buildResponse( Throwable exception, Errors errors ) {
        return buildResponse( exception, errors, HttpStatus.BAD_REQUEST );
    }

    protected ApiResponseView buildResponse( Throwable exception, Errors errors, HttpStatus status ) {
        ApiResponseView response = new ApiResponseView();
        ResponseView responseView = new ResponseView();
        response.setResponse( responseView );
        responseView.setCode( status.value() );
        List<String> messages = new ArrayList<String>();
        messages.add( exception.getMessage() );
        if ( errors != null && errors.hasErrors() ) {
            messages.addAll( errorMessageService.getDisplayableErrors( errors ) );
        }
        responseView.setMessages( messages );
        LOGGER.error( responseView );
        return response;
    }

    protected ApiResponseView buildMixedResponse( ApiView view, Errors errors ) {
        ApiResponseView response = new ApiResponseView();
        ResponseView responseView = new ResponseView();
        response.setResponse( responseView );
        response.setContent( view );
        responseView.setCode( HttpStatus.OK.value() );
        if ( errors != null && errors.hasErrors() ) {
            responseView.setMessages( errorMessageService.getDisplayableErrors( errors ) );
            LOGGER.error( responseView );
        }
        return response;
    }
}
