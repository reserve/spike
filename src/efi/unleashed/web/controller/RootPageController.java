package efi.unleashed.web.controller;

import efi.platform.web.controller.ControllerConstants;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This class handles page requests for URLs at the root level.
 * The controller mapping, request mapping, and resulting view names are slightly different than typical
 * page controllers, since the root mapping is essentially empty.
 */
@Controller
public class RootPageController {

    // Controller mapping name
    public static final String ROOT = "";

    // Request mapping names (please maintain alphabetic order)
    public static final String INDEX = "index";
    public static final String LOGIN = "login";

    // Resulting view names (please maintain alphabetic order)
    public static final String ROOT_INDEX = ROOT + "/" + INDEX;
    public static final String ROOT_LOGIN = ROOT + "/" + LOGIN;
    
    @RequestMapping( value = ROOT_INDEX, method = RequestMethod.GET )
    public String index() {
        // Short-circuit the index page to either go to the home page (if authenticated) or the login page.
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ( authentication != null && authentication.isAuthenticated() ) {
            return ControllerConstants.REDIRECT + HomePageController.HOME_MAIN;
        }
        return null;
    }
    
    @RequestMapping( value = ROOT_LOGIN, method = RequestMethod.GET )
    public String login() {
        return LOGIN;
    }
}
