package efi.unleashed.web.controller;

import efi.platform.web.controller.WorkflowErrors;
import efi.platform.workflow.WorkflowErrorsException;
import efi.unleashed.view.api.ApiResponseView;
import efi.unleashed.view.api.AssignSectionTableView;
import efi.unleashed.view.api.AssignedSectionTableView;
import efi.unleashed.view.api.BinaryDataView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;
import efi.unleashed.workflow.ApiConfigurationWorkflow;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to accept ReST calls for the Configuration domain.
 */
@Controller
@RequestMapping( value = ApiConfigController.API_CONFIG, produces = "application/json" )
public class ApiConfigController extends ApiBaseController {

    // Controller mapping name
    public static final String API_CONFIG = "api/config";

    // Request mapping names (please maintain alphabetic order)
    public static final String FLOOR_PLAN = "floorplan";
    public static final String FLOOR_PLAN_ITEM = "floorplanitem";
    public static final String INFRASTRUCTURE = "infrastructure";
    public static final String MERCHANT = "merchant";
    public static final String SECTION = "section";
    public static final String SERVER = "server";
    public static final String TABLE = "table";
    public static final String UPDATE = "update";
    public static final String WAIT_STATION_TEMPLATE = "waitstationtemplate";

    @Resource
    ApiConfigurationWorkflow apiConfigurationWorkflow;

    @RequestMapping( value = INFRASTRUCTURE, method = RequestMethod.GET )
    @ResponseBody
    public ApiResponseView getMerchantConfig() {
        return buildResponse( apiConfigurationWorkflow.getMerchantConfigurationView() );
    }

    @RequestMapping( value = MERCHANT + "/logo/{binaryMetadataId}", method = RequestMethod.GET )
    @ResponseBody
    public void getMerchantLogo( @PathVariable Long binaryMetadataId, HttpServletResponse response )
            throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        BinaryDataView view = apiConfigurationWorkflow.getMerchantLogo( binaryMetadataId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error retrieving merchant logo", errors );
        }
        prepareBinaryResponse( view, response );
    }

    @RequestMapping( value = FLOOR_PLAN, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addFloorPlan( @RequestBody FloorPlanView floorPlanView ) {
        WorkflowErrors errors = new WorkflowErrors();
        FloorPlanView view = apiConfigurationWorkflow.addFloorPlan( floorPlanView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating floor plan", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = FLOOR_PLAN + "/diagram/{binaryMetadataId}", method = RequestMethod.GET )
    @ResponseBody
    public void getFloorPlanDiagram( @PathVariable Long binaryMetadataId, HttpServletResponse response )
            throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        BinaryDataView view = apiConfigurationWorkflow.getFloorPlanDiagram( binaryMetadataId, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error retrieving floor plan diagram", errors );
        }
        prepareBinaryResponse( view, response );
    }

    @RequestMapping( value = FLOOR_PLAN_ITEM, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addFloorPlanItem( @RequestBody FloorPlanItemView floorPlanItemView ) {
        WorkflowErrors errors = new WorkflowErrors();
        FloorPlanItemView view = apiConfigurationWorkflow.addFloorPlanItem( floorPlanItemView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating new floor plan item", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = TABLE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addTable( @RequestBody TableItemView tableItemView ) {
        WorkflowErrors errors = new WorkflowErrors();
        TableItemView view = apiConfigurationWorkflow.addDiningTable( tableItemView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating new dining table", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = WAIT_STATION_TEMPLATE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addWaitStationTemplate( @RequestBody WaitStationTemplateView waitStationTemplateView ) {
        WorkflowErrors errors = new WorkflowErrors();
        WaitStationTemplateView view = apiConfigurationWorkflow.addWaitStationTemplate( waitStationTemplateView,
                                                                                        errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating new wait station template", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = SECTION, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addSection( @RequestBody SectionView sectionView ) {
        WorkflowErrors errors = new WorkflowErrors();
        SectionView view = apiConfigurationWorkflow.addSection( sectionView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating new section", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = SERVER, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView addServer( @RequestBody ServerView serverView ) {
        WorkflowErrors errors = new WorkflowErrors();
        ServerView view = apiConfigurationWorkflow.addServer( serverView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error creating new server", errors );
        }
        return buildResponse( view, HttpStatus.CREATED );
    }

    @RequestMapping( value = WAIT_STATION_TEMPLATE + "/assignsectiontables", method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView assignSectionTables( @RequestBody AssignSectionTableView assignSectionTableView ) {
        WorkflowErrors errors = new WorkflowErrors();
        AssignedSectionTableView view = apiConfigurationWorkflow.assignSectionTables( assignSectionTableView, errors );
        if ( errors.hasErrors() ) {
            throw new WorkflowErrorsException( "Error assigning tables to sections", errors );
        }
        return buildResponse( view );
    }

    @RequestMapping( value = UPDATE, method = RequestMethod.POST )
    @ResponseBody
    public ApiResponseView updateConfiguration( @RequestBody ConfigurationUpdateView configurationUpdateView ) {
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiConfigurationWorkflow.update( configurationUpdateView, errors );
        return buildMixedResponse( view, errors );
    }

    private void prepareBinaryResponse( BinaryDataView view, HttpServletResponse response ) throws IOException {
        response.setHeader( "Pragma", "public" );
        response.setHeader( "Cache-Control", "max-age=0" );
        response.setHeader( "Content-Type", view.getContentType() );
        response.setHeader( "Content-Disposition", "attachment; filename=\"" + view.getName() + "\"" );
        response.setContentType( view.getContentType() );
        response.setContentLength( view.getSize().intValue() );
        response.getOutputStream().write( view.getData() );
    }
}
