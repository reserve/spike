package efi.unleashed.web.controller;

import efi.unleashed.view.web.ReportEditView;
import efi.unleashed.workflow.ReportsWorkflow;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import sun.management.snmp.jvmmib.JVM_MANAGEMENT_MIB;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * This class handles page requests for URLs dealing with reports.
 */
@Controller
@RequestMapping( value = ReportsController.REPORTS )
@SessionAttributes ( value = { "reportEditView"} )
public class ReportsController {

    // Controller mapping name
    public static final String REPORTS = "web/reports";

    // Request mapping names (please maintain alphabetic order)
    public static final String DATES = "dates";
    public static final String EDIT = "edit";
    public static final String MANAGE = "manage";

    // Resulting view names (please maintain alphabetic order)
    public static final String EDIT_REPORTS = REPORTS + "/" + EDIT;
    public static final String MANAGE_REPORTS = REPORTS + "/" + MANAGE;

    @Resource
    ReportsWorkflow reportsWorkflow;

    @RequestMapping( value = EDIT + "/{reportId}", method = RequestMethod.GET )
    public String editReport( @PathVariable Long reportId, Model model ) {
        model.addAttribute( "reportEditView", reportsWorkflow.prepareToEditReport( reportId ) );
        return EDIT_REPORTS;
    }

    @RequestMapping( value = DATES, method = RequestMethod.GET )
    public @ResponseBody
    List<String> getDatesForRange( @RequestParam( value = "range" ) String range ){
        return reportsWorkflow.getDateStringsForRange( range );
    }

    @RequestMapping( value = MANAGE, method = RequestMethod.GET )
    public String manageReports( Model model ) {
        model.addAttribute( reportsWorkflow.getReports() );
        return MANAGE_REPORTS;
    }

    @RequestMapping( value = MANAGE, params="saveReportButton", method = RequestMethod.POST )
    public String saveReport (@ModelAttribute( "reportEditView" ) @Valid ReportEditView reportEditView, BindingResult result, Model model, SessionStatus status ) {
        if ( result.hasErrors() ) {
            return EDIT_REPORTS;
        }

        reportsWorkflow.validate( reportEditView, result );
        if ( result.hasErrors() ) {
            return EDIT_REPORTS;
        }

        reportsWorkflow.updateReport( reportEditView, result );
        if ( result.hasErrors() ) {
            return EDIT_REPORTS;
        }

        status.setComplete();
        return "redirect:/" + MANAGE_REPORTS;

    }

}
