package efi.unleashed.web.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = HtmlEditorController.HTML_EDITOR)
public class HtmlEditorController {

    public static final String HTML_EDITOR = "web/htmlEditor";
    public static final String MAIN = "main";
    public static final String HTML_EDITOR_MAIN = HTML_EDITOR + "/" + MAIN;

    @RequestMapping(value = MAIN, method = RequestMethod.GET)
    public String mainHtmlEditorTestPage( Model model ) {

        return HTML_EDITOR_MAIN;
    }

    @RequestMapping( value = MAIN, method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded" )
    public ResponseEntity<String> processJson( @RequestBody String requestBody ) throws  Exception {

        String decodedHTML = java.net.URLDecoder.decode( requestBody, "UTF-8" );
        return new ResponseEntity<String>(
                "<TEXTAREA rows=\"35s\" cols=\"100\">" + decodedHTML + "</TEXTAREA>",
                new HttpHeaders(),
                HttpStatus.OK );
    }

}
