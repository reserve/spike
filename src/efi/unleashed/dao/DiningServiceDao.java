package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.TableService;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.List;

public interface DiningServiceDao extends GenericDao {

    DiningReservation findDiningReservationForMerchant( Long diningReservationId, Long merchantId );

    DiningService findDiningServiceForSite( Long diningServiceId, Long siteId );
    DiningService findForSiteByServiceDate( Long siteId, LocalDate serviceDate, boolean isInService );

    TableService findTableServiceForMerchant( Long tableServiceId, Long merchantId );

    List<TableService> findUpdatedTableService( Long siteId, DateTime since );

    List<DiningReservation> findAllReservationsForSite( Long siteId );

    List<DiningReservation> findReservationsByStatus( final Long siteId, final List<String> statuses );

    Integer getReservationCount( final Long siteId );
}
