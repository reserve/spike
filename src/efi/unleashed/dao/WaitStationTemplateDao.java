package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.WaitStationTemplate;

public interface WaitStationTemplateDao extends GenericDao {

    WaitStationTemplate findTemplateForMerchant( Long waitStationTemplateId, Long merchantId );
}
