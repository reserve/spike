package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Server;

import java.util.List;

public interface ServerDao extends GenericDao {

    Server findServerForMerchant( Long serverId, Long merchantId );

    List<Server> findServersByIds( List<Long> serverIds );
}
