package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.DiningTable;

import java.util.List;

public interface DiningTableDao extends GenericDao {

    DiningTable findDiningTableForMerchant( Long tableId, Long merchantId );

    List<DiningTable> findTablesByIds( List<Long> tableIds );
}
