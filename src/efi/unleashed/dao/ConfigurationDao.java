package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.WaitStationTemplate;

/**
 * This interface defines the data access methods used for the configuration-oriented portion of the data model.
 */
public interface ConfigurationDao extends GenericDao {
    // TODO:  Migrate the other config dao methods here.

    AvailabilityMatrix findAvailabilityMatrixForSite( Long availabilityMatrixId, Long siteId );

    FloorPlan findFloorPlanForSite( Long floorPlanId, Long siteId );

    WaitStationTemplate findWaitStationTemplateForSite( Long templateId, Long siteId );
}
