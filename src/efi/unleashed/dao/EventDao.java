package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.EventType;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportFilterField;

import java.util.List;
import java.util.Set;

public interface EventDao extends GenericDao {

    List<Event> findEventsForMerchant( Long merchantId );

    List<Event> findEventsForEventType( Long merchantId, String eventTypeString );

    List<EventType> findEventTypesForMerchant( Long merchantId );

    List<Event> findEventsForReport( final Report report );
}
