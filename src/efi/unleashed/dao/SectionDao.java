package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Section;

import java.util.List;

public interface SectionDao extends GenericDao {

    Section findSectionForMerchant( Long sectionId, Long merchantId );

    List<Section> findSectionsByIds( List<Long> sectionIds );
}
