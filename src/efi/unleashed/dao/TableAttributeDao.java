package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.TableAttribute;

import java.util.List;

public interface TableAttributeDao extends GenericDao {

    TableAttribute findTableAttributeForMerchant( Long attributeId, Long merchantId );

    List<TableAttribute> findByMerchantId( Long merchantId );
}
