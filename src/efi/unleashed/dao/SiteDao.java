package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Site;

public interface SiteDao extends GenericDao {

    Site findSiteForMerchant( Long siteId, Long merchantId );
}
