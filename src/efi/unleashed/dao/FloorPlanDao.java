package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;

public interface FloorPlanDao extends GenericDao {

    FloorPlan findFloorPlanForMerchant( Long floorPlanId, Long merchantId );

    FloorPlanItem findFloorPlanItemForMerchant( Long floorPlanItemId, Long merchantId );
}
