package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Merchant;

public interface MerchantDao extends GenericDao {

    Merchant findByShortName( String shortName );
}
