package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportType;

import java.util.List;

public interface ReportDao extends GenericDao {

    List<Report> findReportsForMerchant( Long merchantId );

    List<Report> findReportsForReportType( Long merchantId, String reportTypeString );

    Report findReportForId( Long reportId );

    ReportType findReportTypeForType( String reportType );

    ReportField findReportFieldForId( Long reportFieldId );
}
