package efi.unleashed.dao;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.ApplicationUser;

public interface UserDao extends GenericDao {

    ApplicationUser findByUsername( String username );
}
