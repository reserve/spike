package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.ServerDao;
import efi.unleashed.domain.Server;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServerDaoImpl extends GenericDaoImpl implements ServerDao {

    @Autowired
    public ServerDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public Server findServerForMerchant( Long serverId, Long merchantId ) {
        return (Server) findEntity( "Server.findForMerchant", "serverId", serverId, "merchantId", merchantId );
    }

    @Override
    public List<Server> findServersByIds( List<Long> serverIds ) {
        return findEntities( Server.class, "Server.findServersByIds", "ids", serverIds );
    }
}
