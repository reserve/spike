package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.WaitStationTemplateDao;
import efi.unleashed.domain.WaitStationTemplate;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WaitStationTemplateDaoImpl extends GenericDaoImpl implements WaitStationTemplateDao {

    @Autowired
    public WaitStationTemplateDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public WaitStationTemplate findTemplateForMerchant( Long waitStationTemplateId, Long merchantId ) {
        return (WaitStationTemplate) findEntity( "WaitStationTemplate.findForMerchant",
                                                 "waitStationTemplateId", waitStationTemplateId,
                                                 "merchantId", merchantId );
    }
}
