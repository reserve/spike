package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.SiteDao;
import efi.unleashed.domain.Site;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SiteDaoImpl extends GenericDaoImpl implements SiteDao {

    @Autowired
    public SiteDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public Site findSiteForMerchant( Long siteId, Long merchantId ) {
        return (Site) findEntity( "Site.findForMerchant", "siteId", siteId, "merchantId", merchantId );
    }
}
