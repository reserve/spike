package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.UserDao;
import efi.unleashed.domain.ApplicationUser;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends GenericDaoImpl implements UserDao {

    @Autowired
    public UserDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public ApplicationUser findByUsername( String username ) {
        return (ApplicationUser) findEntity( "ApplicationUser.findByUsername", "username", username );
    }
}
