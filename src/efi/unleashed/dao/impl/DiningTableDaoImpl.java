package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.DiningTableDao;
import efi.unleashed.domain.DiningTable;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DiningTableDaoImpl extends GenericDaoImpl implements DiningTableDao {

    @Autowired
    public DiningTableDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public DiningTable findDiningTableForMerchant( Long tableId, Long merchantId ) {
        return (DiningTable) findEntity( "DiningTable.findForMerchant", "tableId", tableId, "merchantId", merchantId );
    }

    @Override
    public List<DiningTable> findTablesByIds( List<Long> tableIds ) {
        return findEntities( DiningTable.class, "DiningTable.findTablesByIds", "ids", tableIds );
    }
}
