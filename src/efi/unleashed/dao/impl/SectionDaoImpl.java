package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.SectionDao;
import efi.unleashed.domain.Section;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SectionDaoImpl extends GenericDaoImpl implements SectionDao {

    @Autowired
    public SectionDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public Section findSectionForMerchant( Long sectionId, Long merchantId ) {
        return (Section) findEntity( "Section.findForMerchant", "sectionId", sectionId, "merchantId", merchantId );
    }

    @Override
    public List<Section> findSectionsByIds( List<Long> sectionIds ) {
        return findEntities( Section.class, "Section.findSectionsByIds", "ids", sectionIds );
    }
}
