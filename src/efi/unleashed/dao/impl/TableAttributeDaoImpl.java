package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.TableAttributeDao;
import efi.unleashed.domain.TableAttribute;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TableAttributeDaoImpl extends GenericDaoImpl implements TableAttributeDao {

    @Autowired
    public TableAttributeDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public TableAttribute findTableAttributeForMerchant( Long attributeId, Long merchantId ) {
        return (TableAttribute) findEntity( "TableAttribute.findForMerchant",
                                            "attributeId", attributeId,
                                            "merchantId", merchantId );
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<TableAttribute> findByMerchantId( Long merchantId ) {
        return findEntities( "TableAttribute.findByMerchantId", "merchantId", merchantId );
    }
}
