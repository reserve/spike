package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.ReportDao;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportType;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportDaoImpl extends GenericDaoImpl implements ReportDao {

    @Autowired
    public ReportDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public List<Report> findReportsForMerchant( Long merchantId ) {
        return findEntities( Report.class, "Report.findForMerchant", "merchantId", merchantId );
    }

    @Override
    public List<Report> findReportsForReportType( Long merchantId, String reportTypeString ) {
        return findEntities( Report.class, "Report.findForReportType", "merchantId", merchantId, "reportTypeString", reportTypeString );
    }

    @Override
    public Report findReportForId( Long reportId ) {
        return (Report) findEntity( "Report.findForId", "reportId", reportId );
    }

    @Override
    public ReportType findReportTypeForType( String reportType ) {
        return (ReportType) findEntity( "ReportType.findForType", "reportType", reportType );
    }

    @Override
    public ReportField findReportFieldForId( Long fieldId ) {
        return (ReportField) findEntity( "ReportField.findForId", "fieldId", fieldId );
    }
}
