package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.EventDao;
import efi.unleashed.domain.DateRange;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.EventType;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportDateRangeField;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportFilterField;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class EventDaoImpl extends GenericDaoImpl implements EventDao {

    @Autowired
    public EventDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public List<Event> findEventsForMerchant( Long merchantId ) {
        return findEntities( Event.class, "Event.findForMerchant", "merchantId", merchantId );
    }

    @Override
    public List<Event> findEventsForEventType( Long merchantId, String eventTypeString ) {
        return findEntities( Event.class,
                             "Event.findForEventType",
                             "merchantId",
                             merchantId,
                             "eventTypeString",
                             eventTypeString );
    }

    @Override
    public List<EventType> findEventTypesForMerchant( Long merchantId ) {
        return findEntities( EventType.class, "EventType.findForMerchant", "merchantId", merchantId );
    }

    @SuppressWarnings( "unchecked" )
    public List<Event> findEventsFilterExample1( final String salesperson ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<List<Event>>() {
            @Override
            public List<Event> doInHibernate( Session session )
                    throws HibernateException, SQLException {
                // Define the query.
                String sqlQuery = "select distinct {e.*} from event e " +
                                  " where e.salesperson = :salesperson ";
                SQLQuery query = session.createSQLQuery( sqlQuery );

                // Bind the arguments to query parameters.
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put( "salesperson", salesperson );
                query.setProperties( parameters );

                // Bind the {e.*} to the Event class, then call list() to get the data.
                return query.addEntity( "e", Event.class ).list();
            }
        } );
    }

    @SuppressWarnings( "unchecked" )
    public List<Event> findEventsFilterExample2( final String salesperson ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<List<Event>>() {
            @Override
            public List<Event> doInHibernate( Session session )
                    throws HibernateException, SQLException {
                // Define the query.
                String sqlQuery = "select distinct {e.*} from event e " +
                                  " where e.salesperson = ? ";
                SQLQuery query = session.createSQLQuery( sqlQuery );

                // Bind the arguments to query parameters.
                query.setString( 0, salesperson );

                // Bind the {e.*} to the Event class, then call list() to get the data.
                return query.addEntity( "e", Event.class ).list();
            }
        } );
    }


    @SuppressWarnings( "unchecked" )
    public List<Event> findEventsFilterExample3( final String salesperson ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<List<Event>>() {
            @Override
            public List<Event> doInHibernate( Session session )
                    throws HibernateException, SQLException {
                // Define the criteria.
                Criteria criteria = session.createCriteria( Event.class );
                criteria.add( Restrictions.eq( "salesperson", salesperson ) );

                return criteria.list();
            }
        } );
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Event> findEventsForReport( final Report report ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<List<Event>>() {
            @Override
            public List<Event> doInHibernate( Session session )
                    throws HibernateException, SQLException {

                String selectClause = "select distinct {ev.*} " +
                                      "from event ev " +
                                      "     join event_type etype on etype.id = ev.event_type_id ";

                StringBuilder sb = new StringBuilder();
                sb.append( " where " );
                sb.append( sqlWhereClauseFor( report.getDateRangeField() ) );

                List<ReportFilterField> sortedFilters =
                        new ArrayList<ReportFilterField>( report.getReportFilterFields() );
                Collections.sort( sortedFilters, ReportFilterField.FilterFieldComparator );
                for ( ReportFilterField filter : sortedFilters ) {
                    sb.append( " and " );
                    sb.append( sqlWhereClauseFor( filter ) );
                }

                String sqlQuery = selectClause + sb.toString();
                SQLQuery query = session.createSQLQuery( sqlQuery );

                int index = 0;
                DateRange dateRange = report.getDateRangeField().dateRange();
                Date start = dateRange.getStartDate().toDateMidnight().toDate();
                Date end = dateRange.getEndDate().toDateMidnight().toDate();
                query.setDate( index, start );
                index++;
                query.setDate( index, end );
                index++;
                for ( ReportFilterField filter : sortedFilters ) {
                    query.setString( index, sqlFilterValueForFilter( filter ) );
                    index++;
                }

                // Bind the {ev.*} to the Event class, then call list() to get the data.
                return query.addEntity( "ev", Event.class ).list();
            }
        } );
    }

    private String sqlWhereClauseFor( ReportDateRangeField dateRangeField ) {
        ReportField field = dateRangeField.getReportField();
        String sql =
                field.getDbAliasName() + "." +
                field.getDbColumnName() + " >= ? and " +
                field.getDbAliasName() + "." +
                field.getDbColumnName() + " <= ? ";
        return sql;
    }

    private String sqlWhereClauseFor( ReportFilterField filter ) {
        ReportField field = filter.getReportField();
        String sql =
                field.getDbAliasName() + "." +
                field.getDbColumnName() +
                filter.getOperation().getSqlString() + " ? ";
        return sql;
    }

    private String sqlFilterValueForFilter( ReportFilterField filter ) {
        String filterValue;
        switch ( filter.getOperation() ) {
        case BEGINS_WITH:
            filterValue = "%" + filter.getFilterValue();
            break;
        case CONTAINS:
            filterValue = "%" + filter.getFilterValue() + "%";
            break;
        default:
            filterValue = filter.getFilterValue();
            break;
        }
        return filterValue;
    }
}
