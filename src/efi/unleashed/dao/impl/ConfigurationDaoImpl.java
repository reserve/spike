package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.ConfigurationDao;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.WaitStationTemplate;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ConfigurationDaoImpl extends GenericDaoImpl implements ConfigurationDao {

    @Autowired
    public ConfigurationDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public AvailabilityMatrix findAvailabilityMatrixForSite( Long availabilityMatrixId, Long siteId ) {
        return (AvailabilityMatrix) findEntity( "AvailabilityMatrix.findForSite",
                                                "matrixId", availabilityMatrixId,
                                                "siteId", siteId );
    }

    @Override
    public FloorPlan findFloorPlanForSite( Long floorPlanId, Long siteId ) {
        return (FloorPlan) findEntity( "FloorPlan.findForSite", "floorPlanId", floorPlanId, "siteId", siteId );
    }

    @Override
    public WaitStationTemplate findWaitStationTemplateForSite( Long templateId, Long siteId ) {
        return (WaitStationTemplate) findEntity( "WaitStationTemplate.findForSite",
                                                 "waitStationTemplateId", templateId,
                                                 "siteId", siteId );
    }
}
