package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.MerchantDao;
import efi.unleashed.domain.Merchant;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MerchantDaoImpl extends GenericDaoImpl implements MerchantDao {

    @Autowired
    public MerchantDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public Merchant findByShortName( String shortName ) {
        return (Merchant) findEntity( "Merchant.findByShortName", "shortName", shortName );
    }
}
