package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.DiningServiceDao;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.TableService;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DiningServiceDaoImpl extends GenericDaoImpl implements DiningServiceDao {

    @Autowired
    public DiningServiceDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public DiningReservation findDiningReservationForMerchant( Long diningReservationId, Long merchantId ) {
        return (DiningReservation) findEntity( "DiningReservation.findForMerchant",
                                               "diningReservationId", diningReservationId, "merchantId", merchantId );
    }

    @Override
    public DiningService findDiningServiceForSite( Long diningServiceId, Long siteId ) {
        return (DiningService) findEntity( "DiningService.findForSite",
                                           "diningServiceId", diningServiceId, "siteId", siteId );
    }

    @Override
    public DiningService findForSiteByServiceDate( Long siteId, LocalDate serviceDate, boolean isInService ) {
        return (DiningService) findEntity( "DiningService.findForSiteByServiceDate",
                                           "siteId", siteId, "serviceDate", serviceDate, "isInService", isInService );
    }

    @Override
    public TableService findTableServiceForMerchant( Long tableServiceId, Long merchantId ) {
        return (TableService) findEntity( "TableService.findForMerchant",
                                          "tableServiceId", tableServiceId, "merchantId", merchantId );
    }

    @Override
    public List<TableService> findUpdatedTableService( Long siteId, DateTime since ) {
        return findEntities( TableService.class, "TableService.findUpdates", "siteId", siteId, "since", since );
    }

    @Override
    public List<DiningReservation> findAllReservationsForSite( Long siteId ) {
        return findEntities( DiningReservation.class, "DiningReservation.findAllForSite", "siteId", siteId );
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<DiningReservation> findReservationsByStatus( final Long siteId, final List<String> statuses ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<List<DiningReservation>>() {
            @Override
            public List<DiningReservation> doInHibernate( Session session )
                    throws HibernateException, SQLException {
                // Define the query.
                String sqlQuery = "select distinct {dr.*} from dining_reservation dr " +
                                  " where dr.site_id = :siteId " +
                                  "   and dr.status_type in (:statuses)";
                SQLQuery query = session.createSQLQuery( sqlQuery );

                // Bind the arguments to query parameters.
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put( "siteId", siteId );
                parameters.put( "statuses", statuses );
                query.setProperties( parameters );

                // Bind the {dr.*} to the DiningReservation class, then call list() to get the data.
                return query.addEntity( "dr", DiningReservation.class ).list();
            }
        } );
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public Integer getReservationCount( final Long siteId ) {
        return getHibernateTemplate().executeWithNativeSession( new HibernateCallback<Integer>() {
            @Override
            public Integer doInHibernate( Session session ) throws HibernateException, SQLException {
                // Define the query.
                String sqlQuery = "select distinct count(distinct dr.id) as entityCount " +
                                  "  from dining_reservation dr " +
                                  " where dr.site_id = :siteId";
                SQLQuery query = session.createSQLQuery( sqlQuery );

                // Bind the arguments to query parameters.
                Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put( "siteId", siteId );
                query.setProperties( parameters );

                // Add placeholders for scalar data.
                query.addScalar( "entityCount", StandardBasicTypes.INTEGER );

                // Call list() to get the results.
                List<Integer> results = query.list();

                // Should only be one row, and one column, the count.
                return results.get( 0 );
            }
        } );
    }
}
