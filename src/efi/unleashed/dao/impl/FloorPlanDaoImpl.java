package efi.unleashed.dao.impl;

import efi.platform.dao.impl.GenericDaoImpl;
import efi.unleashed.dao.FloorPlanDao;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FloorPlanDaoImpl extends GenericDaoImpl implements FloorPlanDao {

    @Autowired
    public FloorPlanDaoImpl( SessionFactory sessionFactory ) {
        setSessionFactory( sessionFactory );
    }

    @Override
    public FloorPlan findFloorPlanForMerchant( Long floorPlanId, Long merchantId ) {
        return (FloorPlan) findEntity( "FloorPlan.findForMerchant",
                                       "floorPlanId", floorPlanId,
                                       "merchantId", merchantId );
    }

    @Override
    public FloorPlanItem findFloorPlanItemForMerchant( Long floorPlanItemId, Long merchantId ) {
        return (FloorPlanItem) findEntity( "FloorPlanItem.findForMerchant",
                                           "floorPlanItemId", floorPlanItemId,
                                           "merchantId", merchantId );
    }
}
