package efi.unleashed.workflow.impl;

import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.UserService;
import efi.unleashed.view.api.ApplicationUserView;
import efi.unleashed.workflow.ApiUserWorkflow;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import javax.annotation.Resource;

@Service
public class ApiUserWorkflowImpl implements ApiUserWorkflow {

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    UserService userService;

    @Override
    public ApplicationUserView checkUser( Errors errors ) {
        ApplicationUser user = userService.findByUserId( requestUserContext.getUserId() );
        if ( user == null ) {
            errors.reject( "api.exception.not-found" );
            return null;
        }
        return new ApplicationUserView( user );
    }
}
