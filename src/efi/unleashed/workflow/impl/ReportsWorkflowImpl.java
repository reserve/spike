package efi.unleashed.workflow.impl;

import efi.unleashed.domain.DateRange;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportColumnField;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportFilterField;
import efi.unleashed.domain.ReportType;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.service.DateRangeService;
import efi.unleashed.service.ReportService;
import efi.unleashed.util.DateRangeUtils;
import efi.unleashed.view.web.ReportEditView;
import efi.unleashed.view.web.ReportsView;
import efi.unleashed.workflow.ReportsWorkflow;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.Resource;

@Service
public class ReportsWorkflowImpl implements ReportsWorkflow {

    @Resource
    SessionUserContext sessionUserContext;

    @Resource
    ReportService reportService;

    @Resource
    DateRangeService dateRangeService;

    @Override
    public ReportsView getReports() {
        List<Report> reports = reportService.findReportsForMerchant( sessionUserContext.getMerchantId() );
        List<ReportsView.ReportView> views = new ArrayList<ReportsView.ReportView>( reports.size() );
        for ( Report report : reports ) {
            ReportsView.ReportView reportView = new ReportsView.ReportView();
            reportView.setName( report.getName() );
            reportView.setType( report.getReportType().getType() );
            reportView.setId( report.getId() );
            views.add( reportView );
        }
        ReportsView view = new ReportsView();
        view.setReports( views );
        return view;
    }

    @Override
    public ReportEditView prepareToEditReport( Long reportId ) {
        Report report = reportService.findReportForId( reportId );
        ReportEditView view = new ReportEditView( report );
        view.setAllRanges( dateRangeService.getDateRangeStrings() );
        view.setAvailableFieldGroups( getAvailableFieldGroupsForReportType( report.getReportType() ) );

        List<ReportColumnField> columns = new ArrayList<ReportColumnField>( report.getReportColumnFields() );
        List<ReportEditView.ColumnFieldView> columnFieldViews =
                new ArrayList<ReportEditView.ColumnFieldView>( columns.size() );
        for ( ReportColumnField columnField : columns ) {
            ReportEditView.ColumnFieldView columnView = new ReportEditView.ColumnFieldView();
            columnView.setFieldID( columnField.getReportField().getId() );
            columnView.setColumnOrder( columnField.getColumnOrder() );
            columnView.setDataType( columnField.getReportField().getFieldDataType().name() );
            columnView.setFieldType( columnField.getReportField().getFieldType().name() );
            columnView.setName( columnField.getReportField().getFieldName() );
            columnFieldViews.add( columnView );
        }
        Collections.sort( columnFieldViews, ReportEditView.ColumnFieldView.ColumnOrderComparator );
        view.setColumns( columnFieldViews );

        List<ReportFilterField> filters = new ArrayList<ReportFilterField>( report.getReportFilterFields() );
        List<ReportEditView.FilterFieldView> filterFieldViews =
                new ArrayList<ReportEditView.FilterFieldView>( filters.size() );
        for ( ReportFilterField filterField : filters ) {
            ReportEditView.FilterFieldView filterView = new ReportEditView.FilterFieldView();
            filterView.setFieldID( filterField.getReportField().getId() );
            filterView.setDataType( filterField.getReportField().getFieldDataType().name() );
            filterView.setFieldType( filterField.getReportField().getFieldType().name() );
            filterView.setName( filterField.getReportField().getFieldName() );
            filterView.setOperation( filterField.getOperation().getUserString() );
            filterView.setFilterValue( filterField.getFilterValue() );
            filterFieldViews.add( filterView );
        }
        Collections.sort( filterFieldViews, ReportEditView.FilterFieldView.FieldTypeNameComparator );
        view.setFilters( filterFieldViews );
        return view;
    }

    @Override
    public ReportEditView prepareToEditReport( String reportType ) {
        ReportType type = reportService.findReportTypeForType( reportType );
        ReportEditView view = new ReportEditView();
        //TODO set default date range and field name and start/end dates
        view.setAllRanges( dateRangeService.getDateRangeStrings() );
        view.setName( "New Report: " + type.getType() );
        view.setType( type.getType() );
        view.setAvailableFieldGroups( getAvailableFieldGroupsForReportType( type ) );
        return view;
    }

    private List<ReportEditView.AvailableFieldGroupView> getAvailableFieldGroupsForReportType( ReportType reportType) {
        List<ReportField> availableFields = new ArrayList<ReportField>( reportType.getAvailableFields() );
//      gather all the field types from the available fields
        List<String> types = new ArrayList<String>( availableFields.size() );
        for ( ReportField field : availableFields ) {
            types.add( field.getFieldType().name() );
        }
//      now make them a set so the list only contains unique entries
        TreeSet<String> typeSet = new TreeSet<String>( types );
        List<ReportEditView.AvailableFieldGroupView> groupViews =
                new ArrayList<ReportEditView.AvailableFieldGroupView>( typeSet.size() );
//      for each type, create a group view
        for ( String type : typeSet ) {
            ReportEditView.AvailableFieldGroupView groupView = new ReportEditView.AvailableFieldGroupView();
            groupView.setName( type );
            List<ReportEditView.AvailableFieldView> availableFieldViews =
                    new ArrayList<ReportEditView.AvailableFieldView>( availableFields.size() );
//          now check each field and add it to this group if the type matches
            for ( ReportField field : availableFields ) {
                String current = field.getFieldType().name();
                if ( current.equals( type ) ) {
                    ReportEditView.AvailableFieldView availableView = new ReportEditView.AvailableFieldView();
                    availableView.setFieldID( field.getId() );
                    availableView.setAvailableColumn( field.isAvailableColumn() );
                    availableView.setAvailableFilter( field.isAvailableFilter() );
                    availableView.setAvailableGroupBy( field.isAvailableGroupBy() );
                    availableView.setDataType( field.getFieldDataType().name() );
                    availableView.setFieldType( field.getFieldType().name() );
                    availableView.setName( field.getFieldName() );
                    availableFieldViews.add( availableView );
                }
            }
//          sort the list of available fields and add them to the groupView
            Collections.sort( availableFieldViews, ReportEditView.AvailableFieldView.FieldTypeNameComparator );
            groupView.setAvailableFields( availableFieldViews );
//          add the group view to the list of group views
            groupViews.add( groupView );
        }
        return groupViews;
    }

    @Override
    public List<String> getDateStringsForRange( String range ) {
        return dateRangeService.getDateStringsForRange( range );
    }

    @Override
    public void validate( ReportEditView reportEditView, BindingResult result ) {
        String start = reportEditView.getStartDate();
        String end = reportEditView.getEndDate();
        DateTimeFormatter format = DateTimeFormat.forPattern( "MM/dd/yyyy" );
        try {
            format.parseLocalDate( start );
        }
        catch ( IllegalArgumentException e ) {
            result.rejectValue ( "startDate", null, "spike.date.format" );
        }
        try {
            format.parseLocalDate( end );
        }
        catch ( IllegalArgumentException e ) {
            result.rejectValue ( "endDate", null, "spike.date.format" );
        }
    }

    @Override
    public void updateReport( ReportEditView reportEditView, BindingResult result ) {
        reportService.updateReport( reportEditView, result );
    }

}
