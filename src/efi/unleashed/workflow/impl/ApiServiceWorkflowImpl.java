package efi.unleashed.workflow.impl;

import efi.unleashed.dao.MerchantDao;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningReservationStatusType;
import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.ScheduledServer;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;
import efi.unleashed.domain.TableService;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.ConfigurationService;
import efi.unleashed.service.DayOfServiceService;
import efi.unleashed.service.FloorPlanService;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.PrepareForServiceView;
import efi.unleashed.view.api.ServerSectionView;
import efi.unleashed.view.api.ServiceUpdateView;
import efi.unleashed.view.api.TableServiceView;
import efi.unleashed.workflow.ApiServiceWorkflow;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.Valid;

@Service
public class ApiServiceWorkflowImpl implements ApiServiceWorkflow {

    private static final Logger LOGGER = Logger.getLogger( ApiServiceWorkflowImpl.class.getName() );

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    MerchantDao merchantDao;

    @Resource
    FloorPlanService floorPlanService;

    @Resource
    DayOfServiceService dayOfServiceService;

    @Resource
    ConfigurationService configurationService;

    @Override
    public PrepareForServiceView checkForDiningService( Long siteId, LocalDate date, Errors errors ) {
        LOGGER.debug( "Check for dining service; siteID=" + siteId + " date=" + date );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        DiningService service = dayOfServiceService.findForSiteByServiceDate( siteId, date, true );
        if ( service == null ) {
            return null;
        }

        PrepareForServiceView response = new PrepareForServiceView( service );
        LOGGER.debug( response );

        return response;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public PrepareForServiceView prepareForService( @Valid PrepareForServiceView prepareForServiceView,
                                                    Long siteId,
                                                    Errors errors ) {
        LOGGER.debug( "Before validation: " + prepareForServiceView );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }
        return internalPrepareForService( prepareForServiceView, site, siteId, errors );
    }

    @Override
    public PrepareForServiceView internalPrepareForService( PrepareForServiceView prepareForServiceView,
                                                            Site site,
                                                            Long siteId,
                                                            Errors errors ) {
        AvailabilityMatrix matrix =
                dayOfServiceService.findAvailabilityMatrixForSite( prepareForServiceView.getAvailabilityMatrixId(),
                                                                   siteId );
        if ( matrix == null ) {
            errors.reject( "api.site.ownership", new Object[] { "availability matrix" }, null );
        }
        FloorPlan floorPlan =
                dayOfServiceService.findFloorPlanForSite( prepareForServiceView.getFloorPlanId(), siteId );
        if ( floorPlan == null ) {
            errors.reject( "api.site.ownership", new Object[] { "floor plan" }, null );
        }
        WaitStationTemplate template =
                dayOfServiceService.findWaitStationTemplateForSite( prepareForServiceView.getWaitStationTemplateId(),
                                                                    siteId );
        if ( template == null ) {
            errors.reject( "api.site.ownership", new Object[] { "wait station template" }, null );
        }
        if ( errors.hasErrors() ) {
            return null;
        }
        LOGGER.debug( prepareForServiceView );

        DiningService service = dayOfServiceService.findForSiteByServiceDate( siteId,
                                                                              prepareForServiceView.getServiceDate(),
                                                                              true );
        if ( service == null ) {
            service = DiningService.newInstance();
        }

        // Retrieve domain objects from IDs.
        List<ServerSectionView> ssView = prepareForServiceView.getScheduledServers();
        List<Long> serverIds = new ArrayList<Long>( ssView.size() );
        List<Long> sectionIds = new ArrayList<Long>( ssView.size() );
        for ( ServerSectionView serverSectionView : ssView ) {
            serverIds.add( serverSectionView.getServerId() );
            sectionIds.add( serverSectionView.getSectionId() );
        }
        Map<Long, Server> serverMap = floorPlanService.getServerMap( serverIds );
        Map<Long, Section> sectionMap = floorPlanService.getSectionMap( sectionIds );

        service.setRequestId( prepareForServiceView.getRequestId() );
        service.setServiceDate( prepareForServiceView.getServiceDate() );
        service.setSite( site );
        service.setAvailabilityMatrix( matrix );
        service.setFloorPlan( floorPlan );
        service.setWaitStationTemplate( template );
        service.setInService( true );

        Set<ScheduledServer> scheduledServers = service.getScheduledServers();
        if ( !scheduledServers.isEmpty() ) {
            scheduledServers.clear();
        }
        merchantDao.saveEntity( service );

        for ( ServerSectionView serverSectionView : ssView ) {
            ScheduledServer ss = new ScheduledServer();
            ss.setServer( serverMap.get( serverSectionView.getServerId() ) );
            ss.setSection( sectionMap.get( serverSectionView.getSectionId() ) );
            ss = service.addScheduledServer( ss );
            merchantDao.saveEntity( ss );
        }

        PrepareForServiceView response = new PrepareForServiceView( service );
        LOGGER.debug( response );

        return response;
    }

    @Override
    @Transactional
    public MessageContextView startService( Long siteId, Errors errors ) {
        LOGGER.debug( "Start dining service; siteID=" + siteId );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        // TODO - Clean this up after the NRA trade show.
        List<DiningReservation> reservations = dayOfServiceService.findAllReservationsForSite( siteId );
        for ( DiningReservation reservation : reservations ) {
            reservation.setReservationDate( LocalDate.now() );
            merchantDao.saveEntity( reservation );
        }

        List<TableService> tableServices = dayOfServiceService.findUpdatedTableService( siteId,
                                                                                        DateTime.now().minusYears( 10 ) );
        MessageContextView view = new MessageContextView();
        for ( TableService tableService : tableServices ) {
            view.addTableService( tableService );
        }

        LOGGER.debug( "Returned " + tableServices.size() + " updated reservations" );

        return view;
    }

    @Override
    public MessageContextView getServiceUpdates( Long siteId, DateTime since, Errors errors ) {
        LOGGER.debug( "Get service updates; siteID=" + siteId + " since=" + since );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        // TODO - Eventually, look in an efficient change history table to find new stuff.
        // For now just return some reservations and table status updates.
        List<TableService> tableServices = dayOfServiceService.findUpdatedTableService( siteId, since );
        MessageContextView view = new MessageContextView();
        for ( TableService tableService : tableServices ) {
            view.addTableService( tableService );
        }

        LOGGER.debug( "Returned " + tableServices.size() + " updated reservations" );

        return view;
    }

    @Override
    @Transactional
    @Valid
    public MessageContextView update( @Valid ServiceUpdateView serviceUpdateView, Errors errors ) {
        MessageContextView view = new MessageContextView();
        dayOfServiceService.updateService( serviceUpdateView, view, errors );
        return view;
    }

    @Override
    @Transactional
    @Valid
    public MessageContextView addToWaitList( @Valid DiningReservationView diningReservationView,
                                             Long siteId,
                                             Errors errors ) {
        LOGGER.debug( "Before validation: " + diningReservationView );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        LocalTime time = null;
        try {
            time = LocalTime.parse( diningReservationView.getReservationTime() );
        }
        catch ( Exception e ) {
            errors.reject( "api.parse.error", new Object[] { "reservation time", e.getMessage() }, null );
        }

        DiningReservationStatusType type = null;
        String statusType = diningReservationView.getStatus();
        if ( statusType != null ) {
            try {
                type = DiningReservationStatusType.valueOf( statusType );
            }
            catch ( IllegalArgumentException e ) {
                errors.reject( "api.reservationStatusType.not-found", new Object[] { statusType }, null );
            }
        }
        if ( errors.hasErrors() ) {
            return null;
        }

        DiningReservation reservation = new DiningReservation();
        reservation.setRequestId( diningReservationView.getRequestId() );
        reservation.setSite( site );
        reservation.setReservationDate( diningReservationView.getReservationDate() );
        reservation.setReservationTime( time );
        reservation.setDateTimeMade( DateTime.now() );
        reservation.setSeatingRequests( diningReservationView.getSeatingRequests() );
        reservation.setServiceOptions( diningReservationView.getServiceOptions() );
        reservation.setComments( diningReservationView.getComments() );
        reservation.setFirstName( diningReservationView.getFirstName() );
        reservation.setLastName( diningReservationView.getLastName() );
        reservation.setMobilePhone( diningReservationView.getMobilePhone() );
        reservation.setPartySize( diningReservationView.getPartySize() );
        Boolean flag = diningReservationView.getSpecialOccasion();
        reservation.setSpecialOccasion( flag == null ? false : flag );
        flag = diningReservationView.getRepeatGuest();
        reservation.setRepeatGuest( flag == null ? false : flag );
        flag = diningReservationView.getCancelled();
        reservation.setCancelled( flag == null ? false : flag );
        flag = diningReservationView.getNoShow();
        reservation.setNoShow( flag == null ? false : flag );
        flag = diningReservationView.getMadeToTable();
        reservation.setMadeToTable( flag == null ? false : flag );
        reservation.setStatus( type == null ? DiningReservationStatusType.WALK_IN : type );
        merchantDao.saveEntity( reservation );
        TableService tableService = TableService.newInstance();
        tableService.setReservation( reservation );
        merchantDao.saveEntity( tableService );

        LOGGER.debug( new DiningReservationView( reservation ) );
        LOGGER.debug( new TableServiceView( tableService ) );

        MessageContextView view = new MessageContextView();
        view.addTableService( tableService );
        return view;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public PrepareForServiceView close( @Valid CloseServiceView closeServiceView, Long siteId, Errors errors ) {
        LOGGER.debug( "Before validation: " + closeServiceView );
        Site site = configurationService.findSiteForMerchant( siteId, requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        DiningService service = dayOfServiceService.findDiningServiceForSite( closeServiceView.getDiningServiceId(),
                                                                              siteId );
        if ( service == null ) {
            errors.reject( "api.exception.not-found" );
            return null;
        }

        if ( !service.isInService() ) {
            errors.reject( "api.diningService.closed" );
            return null;
        }

        service.setInService( false );
        PrepareForServiceView response = new PrepareForServiceView( service );
        LOGGER.debug( response );

        return response;
    }
}
