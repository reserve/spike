package efi.unleashed.workflow.impl;

import efi.unleashed.dao.MerchantDao;
import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.domain.AuthorizedSite;
import efi.unleashed.domain.BinaryMetadata;
import efi.unleashed.domain.DiningReservationStatusType;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.FloorPlanItemType;
import efi.unleashed.domain.GraphicItemType;
import efi.unleashed.domain.Merchant;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.domain.TableServiceStatusType;
import efi.unleashed.domain.TableShapeType;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.ConfigurationService;
import efi.unleashed.service.FloorPlanService;
import efi.unleashed.service.TableAttributeService;
import efi.unleashed.view.api.ApplicationUserView;
import efi.unleashed.view.api.AssignSectionTableView;
import efi.unleashed.view.api.AssignedSectionTableView;
import efi.unleashed.view.api.AuthorizedSiteView;
import efi.unleashed.view.api.BinaryDataView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.DiningTableView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MerchantConfigurationView;
import efi.unleashed.view.api.MerchantView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionTableView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;
import efi.unleashed.workflow.ApiConfigurationWorkflow;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

@Service
public class ApiConfigurationWorkflowImpl implements ApiConfigurationWorkflow {

    private static final Logger LOGGER = Logger.getLogger( ApiConfigurationWorkflowImpl.class.getName() );

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    MerchantDao merchantDao;
    
    @Resource
    TableAttributeService tableAttributeService;

    @Resource
    FloorPlanService floorPlanService;

    @Resource
    ConfigurationService configurationService;

    // Retrieves all the configuration information for the merchant and user identified in the user context.
    @Override
    public MerchantConfigurationView getMerchantConfigurationView() {
        Merchant merchant = merchantDao.getEntity( Merchant.class, requestUserContext.getMerchantId() );
        ApplicationUser user = merchantDao.getEntity( ApplicationUser.class, requestUserContext.getUserId() );
        List<TableAttribute> attributes = tableAttributeService.findByMerchantId( requestUserContext.getMerchantId() );

        LOGGER.debug( "Get merchant configuration; merchant=" + merchant.getName() +
                      " user=" + user.getUsername() + " (" + user.getDisplayName() + ")" );

        MessageContextView messageContextView = new MessageContextView();
        ApplicationUserView userView = new ApplicationUserView( user, messageContextView );
        MerchantView merchantView = new MerchantView( merchant, messageContextView );

        MerchantConfigurationView view = new MerchantConfigurationView();
        view.setUser( userView );
        view.setMerchant( merchantView );
        view.setMessageContext( messageContextView );

        List<AuthorizedSiteView> authorizedSites = new ArrayList<AuthorizedSiteView>( merchant.getSites().size() );
        for ( AuthorizedSite authorizedSite : user.getAuthorizedSites() ) {
            authorizedSites.add( new AuthorizedSiteView( authorizedSite.getSite().getId(), authorizedSite.isAdmin() ) );
        }
        view.setAuthorizedSites( authorizedSites );

        for ( TableAttribute attribute : attributes ) {
            messageContextView.addTableAttribute( attribute );
        }
        for ( FloorPlanItemType floorPlanItemType : FloorPlanItemType.values() ) {
            messageContextView.addFloorPlanItemType( floorPlanItemType );
        }
        for ( TableShapeType tableShapeType : TableShapeType.values() ) {
            messageContextView.addTableShapeType( tableShapeType );
        }
        for ( DiningReservationStatusType diningReservationStatusType : DiningReservationStatusType.values() ) {
            messageContextView.addReservationStatusType( diningReservationStatusType );
        }
        for ( TableServiceStatusType tableServiceStatusType : TableServiceStatusType.values() ) {
            messageContextView.addTableServiceStatusType( tableServiceStatusType );
        }
        for ( GraphicItemType graphicItemType : GraphicItemType.values() ) {
            messageContextView.addGraphicItemType( graphicItemType );
        }
        return view;
    }

    @Override
    public BinaryDataView getMerchantLogo( Long binaryMetadataId, Errors errors ) {
        LOGGER.debug( "Get merchant logo; binaryMetadataID=" + binaryMetadataId );
        BinaryMetadata metadata = merchantDao.getEntity( BinaryMetadata.class, binaryMetadataId );
        if ( metadata == null ) {
            errors.reject( "api.exception.not-found" );
            return null;
        }
        return new BinaryDataView( metadata );
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public FloorPlanView addFloorPlan( @Valid FloorPlanView floorPlanView, Errors errors ) {
        LOGGER.debug( "Before validation: " + floorPlanView );
        Site site = configurationService.findSiteForMerchant( floorPlanView.getSiteId(), requestUserContext.getMerchantId() );
        if ( site == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "site" }, null );
            return null;
        }

        FloorPlan floorPlan = FloorPlan.newInstance();
        floorPlan.setRequestId( floorPlanView.getRequestId() );
        floorPlan.setName( floorPlanView.getName() );
        floorPlan.setSite( site );
        merchantDao.saveEntity( floorPlan );

        FloorPlanView response = new FloorPlanView( floorPlan );
        LOGGER.debug( response );

        return response;
    }

    @Override
    public BinaryDataView getFloorPlanDiagram( Long binaryMetadataId, Errors errors ) {
        LOGGER.debug( "Get floor plan diagram; binaryMetadataID=" + binaryMetadataId );
        BinaryMetadata metadata = merchantDao.getEntity( BinaryMetadata.class, binaryMetadataId );
        if ( metadata == null ) {
            errors.reject( "api.exception.not-found" );
            return null;
        }
        return new BinaryDataView( metadata );
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public FloorPlanItemView addFloorPlanItem( @Valid FloorPlanItemView floorPlanItemView, Errors errors ) {
        LOGGER.debug( "Before validation: " + floorPlanItemView );
        FloorPlanItemType type = getFloorPlanItemType( errors, floorPlanItemView.getItemType() );

        String graphicItemType = floorPlanItemView.getGraphicType();
        if ( type == FloorPlanItemType.GRAPHIC && StringUtils.isBlank( graphicItemType ) ) {
            errors.reject( "api.need.graphic.type" );
            return null;
        }
        GraphicItemType graphicType = null;
        if ( StringUtils.isNotBlank( graphicItemType ) ) {
            graphicType = getGraphicItemType( errors, graphicItemType );
        }

        FloorPlan floorPlan =
                configurationService.findFloorPlanForMerchant( floorPlanItemView.getFloorPlanId(),
                                                               requestUserContext.getMerchantId() );
        if ( floorPlan == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "floor plan" }, null );
        }
        if ( errors.hasErrors() ) {
            return null;
        }

        FloorPlanItem item = createFloorPlanItem( floorPlanItemView, floorPlan, type, graphicType );
        merchantDao.saveEntity( item );

        FloorPlanItemView response = new FloorPlanItemView( item );
        LOGGER.debug( response );

        return response;
    }

    private FloorPlanItemType getFloorPlanItemType( Errors errors, String itemType ) {
        FloorPlanItemType type = null;
        try {
            type = FloorPlanItemType.valueOf( itemType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.itemType.not-found", new Object[] { itemType }, null );
        }
        return type;
    }

    private GraphicItemType getGraphicItemType( Errors errors, String graphicItemType ) {
        GraphicItemType type = null;
        try {
            type = GraphicItemType.valueOf( graphicItemType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.graphicItemType.not-found", new Object[] { graphicItemType }, null );
        }
        return type;
    }

    private FloorPlanItem createFloorPlanItem( FloorPlanItemView floorPlanItemView, FloorPlan floorPlan,
                                               FloorPlanItemType type, GraphicItemType graphicType ) {
        FloorPlanItem item = new FloorPlanItem();
        item.setRequestId( floorPlanItemView.getRequestId() );
        item.setName( floorPlanItemView.getName() );
        item.setItemType( type );
        item.setGraphicType( graphicType );
        item.setX( floorPlanItemView.getX() );
        item.setY( floorPlanItemView.getY() );
        item.setWidth( floorPlanItemView.getWidth() );
        item.setHeight( floorPlanItemView.getHeight() );
        item.setRotation( floorPlanItemView.getRotation() );
        item.setFloorPlan( floorPlan );
        return item;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public TableItemView addDiningTable( @Valid TableItemView tableItemView, Errors errors ) {
        LOGGER.debug( "Before validation: " + tableItemView );
        FloorPlanItemView floorPlanItemView = tableItemView.getFloorPlanItem();
        DiningTableView diningTableView = tableItemView.getDiningTable();
        FloorPlanItemType type = getFloorPlanItemType( errors, floorPlanItemView.getItemType() );
        TableShapeType shapeType = getTableShapeType( errors, diningTableView.getShape() );
        FloorPlan floorPlan =
                configurationService.findFloorPlanForMerchant( floorPlanItemView.getFloorPlanId(),
                                                               requestUserContext.getMerchantId() );
        if ( floorPlan == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "floor plan" }, null );
        }
        
        Long attributeId = diningTableView.getAttributeId();
        TableAttribute attribute = null;
        if ( attributeId != null ) {
            attribute = configurationService.findTableAttributeForMerchant( attributeId, requestUserContext.getMerchantId() );
            if ( attribute == null ) {
                errors.reject( "api.merchant.ownership", new Object[] { "table attribute" }, null );
            }
        }
        if ( errors.hasErrors() ) {
            return null;
        }

        DiningTable table = DiningTable.newInstance();
        table.setRequestId( diningTableView.getRequestId() );
        table.setName( diningTableView.getName() );
        table.setShape( shapeType );
        table.setCapacity( diningTableView.getCapacity() );
        table.setFloorPlan( floorPlan );
        table.setAttribute( attribute );
        merchantDao.saveEntity( table );
        FloorPlanItem item = createFloorPlanItem( floorPlanItemView, floorPlan, type, null );
        item.setTable( table );
        merchantDao.saveEntity( item );

        TableItemView response = new TableItemView( table, item );
        LOGGER.debug( response );

        return response;
    }

    private TableShapeType getTableShapeType( Errors errors, String shape ) {
        TableShapeType shapeType = null;
        try {
            shapeType = TableShapeType.valueOf( shape );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.tableType.not-found", new Object[] { shape }, null );
        }
        return shapeType;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public WaitStationTemplateView addWaitStationTemplate( @Valid WaitStationTemplateView waitStationTemplateView,
                                                           Errors errors ) {
        LOGGER.debug( "Before validation: " + waitStationTemplateView );
        FloorPlan floorPlan =
                configurationService.findFloorPlanForMerchant( waitStationTemplateView.getFloorPlanId(),
                                                               requestUserContext.getMerchantId() );
        if ( floorPlan == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "floor plan" }, null );
            return null;
        }

        WaitStationTemplate template = WaitStationTemplate.newInstance();
        template.setRequestId( waitStationTemplateView.getRequestId() );
        template.setName( waitStationTemplateView.getName() );
        template.setFloorPlan( floorPlan );
        merchantDao.saveEntity( template );

        WaitStationTemplateView response = new WaitStationTemplateView( template );
        LOGGER.debug( response );

        return response;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public SectionView addSection( @Valid SectionView sectionView, Errors errors ) {
        LOGGER.debug( "Before validation: " + sectionView );
        WaitStationTemplate template =
                configurationService.findWaitStationTemplateForMerchant( sectionView.getWaitStationTemplateId(),
                                                                         requestUserContext.getMerchantId() );
        if ( template == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "wait station template" }, null );
            return null;
        }

        Section section = Section.newInstance();
        section.setRequestId( sectionView.getRequestId() );
        section.setName( sectionView.getName() );
        section.setBorderColor( sectionView.getBorderColor() );
        section.setWaitStationTemplate( template );
        merchantDao.saveEntity( section );

        SectionView response = new SectionView( section );
        LOGGER.debug( response );

        return response;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public ServerView addServer( @Valid ServerView serverView, Errors errors ) {
        LOGGER.debug( "Before validation: " + serverView );
        if ( !requestUserContext.getMerchantId().equals( serverView.getMerchantId() ) ) {
            errors.reject( "api.merchant.mismatch" );
            return null;
        }
        Merchant merchant = merchantDao.getEntity( Merchant.class, requestUserContext.getMerchantId() );

        Server server = new Server();
        server.setRequestId( serverView.getRequestId() );
        server.setFirstName( serverView.getFirstName() );
        server.setLastName( serverView.getLastName() );
        server.setAlias( serverView.getAlias() );
        server.setMerchant( merchant );
        merchantDao.saveEntity( server );

        ServerView response = new ServerView( server );
        LOGGER.debug( response );

        return response;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public AssignedSectionTableView assignSectionTables( @Valid AssignSectionTableView assignSectionTableView, Errors errors ) {
        LOGGER.debug( "Before validation: " + assignSectionTableView );
        WaitStationTemplate template = configurationService.findWaitStationTemplateForMerchant(
                assignSectionTableView.getWaitStationTemplateId(), requestUserContext.getMerchantId() );
        if ( template == null ) {
            errors.reject( "api.merchant.ownership", new Object[] { "wait station template" }, null );
        }

        // Retrieve domain objects from IDs.
        List<SectionTableView> stView = assignSectionTableView.getSectionTables();
        List<Long> sectionIds = new ArrayList<Long>( stView.size() );
        List<Long> tableIds = new ArrayList<Long>( stView.size() );
        for ( SectionTableView sectionTableView : stView ) {
            sectionIds.add( sectionTableView.getSectionId() );
            Long tableId = sectionTableView.getDiningTableId();
            if ( tableIds.contains( tableId ) ) {
                errors.reject( "api.diningTable.duplicate", new Object[] { tableId }, null );
            }
            tableIds.add( tableId );
        }

        // Bail if there are any errors before looking up IDs.
        if ( errors.hasErrors() ) {
            return null;
        }

        Map<Long, Section> sectionMap = floorPlanService.getSectionMap( sectionIds );
        Map<Long, DiningTable> tableMap = floorPlanService.getDiningTableMap( tableIds );

        // Make sure all sections match the wait station template.
        for ( Section section : sectionMap.values() ) {
            if ( !section.getWaitStationTemplate().getId().equals(
                    assignSectionTableView.getWaitStationTemplateId() ) ) {
                errors.reject( "api.section.bad-parent", new Object[] { section.getId() }, null );
            }
        }

        if ( errors.hasErrors() ) {
            return null;
        }

        // Group the tables by section.
        Map<Long, List<DiningTable>> sectionTablesMap = new HashMap<Long, List<DiningTable>>();
        for ( SectionTableView sectionTableView : stView ) {
            List<DiningTable> tableList = sectionTablesMap.get( sectionTableView.getSectionId() );
            if ( tableList == null ) {
                tableList = new ArrayList<DiningTable>();
                sectionTablesMap.put( sectionTableView.getSectionId(), tableList );
            }
            tableList.add( tableMap.get( sectionTableView.getDiningTableId() ) );
        }

        // For each section, synchronize the tables and store.
        for ( Map.Entry<Long, List<DiningTable>> sectionTables : sectionTablesMap.entrySet() ) {
            Section section = sectionMap.get( sectionTables.getKey() );
            List<DiningTable> tables = sectionTables.getValue();
            Collection<DiningTable> tablesToRemove = CollectionUtils.subtract( section.getTables(), tables );
            Collection<DiningTable> tablesToAdd = CollectionUtils.subtract( tables, section.getTables() );
            for ( DiningTable diningTable : tablesToRemove ) {
                diningTable.getSections().remove( section );
            }
            section.getTables().removeAll( tablesToRemove );
            for ( DiningTable diningTable : tablesToAdd ) {
                section.addDiningTable( diningTable );
            }
        }

        List<SectionView> sectionViews = new ArrayList<SectionView>();
        for ( Section section : sectionMap.values() ) {
            merchantDao.refreshEntity( section );
            sectionViews.add( new SectionView( section ) );
        }
        AssignedSectionTableView view = new AssignedSectionTableView();
        view.setAssignedSections( sectionViews );
        LOGGER.debug( view );

        return view;
    }

    @Override
    @Transactional
    @Valid
    @Secured( "APP_ADMIN" )
    public MessageContextView update( @Valid ConfigurationUpdateView configurationUpdateView, Errors errors ) {
        MessageContextView view = new MessageContextView();
        configurationService.updateConfiguration( configurationUpdateView, view, errors );
        return view;
    }
}
