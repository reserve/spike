package efi.unleashed.workflow;

import efi.unleashed.view.web.ReportEditView;
import efi.unleashed.view.web.ReportsView;

import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * This class defines the workflows related to reporting.
 */
public interface ReportsWorkflow {

    ReportsView getReports();

    ReportEditView prepareToEditReport( Long reportId );

    ReportEditView prepareToEditReport( String reportType );

    List<String> getDateStringsForRange( String range );

    void validate ( ReportEditView reportEditView, BindingResult result );

    void updateReport ( ReportEditView reportEditView, BindingResult result );
}
