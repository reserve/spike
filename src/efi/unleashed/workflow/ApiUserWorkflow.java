package efi.unleashed.workflow;

import efi.unleashed.view.api.ApplicationUserView;

import org.springframework.validation.Errors;

/**
 * This class defines the workflows related to an application.
 */
public interface ApiUserWorkflow {

    ApplicationUserView checkUser( Errors errors );
}
