package efi.unleashed.workflow;

import efi.unleashed.domain.Site;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.PrepareForServiceView;
import efi.unleashed.view.api.ServiceUpdateView;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.validation.Errors;

import javax.validation.Valid;

public interface ApiServiceWorkflow {

    PrepareForServiceView checkForDiningService( Long siteId, LocalDate date, Errors errors );

    @Valid
    PrepareForServiceView prepareForService( @Valid PrepareForServiceView prepareForServiceView,
                                             Long siteId,
                                             Errors errors );
    PrepareForServiceView internalPrepareForService( PrepareForServiceView prepareForServiceView,
                                                     Site site,
                                                     Long siteId,
                                                     Errors errors ); // For internal use only.

    MessageContextView startService( Long siteId, Errors errors );

    MessageContextView getServiceUpdates( Long siteId, DateTime since, Errors errors );

    @Valid
    MessageContextView update( @Valid ServiceUpdateView serviceUpdateView, Errors errors );

    @Valid
    MessageContextView addToWaitList( @Valid DiningReservationView diningReservationView, Long siteId, Errors errors );

    @Valid
    PrepareForServiceView close( @Valid CloseServiceView closeServiceView, Long siteId, Errors errors );
}
