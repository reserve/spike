package efi.unleashed.workflow;

import efi.unleashed.view.api.AssignSectionTableView;
import efi.unleashed.view.api.AssignedSectionTableView;
import efi.unleashed.view.api.BinaryDataView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MerchantConfigurationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;

import org.springframework.validation.Errors;

import javax.validation.Valid;

public interface ApiConfigurationWorkflow {

    MerchantConfigurationView getMerchantConfigurationView();

    BinaryDataView getMerchantLogo( Long binaryMetadataId, Errors errors );
    BinaryDataView getFloorPlanDiagram( Long binaryMetadataId, Errors errors );

    @Valid
    FloorPlanView addFloorPlan( @Valid FloorPlanView floorPlanView, Errors errors );

    @Valid
    FloorPlanItemView addFloorPlanItem( @Valid FloorPlanItemView floorPlanItemView, Errors errors );

    @Valid
    TableItemView addDiningTable( @Valid TableItemView tableItemView, Errors errors );

    @Valid
    WaitStationTemplateView addWaitStationTemplate( @Valid WaitStationTemplateView waitStationTemplateView,
                                                    Errors errors );

    @Valid
    SectionView addSection( @Valid SectionView sectionView, Errors errors );

    @Valid
    ServerView addServer( @Valid ServerView serverView, Errors errors );

    @Valid
    AssignedSectionTableView assignSectionTables( @Valid AssignSectionTableView assignSectionTableView, Errors errors );

    @Valid
    MessageContextView update( @Valid ConfigurationUpdateView configurationUpdateView, Errors errors );
}
