package efi.unleashed.util;

/**
 * Enumerates the available predefined date ranges.
 */
public enum DateRangeIndicator {
    CUSTOM_RANGE( "Custom Date Range"),
    CURRENT_WEEK( "Current Week"),
    CURRENT_MONTH( "Current Month"),
    CURRENT_QUARTER( "Current Quarter"),
    CURRENT_YEAR( "Current Year"),
    MONTH_TO_DATE( "Month to Date"),
    QUARTER_TO_DATE( "Quarter to Date"),
    YEAR_TO_DATE( "Year to Date"),
    LAST_WEEK( "Last Week"),
    LAST_MONTH( "Last Month"),
    LAST_QUARTER( "Last Quarter"),
    LAST_YEAR( "Last Year"),
    NEXT_WEEK( "Next Week"),
    NEXT_MONTH( "Next Month"),
    NEXT_QUARTER( "Next Quarter"),
    NEXT_YEAR( "Next Year"),
    YESTERDAY( "Yesterday"),
    TODAY( "Today"),
    TOMORROW( "Tomorrow");

    private String userString;

    DateRangeIndicator( String userString ) {
        this.userString = userString;
    }

    public String getUserString() {
        return userString;
    }

    public void setUserString( String userString ) {
        this.userString = userString;
    }

    static public DateRangeIndicator findByUserString( String s ) {
        for (DateRangeIndicator dr : DateRangeIndicator.values() ) {
            if (dr.userString.equals( s ) ) {
                return dr;
            }
        }
        return null;
    }
}
