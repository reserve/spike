package efi.unleashed.view.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReportsView implements Serializable {

    List<ReportView> reports = new ArrayList<ReportView>();

    public List<ReportView> getReports() {
        return reports;
    }
    public void setReports( List<ReportView> reports ) {
        this.reports = reports;
    }

    public static class ReportView {

        private Long id;
        private String name;
        private String type;

        public Long getId() {
            return id;
        }

        public void setId( Long id ) {
            this.id = id;
        }

        public String getName() {
            return name;
        }
        public void setName( String name ) {
            this.name = name;
        }

        public String getType() {
            return type;
        }
        public void setType( String type ) {
            this.type = type;
        }
    }
}
