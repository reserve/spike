package efi.unleashed.view.web;

import efi.unleashed.domain.DateRange;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportColumnField;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportFilterField;
import efi.unleashed.domain.ReportType;
import efi.unleashed.util.DateRangeUtils;
import efi.unleashed.view.api.EntityView;

import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.validation.constraints.NotNull;

/**
 * This class is used to pass data about a single report between the domain and the GUI.
 */
public class ReportEditView extends EntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private String dateRange;

    private String type;
    private String dateRangeFieldName;
    private String startDate;
    private String endDate;

/*
    This is used by a hidden field on the form that lists of the ids of all of the selected columns in the
    order chosen by the user
*/
    private List<String> columnIDs = new ArrayList<String>();

    //This is used to populate the available date ranges
    private List<String> allRanges = new ArrayList<String>();

    //TODO add group by fields as well

    private List<AvailableFieldGroupView> availableFieldGroups = new ArrayList<AvailableFieldGroupView>();
    private List<ColumnFieldView> columns = new ArrayList<ColumnFieldView>();
    private List<FilterFieldView> filters = new ArrayList<FilterFieldView>();

    public ReportEditView() {
    }

    public ReportEditView( Report report ) {
        super( report );
        this.name = report.getName();
        this.type = report.getReportType().getType();
        this.dateRange = report.getDateRangeField().getDateRangeIndicator().getUserString();
        this.dateRangeFieldName = report.getDateRangeField().getReportField().getFieldName();
        DateRange dateRange = DateRangeUtils.dateRangeForIndicator( report.getDateRangeField().getDateRangeIndicator() );
        this.startDate = dateRange.getStartDate().toString( "MM/dd/yyyy" );
        this.endDate = dateRange.getEndDate().toString("MM/dd/yyyy");
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange( String dateRange ) {
        this.dateRange = dateRange;
    }

    public String getDateRangeFieldName() {
        return dateRangeFieldName;
    }

    public void setDateRangeFieldName( String dateRangeFieldName ) {
        this.dateRangeFieldName = dateRangeFieldName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate( String startDate ) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate( String endDate ) {
        this.endDate = endDate;
    }

    public List<String> getColumnIDs() {
        return columnIDs;
    }

    public void setColumnIDs( List<String> columnIDs ) {
        this.columnIDs = columnIDs;
    }

    public List<String> getAllRanges() {
        return allRanges;
    }

    public void setAllRanges( List<String> allRanges ) {
        this.allRanges = allRanges;
    }

    public List<AvailableFieldGroupView> getAvailableFieldGroups() {
        return availableFieldGroups;
    }

    public void setAvailableFieldGroups( List<AvailableFieldGroupView> availableFieldGroups ) {
        this.availableFieldGroups = availableFieldGroups;
    }

    public List<ColumnFieldView> getColumns() {
        return columns;
    }

    public void setColumns( List<ColumnFieldView> columns ) {
        this.columns = columns;
    }

    public List<FilterFieldView> getFilters() {
        return filters;
    }

    public void setFilters( List<FilterFieldView> filters ) {
        this.filters = filters;
    }

    public static class AvailableFieldGroupView {
        private String name;
        List<AvailableFieldView> availableFields = new ArrayList<AvailableFieldView>();

        public String getName() {
            return name;
        }

        public void setName( String name ) {
            this.name = name;
        }

        public List<AvailableFieldView> getAvailableFields() {
            return availableFields;
        }

        public void setAvailableFields( List<AvailableFieldView> availableFields ) {
            this.availableFields = availableFields;
        }
    }

    public static class AvailableFieldView {
        private String name;
        private String fieldType;
        private String dataType;
        private Long fieldID;

        private boolean isAvailableColumn;
        private boolean isAvailableFilter;
        private boolean isAvailableGroupBy;

        public String getName() {
            return name;
        }

        public void setName( String name ) {
            this.name = name;
        }

        public String getFieldType() {
            return fieldType;
        }

        public void setFieldType( String fieldType ) {
            this.fieldType = fieldType;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType( String dataType ) {
            this.dataType = dataType;
        }

        public Long getFieldID() {
            return fieldID;
        }

        public void setFieldID( Long fieldID ) {
            this.fieldID = fieldID;
        }

        public boolean isAvailableColumn() {
            return isAvailableColumn;
        }

        public void setAvailableColumn( boolean availableColumn ) {
            isAvailableColumn = availableColumn;
        }

        public boolean isAvailableFilter() {
            return isAvailableFilter;
        }

        public void setAvailableFilter( boolean availableFilter ) {
            isAvailableFilter = availableFilter;
        }

        public boolean isAvailableGroupBy() {
            return isAvailableGroupBy;
        }

        public void setAvailableGroupBy( boolean availableGroupBy ) {
            isAvailableGroupBy = availableGroupBy;
        }

        public static Comparator<AvailableFieldView> FieldTypeNameComparator
                = new Comparator<AvailableFieldView>() {
            public int compare(AvailableFieldView view1, AvailableFieldView view2) {
                if ( view1.getFieldType().equals( view2.getFieldType() )) {
                    return view1.getName().compareTo( view2.getName() );
                }
                else {
                    return view1.getFieldType().compareTo( view2.getFieldType() );
                }
            }
        };
    }

    public static class ColumnFieldView {
        private String name;
        private String fieldType;
        private String dataType;
        private Long fieldID;
        private Integer columnOrder;

        public String getName() {
            return name;
        }

        public void setName( String name ) {
            this.name = name;
        }

        public String getFieldType() {
            return fieldType;
        }

        public void setFieldType( String fieldType ) {
            this.fieldType = fieldType;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType( String dataType ) {
            this.dataType = dataType;
        }

        public Long getFieldID() {
            return fieldID;
        }

        public void setFieldID( Long fieldID ) {
            this.fieldID = fieldID;
        }

        public Integer getColumnOrder() {
            return columnOrder;
        }

        public void setColumnOrder( Integer columnOrder ) {
            this.columnOrder = columnOrder;
        }

        public static Comparator<ColumnFieldView> ColumnOrderComparator
                = new Comparator<ColumnFieldView>() {
            public int compare( ColumnFieldView view1, ColumnFieldView view2 ) {
                return view1.getColumnOrder().compareTo( view2.getColumnOrder() );
            }
        };
    }

    public static class FilterFieldView {
        private String name;
        private String fieldType;
        private String dataType;
        private Long fieldID;
        private String operation;
        private String filterValue;

        public String getName() {
            return name;
        }

        public void setName( String name ) {
            this.name = name;
        }

        public String getFieldType() {
            return fieldType;
        }

        public void setFieldType( String fieldType ) {
            this.fieldType = fieldType;
        }

        public String getDataType() {
            return dataType;
        }

        public Long getFieldID() {
            return fieldID;
        }

        public void setFieldID( Long fieldID ) {
            this.fieldID = fieldID;
        }

        public void setDataType( String dataType ) {
            this.dataType = dataType;
        }

        public String getOperation() {
            return operation;
        }

        public void setOperation( String operation ) {
            this.operation = operation;
        }

        public String getFilterValue() {
            return filterValue;
        }

        public void setFilterValue( String filterValue ) {
            this.filterValue = filterValue;
        }
        public static Comparator<FilterFieldView> FieldTypeNameComparator
                = new Comparator<FilterFieldView>() {
            public int compare(FilterFieldView view1, FilterFieldView view2) {
                if ( view1.getFieldType().equals( view2.getFieldType() )) {
                    return view1.getName().compareTo( view2.getName() );
                }
                else {
                    return view1.getFieldType().compareTo( view2.getFieldType() );
                }
            }
        };
    }
}
