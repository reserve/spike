package efi.unleashed.view.api;

import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.AvailabilitySlot;

import java.util.ArrayList;
import java.util.List;

public class AvailabilityMatrixView extends EntityView {
    
    private String name;
    private Long siteId;
    private List<Long> slotIds;

    public AvailabilityMatrixView( AvailabilityMatrix matrix, MessageContextView messageContextView ) {
        super( matrix );
        this.name = matrix.getName();
        this.siteId = matrix.getSite().getId();
        this.slotIds = new ArrayList<Long>( matrix.getSlots().size() );
        for ( AvailabilitySlot slot : matrix.getSlots() ) {
            this.slotIds.add( slot.getId() );
            messageContextView.addAvailabilitySlot( slot );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public Long getSiteId() {
        return siteId;
    }
    public void setSiteId( Long siteId ) {
        this.siteId = siteId;
    }

    public List<Long> getSlotIds() {
        return slotIds;
    }
    public void setSlotIds( List<Long> slotIds ) {
        this.slotIds = slotIds;
    }

    @Override public String toString() {
        return "AvailabilityMatrixView {" +
               "name='" + name + '\'' +
               ", siteId=" + siteId +
               ", slotIds=" + slotIds +
               "} " + super.toString();
    }
}
