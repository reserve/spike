package efi.unleashed.view.api;

import efi.unleashed.domain.ApplicationUser;

public class ApplicationUserView extends EntityView {

    private String username;
    private String firstName;
    private String lastName;
    private String passcode;
    private Boolean isActive;
    private String displayName;

    public ApplicationUserView() {
    }

    public ApplicationUserView( ApplicationUser user ) {
        super( user );
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.passcode = user.getPasscode();
        this.isActive = user.isActive();
        this.displayName = user.getDisplayName();
    }

    // Added for consistency.
    public ApplicationUserView( ApplicationUser user, MessageContextView messageContextView ) {
        this( user );
    }

    public String getUsername() {
        return username;
    }
    public void setUsername( String username ) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    public String getPasscode() {
        return passcode;
    }
    public void setPasscode( String passcode ) {
        this.passcode = passcode;
    }

    public Boolean getActive() {
        return isActive;
    }
    public void setActive( Boolean active ) {
        isActive = active;
    }

    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName( String displayName ) {
        this.displayName = displayName;
    }

    @Override public String toString() {
        return "ApplicationUserView {" +
               "displayName='" + displayName + '\'' +
               ", firstName='" + firstName + '\'' +
               ", isActive=" + isActive +
               ", lastName='" + lastName + '\'' +
               ", passcode=" + passcode +
               ", username='" + username + '\'' +
               "} " + super.toString();
    }
}
