package efi.unleashed.view.api;

import efi.unleashed.domain.DiningReservation;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.JodaSerializers;
import org.joda.time.LocalDate;

import javax.validation.constraints.NotNull;

public class DiningReservationView extends RequestableEntityView {

    @NotNull
    @JsonSerialize(using = JodaSerializers.LocalDateSerializer.class)
    private LocalDate reservationDate;

    @NotNull
    private String reservationTime;

    private String confirmationNumber;

    private String seatingRequests;
    private String serviceOptions;
    private String comments;

    private String displayName; // read-only
    private String firstName;
    private String lastName;
    private String mobilePhone;

    @NotNull
    private Integer partySize;

    private Boolean isRepeatGuest;
    private Boolean isSpecialOccasion;
    private Boolean isCancelled;
    private Boolean isNoShow;
    private Boolean isMadeToTable;

    private String status;
    private Long siteId;
    private String graphicalIndicator; // read-only

    public DiningReservationView() {
    }

    public DiningReservationView( DiningReservation reservation ) {
        super( reservation );
        this.reservationDate = reservation.getReservationDate();
        this.reservationTime = reservation.getReservationTime().toString();
        this.confirmationNumber = reservation.getConfirmationNumber();
        this.seatingRequests = reservation.getSeatingRequests();
        this.serviceOptions = reservation.getServiceOptions();
        this.comments = reservation.getComments();
        this.displayName = reservation.getDisplayName();
        this.firstName = reservation.getFirstName();
        this.lastName = reservation.getLastName();
        this.mobilePhone = reservation.getMobilePhone();
        this.partySize = reservation.getPartySize();
        this.isRepeatGuest = reservation.isRepeatGuest();
        this.isSpecialOccasion = reservation.isSpecialOccasion();
        this.isCancelled = reservation.isCancelled();
        this.isNoShow = reservation.isNoShow();
        this.isMadeToTable = reservation.isMadeToTable();
        if ( reservation.getStatus() != null ) {
            this.status = reservation.getStatus().name();
        }
        this.siteId = reservation.getSite().getId();
        this.graphicalIndicator = reservation.getGraphicalIndicator();
    }

    public DiningReservationView( DiningReservation reservation, MessageContextView messageContextView ) {
        this( reservation );
    }

    public LocalDate getReservationDate() {
        return reservationDate;
    }
    public void setReservationDate( LocalDate reservationDate ) {
        this.reservationDate = reservationDate;
    }

    public String getReservationTime() {
        return reservationTime;
    }
    public void setReservationTime( String reservationTime ) {
        this.reservationTime = reservationTime;
    }

    public String getConfirmationNumber() {
        return confirmationNumber;
    }
    public void setConfirmationNumber( String confirmationNumber ) {
        this.confirmationNumber = confirmationNumber;
    }

    public String getSeatingRequests() {
        return seatingRequests;
    }
    public void setSeatingRequests( String seatingRequests ) {
        this.seatingRequests = seatingRequests;
    }

    public String getServiceOptions() {
        return serviceOptions;
    }
    public void setServiceOptions( String serviceOptions ) {
        this.serviceOptions = serviceOptions;
    }

    public String getComments() {
        return comments;
    }
    public void setComments( String comments ) {
        this.comments = comments;
    }

    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName( String displayName ) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }
    public void setMobilePhone( String mobilePhone ) {
        this.mobilePhone = mobilePhone;
    }

    public Integer getPartySize() {
        return partySize;
    }
    public void setPartySize( Integer partySize ) {
        this.partySize = partySize;
    }

    public Boolean getRepeatGuest() {
        return isRepeatGuest;
    }
    public void setRepeatGuest( Boolean repeatGuest ) {
        isRepeatGuest = repeatGuest;
    }

    public Boolean getSpecialOccasion() {
        return isSpecialOccasion;
    }
    public void setSpecialOccasion( Boolean specialOccasion ) {
        isSpecialOccasion = specialOccasion;
    }

    public Boolean getCancelled() {
        return isCancelled;
    }
    public void setCancelled( Boolean cancelled ) {
        isCancelled = cancelled;
    }

    public Boolean getNoShow() {
        return isNoShow;
    }
    public void setNoShow( Boolean noShow ) {
        isNoShow = noShow;
    }

    public Boolean getMadeToTable() {
        return isMadeToTable;
    }
    public void setMadeToTable( Boolean madeToTable ) {
        isMadeToTable = madeToTable;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus( String status ) {
        this.status = status;
    }

    public Long getSiteId() {
        return siteId;
    }
    public void setSiteId( Long siteId ) {
        this.siteId = siteId;
    }

    public String getGraphicalIndicator() {
        return graphicalIndicator;
    }
    public void setGraphicalIndicator( String graphicalIndicator ) {
        this.graphicalIndicator = graphicalIndicator;
    }

    @Override public String toString() {
        return "DiningReservationView {" +
               "comments='" + comments + '\'' +
               ", confirmationNumber='" + confirmationNumber + '\'' +
               ", displayName='" + displayName + '\'' +
               ", firstName='" + firstName + '\'' +
               ", graphicalIndicator='" + graphicalIndicator + '\'' +
               ", isCancelled=" + isCancelled +
               ", isMadeToTable=" + isMadeToTable +
               ", isNoShow=" + isNoShow +
               ", isRepeatGuest=" + isRepeatGuest +
               ", isSpecialOccasion=" + isSpecialOccasion +
               ", lastName='" + lastName + '\'' +
               ", mobilePhone='" + mobilePhone + '\'' +
               ", partySize=" + partySize +
               ", reservationDate=" + reservationDate +
               ", reservationTime='" + reservationTime + '\'' +
               ", seatingRequests='" + seatingRequests + '\'' +
               ", serviceOptions='" + serviceOptions + '\'' +
               ", siteId=" + siteId +
               ", status='" + status + '\'' +
               "} " + super.toString();
    }
}
