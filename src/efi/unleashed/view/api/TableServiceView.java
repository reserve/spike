package efi.unleashed.view.api;

import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.TableService;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.JodaSerializers;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class TableServiceView extends RequestableEntityView {

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime timeQuoted;

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime timeGreeted;

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime timeSeated;

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime timeArrived;

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime timeCleared;

    private String status;
    private Long reservationId;
    private List<Long> tableIds = new ArrayList<Long>();

    public TableServiceView() {
    }

    public TableServiceView( TableService tableService ) {
        super( tableService );
        this.timeQuoted = tableService.getTimeQuoted();
        this.timeGreeted = tableService.getTimeGreeted();
        this.timeSeated = tableService.getTimeSeated();
        this.timeArrived = tableService.getTimeArrived();
        this.timeCleared = tableService.getTimeCleared();
        if ( tableService.getStatus() != null ) {
            this.status = tableService.getStatus().name();
        }
        this.reservationId = tableService.getReservation().getId();
        this.tableIds = new ArrayList<Long>( tableService.getTables().size() );
        for ( DiningTable diningTable : tableService.getTables() ) {
            this.tableIds.add( diningTable.getId() );
        }
    }

    public TableServiceView( TableService tableService, MessageContextView messageContextView ) {
        this( tableService, messageContextView, true );
    }

    public TableServiceView( TableService tableService, MessageContextView messageContextView, boolean cascade ) {
        this( tableService );
        if ( cascade ) {
            messageContextView.addDiningReservation( tableService.getReservation() );
        }
    }

    public DateTime getTimeQuoted() {
        return timeQuoted;
    }
    public void setTimeQuoted( DateTime timeQuoted ) {
        this.timeQuoted = timeQuoted;
    }

    public DateTime getTimeGreeted() {
        return timeGreeted;
    }
    public void setTimeGreeted( DateTime timeGreeted ) {
        this.timeGreeted = timeGreeted;
    }

    public DateTime getTimeSeated() {
        return timeSeated;
    }
    public void setTimeSeated( DateTime timeSeated ) {
        this.timeSeated = timeSeated;
    }

    public DateTime getTimeArrived() {
        return timeArrived;
    }
    public void setTimeArrived( DateTime timeArrived ) {
        this.timeArrived = timeArrived;
    }

    public DateTime getTimeCleared() {
        return timeCleared;
    }
    public void setTimeCleared( DateTime timeCleared ) {
        this.timeCleared = timeCleared;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus( String status ) {
        this.status = status;
    }

    public Long getReservationId() {
        return reservationId;
    }
    public void setReservationId( Long reservationId ) {
        this.reservationId = reservationId;
    }

    public List<Long> getTableIds() {
        return tableIds;
    }
    public void setTableIds( List<Long> tableIds ) {
        this.tableIds = tableIds;
    }

    @Override public String toString() {
        return "TableServiceView {" +
               "reservationId=" + reservationId +
               ", status='" + status + '\'' +
               ", tableIds=" + tableIds +
               ", timeArrived=" + timeArrived +
               ", timeCleared=" + timeCleared +
               ", timeGreeted=" + timeGreeted +
               ", timeQuoted=" + timeQuoted +
               ", timeSeated=" + timeSeated +
               "} " + super.toString();
    }
}
