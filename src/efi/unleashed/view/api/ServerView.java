package efi.unleashed.view.api;

import efi.unleashed.domain.Server;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class ServerView extends RequestableEntityView {

    @Length( max = 32 )
    private String firstName;

    @NotNull
    @Length( max = 32 )
    private String lastName;

    @Length( max = 32 )
    private String alias;

    @NotNull
    private Long merchantId;

    public ServerView() {
    }

    public ServerView( Server server ) {
        super( server );
        this.firstName = server.getFirstName();
        this.lastName = server.getLastName();
        this.alias = server.getAlias();
        this.merchantId = server.getMerchant().getId();
    }

    // Added for consistency.
    public ServerView( Server server, MessageContextView messageContextView ) {
        this( server );
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    public String getAlias() {
        return alias;
    }
    public void setAlias( String alias ) {
        this.alias = alias;
    }

    public Long getMerchantId() {
        return merchantId;
    }
    public void setMerchantId( Long merchantId ) {
        this.merchantId = merchantId;
    }

    @Override public String toString() {
        return "ServerView {" +
               "alias='" + alias + '\'' +
               ", firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
               ", merchantId=" + merchantId +
               "} " + super.toString();
    }
}
