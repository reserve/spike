package efi.unleashed.view.api;

import javax.validation.constraints.NotNull;

public class ServerSectionView implements ApiView {

    @NotNull
    private Long serverId;

    @NotNull
    private Long sectionId;

    public ServerSectionView() {
    }

    public ServerSectionView( Long serverId, Long sectionId ) {
        this.serverId = serverId;
        this.sectionId = sectionId;
    }

    public Long getServerId() {
        return serverId;
    }
    public void setServerId( Long serverId ) {
        this.serverId = serverId;
    }

    public Long getSectionId() {
        return sectionId;
    }
    public void setSectionId( Long sectionId ) {
        this.sectionId = sectionId;
    }

    @Override public String toString() {
        return "ServerSectionView {" +
               "sectionId=" + sectionId +
               ", serverId=" + serverId +
               '}';
    }
}
