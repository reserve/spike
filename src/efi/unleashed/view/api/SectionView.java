package efi.unleashed.view.api;

import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.Section;

import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

public class SectionView extends RequestableEntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    @Length( max = 32 )
    private String borderColor;

    @NotNull
    private Long waitStationTemplateId;

    // TODO perhaps rename to tableIds?
    private List<Long> tables;

    public SectionView() {
    }

    public SectionView( Section section ) {
        super( section );
        this.name = section.getName();
        this.borderColor = section.getBorderColor();
        this.waitStationTemplateId = section.getWaitStationTemplate().getId();
        this.tables = new ArrayList<Long>( section.getTables().size() );
        for ( DiningTable table : section.getTables() ) {
            this.tables.add( table.getId() );
            // Don't need to add to the message context, since they'll be added from the floor plan side.
        }
    }

    // Added for consistency.
    public SectionView( Section section, MessageContextView messageContextView ) {
        this( section );
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public String getBorderColor() {
        return borderColor;
    }
    public void setBorderColor( String borderColor ) {
        this.borderColor = borderColor;
    }

    public Long getWaitStationTemplateId() {
        return waitStationTemplateId;
    }
    public void setWaitStationTemplateId( Long waitStationTemplateId ) {
        this.waitStationTemplateId = waitStationTemplateId;
    }

    public List<Long> getTables() {
        return tables;
    }
    public void setTables( List<Long> tables ) {
        this.tables = tables;
    }

    @Override public String toString() {
        return "SectionView {" +
               "borderColor='" + borderColor + '\'' +
               ", name='" + name + '\'' +
               ", tables=" + tables +
               ", waitStationTemplateId=" + waitStationTemplateId +
               "} " + super.toString();
    }
}
