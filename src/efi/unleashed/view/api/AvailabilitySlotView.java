package efi.unleashed.view.api;

import efi.unleashed.domain.AvailabilitySlot;

public class AvailabilitySlotView extends EntityView {

    private String timeSlot;
    private Integer partySize;
    private Integer numberAvailable;
    private Boolean isMaxSlot;
    private Long availabilityMatrixId;

    public AvailabilitySlotView( AvailabilitySlot slot, MessageContextView messageContextView ) {
        super( slot );
        this.timeSlot = slot.getTimeSlot();
        this.partySize = slot.getPartySize();
        this.numberAvailable = slot.getNumberAvailable();
        this.isMaxSlot = slot.isMaxSlot();
        this.availabilityMatrixId = slot.getMatrix().getId();
    }

    public String getTimeSlot() {
        return timeSlot;
    }
    public void setTimeSlot( String timeSlot ) {
        this.timeSlot = timeSlot;
    }

    public Integer getPartySize() {
        return partySize;
    }
    public void setPartySize( Integer partySize ) {
        this.partySize = partySize;
    }

    public Integer getNumberAvailable() {
        return numberAvailable;
    }
    public void setNumberAvailable( Integer numberAvailable ) {
        this.numberAvailable = numberAvailable;
    }

    public Boolean getMaxSlot() {
        return isMaxSlot;
    }
    public void setMaxSlot( Boolean maxSlot ) {
        isMaxSlot = maxSlot;
    }

    public Long getAvailabilityMatrixId() {
        return availabilityMatrixId;
    }
    public void setAvailabilityMatrixId( Long availabilityMatrixId ) {
        this.availabilityMatrixId = availabilityMatrixId;
    }

    @Override public String toString() {
        return "AvailabilitySlotView {" +
               "availabilityMatrixId=" + availabilityMatrixId +
               ", isMaxSlot=" + isMaxSlot +
               ", numberAvailable=" + numberAvailable +
               ", partySize=" + partySize +
               ", timeSlot='" + timeSlot + '\'' +
               "} " + super.toString();
    }
}
