package efi.unleashed.view.api;

import efi.unleashed.domain.RequestableEntity;

public class RequestableEntityView extends EntityView {

    private String requestId;

    public RequestableEntityView() {}

    public RequestableEntityView( RequestableEntity entity ) {
        super( entity );
        this.requestId = entity.getRequestId();
    }

    public String getRequestId() {
        return requestId;
    }
    public void setRequestId( String requestId ) {
        this.requestId = requestId;
    }

    @Override public String toString() {
        return "RequestableEntityView {" +
               "requestId='" + requestId + '\'' +
               "} " + super.toString();
    }
}
