package efi.unleashed.view.api;

/**
 * This class is used to hold the response data for an API ReST request.
 */
public class ApiResponseView implements ApiView {

    private ResponseView response;
    private ApiView content;

    public ResponseView getResponse() {
        return response;
    }
    public void setResponse( ResponseView response ) {
        this.response = response;
    }

    public ApiView getContent() {
        return content;
    }
    public void setContent( ApiView content ) {
        this.content = content;
    }
}
