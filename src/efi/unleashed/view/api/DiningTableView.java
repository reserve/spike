package efi.unleashed.view.api;

import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.Section;

import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class DiningTableView extends RequestableEntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private String shape;

    @NotNull
    @Min( value = 1 )
    private Integer capacity;

    @NotNull
    private Long floorPlanId;

    private Long attributeId;
    private List<Long> sections;

    public DiningTableView() {
    }

    public DiningTableView( DiningTable table ) {
        super( table );
        this.name = table.getName();
        this.shape = table.getShape().name();
        this.capacity = table.getCapacity();
        this.floorPlanId = table.getFloorPlan().getId();
        this.attributeId = table.getAttribute() != null ? table.getAttribute().getId() : null;
        this.sections = new ArrayList<Long>( table.getSections().size() );
        for ( Section section : table.getSections() ) {
            this.sections.add( section.getId() );
        }
    }

    public DiningTableView( DiningTable table, MessageContextView messageContextView ) {
        this( table );
        for ( Section section : table.getSections() ) {
            messageContextView.addSection( section );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public String getShape() {
        return shape;
    }
    public void setShape( String shape ) {
        this.shape = shape;
    }

    public Integer getCapacity() {
        return capacity;
    }
    public void setCapacity( Integer capacity ) {
        this.capacity = capacity;
    }

    public Long getFloorPlanId() {
        return floorPlanId;
    }
    public void setFloorPlanId( Long floorPlanId ) {
        this.floorPlanId = floorPlanId;
    }

    public Long getAttributeId() {
        return attributeId;
    }
    public void setAttributeId( Long attributeId ) {
        this.attributeId = attributeId;
    }

    public List<Long> getSections() {
        return sections;
    }
    public void setSections( List<Long> sections ) {
        this.sections = sections;
    }

    @Override public String toString() {
        return "DiningTableView {" +
               "attributeId=" + attributeId +
               ", capacity=" + capacity +
               ", floorPlanId=" + floorPlanId +
               ", name='" + name + '\'' +
               ", sections=" + sections +
               ", shape='" + shape + '\'' +
               "} " + super.toString();
    }
}
