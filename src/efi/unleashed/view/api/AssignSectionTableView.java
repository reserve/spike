package efi.unleashed.view.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class AssignSectionTableView implements ApiView {

    @NotNull
    private Long waitStationTemplateId;

    @Valid
    private List<SectionTableView> sectionTables = new ArrayList<SectionTableView>();

    public Long getWaitStationTemplateId() {
        return waitStationTemplateId;
    }
    public void setWaitStationTemplateId( Long waitStationTemplateId ) {
        this.waitStationTemplateId = waitStationTemplateId;
    }

    public List<SectionTableView> getSectionTables() {
        return sectionTables;
    }
    public void setSectionTables( List<SectionTableView> sectionTables ) {
        this.sectionTables = sectionTables;
    }

    @Override public String toString() {
        return "AssignSectionTableView {" +
               "sectionTables=" + sectionTables +
               ", waitStationTemplateId=" + waitStationTemplateId +
               '}';
    }
}
