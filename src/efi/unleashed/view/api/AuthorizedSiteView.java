package efi.unleashed.view.api;

public class AuthorizedSiteView implements ApiView {

    private Long authorizedSiteId;
    private Boolean isAdminForSite;

    public AuthorizedSiteView() {
    }

    public AuthorizedSiteView( Long authorizedSiteId, Boolean adminForSite ) {
        this.authorizedSiteId = authorizedSiteId;
        isAdminForSite = adminForSite;
    }

    public Long getAuthorizedSiteId() {
        return authorizedSiteId;
    }
    public void setAuthorizedSiteId( Long authorizedSiteId ) {
        this.authorizedSiteId = authorizedSiteId;
    }

    public Boolean getAdminForSite() {
        return isAdminForSite;
    }
    public void setAdminForSite( Boolean adminForSite ) {
        isAdminForSite = adminForSite;
    }

    @Override public String toString() {
        return "AuthorizedSiteView {" +
               "authorizedSiteId=" + authorizedSiteId +
               ", isAdminForSite=" + isAdminForSite +
               '}';
    }
}
