package efi.unleashed.view.api;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.JodaSerializers;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to hold the response metadata for an API ReST request.
 */
public class ResponseView implements ApiView {

    private Integer code;

    @JsonSerialize(using = JodaSerializers.DateTimeSerializer.class)
    private DateTime dateTime = DateTime.now();

    private List<String> messages = new ArrayList<String>();

    public Integer getCode() {
        return code;
    }
    public void setCode( Integer code ) {
        this.code = code;
    }

    public DateTime getDateTime() {
        return dateTime;
    }
    public void setDateTime( DateTime dateTime ) {
        this.dateTime = dateTime;
    }

    public List<String> getMessages() {
        return messages;
    }
    public void setMessages( List<String> messages ) {
        this.messages = messages;
    }

    public void addMessage( String message ) {
        getMessages().add( message );
    }

    @Override public String toString() {
        return "ResponseView {" +
               "code=" + code +
               ", dateTime=" + dateTime +
               ", messages=" + messages +
               '}';
    }
}
