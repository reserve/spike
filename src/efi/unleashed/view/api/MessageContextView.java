package efi.unleashed.view.api;

import efi.platform.util.StringUtils;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.AvailabilitySlot;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningReservationStatusType;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.FloorPlanItemType;
import efi.unleashed.domain.GraphicItemType;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.domain.TableService;
import efi.unleashed.domain.TableServiceStatusType;
import efi.unleashed.domain.TableShapeType;
import efi.unleashed.domain.WaitStationTemplate;

import java.util.HashMap;
import java.util.Map;

public class MessageContextView implements ApiView {

    private Map<String, String> floorPlanItemTypeMap = new HashMap<String, String>();
    private Map<String, String> tableShapeTypeMap = new HashMap<String, String>();
    private Map<String, String> reservationStatusTypeMap = new HashMap<String, String>();
    private Map<String, String> tableServiceStatusTypeMap = new HashMap<String, String>();
    private Map<String, String> graphicItemTypeMap = new HashMap<String, String>();

    private Map<Long, AvailabilityMatrixView> availabilityMatrixMap = new HashMap<Long, AvailabilityMatrixView>();
    private Map<Long, AvailabilitySlotView> availabilitySlots = new HashMap<Long, AvailabilitySlotView>();
    private Map<Long, DiningTableView> tables = new HashMap<Long, DiningTableView>();
    private Map<Long, FloorPlanView> floorPlans = new HashMap<Long, FloorPlanView>();
    private Map<Long, FloorPlanItemView> floorPlanItems = new HashMap<Long, FloorPlanItemView>();
    private Map<Long, SectionView> sections = new HashMap<Long, SectionView>();
    private Map<Long, ServerView> servers = new HashMap<Long, ServerView>();
    private Map<Long, SiteView> sites = new HashMap<Long, SiteView>();
    private Map<Long, TableAttributeView> tableAttributes = new HashMap<Long, TableAttributeView>();
    private Map<Long, WaitStationTemplateView> waitStationTemplates = new HashMap<Long, WaitStationTemplateView>();

    private Map<Long, TableServiceView> tableServiceMap = new HashMap<Long, TableServiceView>();
    private Map<Long, DiningReservationView> diningReservations = new HashMap<Long, DiningReservationView>();

    public String addFloorPlanItemType( FloorPlanItemType type ) {
        String typeName = type.name();
        if ( !floorPlanItemTypeMap.containsKey( typeName ) ) {
            floorPlanItemTypeMap.put( typeName, StringUtils.toUserPresentable( type ) );
        }
        return typeName;
    }

    public String addTableShapeType( TableShapeType type ) {
        String typeName = type.name();
        if ( !tableShapeTypeMap.containsKey( typeName ) ) {
            tableShapeTypeMap.put( typeName, StringUtils.toUserPresentable( type ) );
        }
        return typeName;
    }

    public String addReservationStatusType( DiningReservationStatusType type ) {
        String typeName = type.name();
        if ( !reservationStatusTypeMap.containsKey( typeName ) ) {
            reservationStatusTypeMap.put( typeName, StringUtils.toUserPresentable( type ) );
        }
        return typeName;
    }

    public String addTableServiceStatusType( TableServiceStatusType type ) {
        String typeName = type.name();
        if ( !tableServiceStatusTypeMap.containsKey( typeName ) ) {
            tableServiceStatusTypeMap.put( typeName, StringUtils.toUserPresentable( type ) );
        }
        return typeName;
    }

    public String addGraphicItemType( GraphicItemType type ) {
        String typeName = type.name();
        if ( !graphicItemTypeMap.containsKey( typeName ) ) {
            graphicItemTypeMap.put( typeName, StringUtils.toUserPresentable( type ) );
        }
        return typeName;
    }

    public Long addAvailabilityMatrix( AvailabilityMatrix matrix ) {
        Long id = matrix.getId();
        if ( !availabilityMatrixMap.containsKey( id ) ) {
            availabilityMatrixMap.put( id, new AvailabilityMatrixView( matrix, this ) );
        }
        return id;
    }

    public Long addAvailabilitySlot( AvailabilitySlot slot ) {
        Long id = slot.getId();
        if ( !availabilitySlots.containsKey( id ) ) {
            availabilitySlots.put( id, new AvailabilitySlotView( slot, this ) );
        }
        return id;
    }

    public Long addDiningTable( DiningTable table ) {
        Long id = table.getId();
        if ( !tables.containsKey( id ) ) {
            tables.put( id, new DiningTableView( table, this ) );
        }
        return id;
    }

    public Long addFloorPlan( FloorPlan floorPlan ) {
        Long id = floorPlan.getId();
        if ( !floorPlans.containsKey( id ) ) {
            floorPlans.put( id, new FloorPlanView( floorPlan, this ) );
        }
        return id;
    }

    public Long addFloorPlanItem( FloorPlanItem floorPlanItem ) {
        Long id = floorPlanItem.getId();
        if ( !floorPlanItems.containsKey( id ) ) {
            floorPlanItems.put( id, new FloorPlanItemView( floorPlanItem, this ) );
        }
        return id;
    }

    public Long addSection( Section section ) {
        Long id = section.getId();
        if ( !sections.containsKey( id ) ) {
            sections.put( id, new SectionView( section, this ) );
        }
        return id;
    }

    public Long addServer( Server server ) {
        Long id = server.getId();
        if ( !servers.containsKey( id ) ) {
            servers.put( id, new ServerView( server, this ) );
        }
        return id;
    }

    public Long addSite( Site site ) {
        Long id = site.getId();
        if ( !sites.containsKey( id ) ) {
            sites.put( id, new SiteView( site, this ) );
        }
        return id;
    }

    public Long addTableAttribute( TableAttribute attribute ) {
        Long id = attribute.getId();
        if ( !tableAttributes.containsKey( id ) ) {
            tableAttributes.put( id, new TableAttributeView( attribute, this ) );
        }
        return id;
    }

    public Long addWaitStationTemplate( WaitStationTemplate template ) {
        Long id = template.getId();
        if ( !waitStationTemplates.containsKey( id ) ) {
            waitStationTemplates.put( id, new WaitStationTemplateView( template, this ) );
        }
        return id;
    }

    public Long addTableService( TableService tableService ) {
        return addTableService( tableService, true );
    }

    public Long addTableService( TableService tableService, boolean cascade ) {
        Long id = tableService.getId();
        if ( !tableServiceMap.containsKey( id ) ) {
            tableServiceMap.put( id, new TableServiceView( tableService, this, cascade ) );
        }
        return id;
    }

    public Long addDiningReservation( DiningReservation reservation ) {
        Long id = reservation.getId();
        if ( !diningReservations.containsKey( id ) ) {
            diningReservations.put( id, new DiningReservationView( reservation, this ) );
        }
        return id;
    }

    public Map<String, String> getFloorPlanItemTypeMap() {
        return floorPlanItemTypeMap;
    }

    public Map<String, String> getTableShapeTypeMap() {
        return tableShapeTypeMap;
    }

    public Map<String, String> getReservationStatusTypeMap() {
        return reservationStatusTypeMap;
    }

    public Map<String, String> getTableServiceStatusTypeMap() {
        return tableServiceStatusTypeMap;
    }

    public Map<String, String> getGraphicItemTypeMap() {
        return graphicItemTypeMap;
    }

    public Map<Long, AvailabilityMatrixView> getAvailabilityMatrixMap() {
        return availabilityMatrixMap;
    }

    public Map<Long, AvailabilitySlotView> getAvailabilitySlots() {
        return availabilitySlots;
    }

    public Map<Long, DiningTableView> getTables() {
        return tables;
    }

    public Map<Long, FloorPlanView> getFloorPlans() {
        return floorPlans;
    }

    public Map<Long, FloorPlanItemView> getFloorPlanItems() {
        return floorPlanItems;
    }

    public Map<Long, SectionView> getSections() {
        return sections;
    }

    public Map<Long, ServerView> getServers() {
        return servers;
    }

    public Map<Long, SiteView> getSites() {
        return sites;
    }

    public Map<Long, TableAttributeView> getTableAttributes() {
        return tableAttributes;
    }

    public Map<Long, WaitStationTemplateView> getWaitStationTemplates() {
        return waitStationTemplates;
    }

    public Map<Long, TableServiceView> getTableServiceMap() {
        return tableServiceMap;
    }

    public Map<Long, DiningReservationView> getDiningReservations() {
        return diningReservations;
    }
}
