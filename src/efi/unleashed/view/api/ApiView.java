package efi.unleashed.view.api;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.io.Serializable;

/**
 * This interface is a marker interface to help identify views used in the API ReST layer.
 */
@JsonTypeInfo( use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class" )
public interface ApiView extends Serializable {
}
