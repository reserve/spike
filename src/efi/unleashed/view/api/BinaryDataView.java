package efi.unleashed.view.api;

import efi.unleashed.domain.BinaryMetadata;

/**
 * This class is the view representation of any binary data element.
 * Most methods delegate to an instance of the diagram binary metadata.
 */
public class BinaryDataView implements ApiView {

    private BinaryMetadata binaryMetadata;

    public BinaryDataView( BinaryMetadata binaryMetadata ) {
        this.binaryMetadata = binaryMetadata;
    }
    
    public String getName() {
        return binaryMetadata.getName();
    }
    
    public String getContentType() {
        return binaryMetadata.getContentType();
    }
    
    public Long getSize() {
        return binaryMetadata.getSize();
    }

    public byte[] getData() {
        return binaryMetadata.getBinaryData().getData();
    }
}
