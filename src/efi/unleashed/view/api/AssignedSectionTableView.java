package efi.unleashed.view.api;

import java.util.ArrayList;
import java.util.List;

public class AssignedSectionTableView implements ApiView {

    private List<SectionView> assignedSections = new ArrayList<SectionView>();

    public List<SectionView> getAssignedSections() {
        return assignedSections;
    }
    public void setAssignedSections( List<SectionView> assignedSections ) {
        this.assignedSections = assignedSections;
    }

    @Override public String toString() {
        return "AssignedSectionTableView {" +
               "assignedSections=" + assignedSections +
               '}';
    }
}
