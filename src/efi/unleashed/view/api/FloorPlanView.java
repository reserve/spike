package efi.unleashed.view.api;

import efi.unleashed.domain.BinaryMetadata;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.WaitStationTemplate;

import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

public class FloorPlanView extends RequestableEntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private Long siteId;

    private String diagramUrl;
    private List<Long> waitStationTemplateIds;
    private List<Long> tableIds;
    private List<Long> floorPlanItemIds;

    public FloorPlanView() {
    }

    public FloorPlanView( FloorPlan floorPlan ) {
        super( floorPlan );
        this.name = floorPlan.getName();
        this.siteId = floorPlan.getSite().getId();
        BinaryMetadata binaryMetadata = floorPlan.getDiagram();
        if ( binaryMetadata != null ) {
            this.diagramUrl = "/api/config/floorplan/diagram/" + binaryMetadata.getId();
        }
        this.waitStationTemplateIds = new ArrayList<Long>( floorPlan.getWaitStationTemplates().size() );
        for ( WaitStationTemplate waitStationTemplate : floorPlan.getWaitStationTemplates() ) {
            this.waitStationTemplateIds.add( waitStationTemplate.getId() );
        }
        this.tableIds = new ArrayList<Long>( floorPlan.getTables().size() );
        for ( DiningTable table : floorPlan.getTables() ) {
            this.tableIds.add( table.getId() );
        }
        this.floorPlanItemIds = new ArrayList<Long>( floorPlan.getFloorPlanItems().size() );
        for ( FloorPlanItem floorPlanItem : floorPlan.getFloorPlanItems() ) {
            this.floorPlanItemIds.add( floorPlanItem.getId() );
        }
    }

    public FloorPlanView( FloorPlan floorPlan, MessageContextView messageContextView ) {
        this( floorPlan );
        for ( WaitStationTemplate waitStationTemplate : floorPlan.getWaitStationTemplates() ) {
            messageContextView.addWaitStationTemplate( waitStationTemplate );
        }
        for ( DiningTable table : floorPlan.getTables() ) {
            messageContextView.addDiningTable( table );
        }
        for ( FloorPlanItem floorPlanItem : floorPlan.getFloorPlanItems() ) {
            messageContextView.addFloorPlanItem( floorPlanItem );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public String getDiagramUrl() {
        return diagramUrl;
    }
    public void setDiagramUrl( String diagramUrl ) {
        this.diagramUrl = diagramUrl;
    }

    public Long getSiteId() {
        return siteId;
    }
    public void setSiteId( Long siteId ) {
        this.siteId = siteId;
    }

    public List<Long> getWaitStationTemplateIds() {
        return waitStationTemplateIds;
    }
    public void setWaitStationTemplateIds( List<Long> waitStationTemplateIds ) {
        this.waitStationTemplateIds = waitStationTemplateIds;
    }

    public List<Long> getTableIds() {
        return tableIds;
    }
    public void setTableIds( List<Long> tableIds ) {
        this.tableIds = tableIds;
    }

    public List<Long> getFloorPlanItemIds() {
        return floorPlanItemIds;
    }
    public void setFloorPlanItemIds( List<Long> floorPlanItemIds ) {
        this.floorPlanItemIds = floorPlanItemIds;
    }

    @Override public String toString() {
        return "FloorPlanView {" +
               "diagramUrl='" + diagramUrl + '\'' +
               ", floorPlanItemIds=" + floorPlanItemIds +
               ", name='" + name + '\'' +
               ", siteId=" + siteId +
               ", tableIds=" + tableIds +
               ", waitStationTemplateIds=" + waitStationTemplateIds +
               "} " + super.toString();
    }
}
