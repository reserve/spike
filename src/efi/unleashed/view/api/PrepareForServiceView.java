package efi.unleashed.view.api;

import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.ScheduledServer;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ext.JodaSerializers;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

public class PrepareForServiceView extends RequestableEntityView {

    private Long siteId;
    private Boolean isInService;

    @NotNull
    @JsonSerialize(using = JodaSerializers.LocalDateSerializer.class)
    private LocalDate serviceDate;

    @NotNull
    private Long availabilityMatrixId;

    @NotNull
    private Long floorPlanId;

    @NotNull
    private Long waitStationTemplateId;

    private List<ServerSectionView> scheduledServers = new ArrayList<ServerSectionView>();

    public PrepareForServiceView() {
    }

    public PrepareForServiceView( DiningService diningService ) {
        super( diningService );
        this.siteId = diningService.getSite().getId();
        this.isInService = diningService.isInService();
        this.serviceDate = diningService.getServiceDate();
        this.availabilityMatrixId = diningService.getAvailabilityMatrix().getId();
        this.floorPlanId = diningService.getFloorPlan().getId();
        this.waitStationTemplateId = diningService.getWaitStationTemplate().getId();
        for ( ScheduledServer scheduledServer : diningService.getScheduledServers() ) {
            scheduledServers.add( new ServerSectionView( scheduledServer.getServer().getId(),
                                                         scheduledServer.getSection().getId() ) );
        }
    }

    public Long getSiteId() {
        return siteId;
    }
    public void setSiteId( Long siteId ) {
        this.siteId = siteId;
    }

    public Boolean getInService() {
        return isInService;
    }
    public void setInService( Boolean inService ) {
        isInService = inService;
    }

    public LocalDate getServiceDate() {
        return serviceDate;
    }
    public void setServiceDate( LocalDate serviceDate ) {
        this.serviceDate = serviceDate;
    }

    public Long getAvailabilityMatrixId() {
        return availabilityMatrixId;
    }
    public void setAvailabilityMatrixId( Long availabilityMatrixId ) {
        this.availabilityMatrixId = availabilityMatrixId;
    }

    public Long getFloorPlanId() {
        return floorPlanId;
    }
    public void setFloorPlanId( Long floorPlanId ) {
        this.floorPlanId = floorPlanId;
    }

    public Long getWaitStationTemplateId() {
        return waitStationTemplateId;
    }
    public void setWaitStationTemplateId( Long waitStationTemplateId ) {
        this.waitStationTemplateId = waitStationTemplateId;
    }

    public List<ServerSectionView> getScheduledServers() {
        return scheduledServers;
    }
    public void setScheduledServers( List<ServerSectionView> scheduledServers ) {
        this.scheduledServers = scheduledServers;
    }

    @Override public String toString() {
        return "PrepareForServiceView {" +
               "availabilityMatrixId=" + availabilityMatrixId +
               ", floorPlanId=" + floorPlanId +
               ", isInService=" + isInService +
               ", scheduledServers=" + scheduledServers +
               ", serviceDate=" + serviceDate +
               ", siteId=" + siteId +
               ", waitStationTemplateId=" + waitStationTemplateId +
               "} " + super.toString();
    }
}
