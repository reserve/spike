package efi.unleashed.view.api;

import javax.validation.constraints.NotNull;

public class CloseServiceView implements ApiView {

    @NotNull
    private Long diningServiceId;

    public Long getDiningServiceId() {
        return diningServiceId;
    }
    public void setDiningServiceId( Long diningServiceId ) {
        this.diningServiceId = diningServiceId;
    }

    @Override public String toString() {
        return "CloseServiceView {" +
               "diningServiceId=" + diningServiceId +
               '}';
    }
}
