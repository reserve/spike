package efi.unleashed.view.api;

import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.GraphicItemType;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class FloorPlanItemView extends RequestableEntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private String itemType;
    private String graphicType;

    @Min( value = 0 )
    private Integer x = 0;

    @Min( value = 0 )
    private Integer y = 0;

    @Min( value = 0 )
    private Integer width = 0;

    @Min( value = 0 )
    private Integer height = 0;

    @Min( value = -359 )
    @Max( value = 359 )
    private Integer rotation = 0;

    private Long tableId;

    @NotNull
    private Long floorPlanId;

    public FloorPlanItemView() {
    }

    public FloorPlanItemView( FloorPlanItem item ) {
        super( item );
        this.name = item.getName();
        this.itemType = item.getItemType().name();
        GraphicItemType graphicType = item.getGraphicType();
        if ( graphicType != null ) {
            this.graphicType = graphicType.name();
        }
        this.x = item.getX();
        this.y = item.getY();
        this.width = item.getWidth();
        this.height = item.getHeight();
        this.rotation = item.getRotation();
        DiningTable table = item.getTable();
        if ( table != null ) {
            this.tableId = item.getTable().getId();
        }
        this.floorPlanId = item.getFloorPlan().getId();
    }

    // Added for consistency.
    public FloorPlanItemView( FloorPlanItem item, MessageContextView messageContextView ) {
        this( item );
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public String getItemType() {
        return itemType;
    }
    public void setItemType( String itemType ) {
        this.itemType = itemType;
    }

    public String getGraphicType() {
        return graphicType;
    }
    public void setGraphicType( String graphicType ) {
        this.graphicType = graphicType;
    }

    public Integer getX() {
        return x;
    }
    public void setX( Integer x ) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }
    public void setY( Integer y ) {
        this.y = y;
    }

    public Integer getWidth() {
        return width;
    }
    public void setWidth( Integer width ) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }
    public void setHeight( Integer height ) {
        this.height = height;
    }

    public Integer getRotation() {
        return rotation;
    }
    public void setRotation( Integer rotation ) {
        this.rotation = rotation;
    }

    public Long getTableId() {
        return tableId;
    }
    public void setTableId( Long tableId ) {
        this.tableId = tableId;
    }

    public Long getFloorPlanId() {
        return floorPlanId;
    }
    public void setFloorPlanId( Long floorPlanId ) {
        this.floorPlanId = floorPlanId;
    }

    @Override public String toString() {
        return "FloorPlanItemView {" +
               "floorPlanId=" + floorPlanId +
               ", graphicType='" + graphicType + '\'' +
               ", height=" + height +
               ", itemType='" + itemType + '\'' +
               ", name='" + name + '\'' +
               ", rotation=" + rotation +
               ", tableId=" + tableId +
               ", width=" + width +
               ", x=" + x +
               ", y=" + y +
               "} " + super.toString();
    }
}
