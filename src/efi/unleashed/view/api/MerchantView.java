package efi.unleashed.view.api;

import efi.unleashed.domain.BinaryMetadata;
import efi.unleashed.domain.Merchant;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;

import java.util.ArrayList;
import java.util.List;

public class MerchantView extends EntityView {

    private String name;
    private String shortName;
    private String logoUrl;
    private List<Long> serverIds;
    private List<Long> siteIds;

    public MerchantView( Merchant merchant, MessageContextView messageContextView ) {
        super( merchant );
        this.name = merchant.getName();
        this.shortName = merchant.getShortName();
        BinaryMetadata binaryMetadata = merchant.getLogo();
        if ( binaryMetadata != null ) {
            this.logoUrl = "/api/config/merchant/logo/" + binaryMetadata.getId();
        }
        this.serverIds = new ArrayList<Long>( merchant.getServers().size() );
        for ( Server server : merchant.getServers() ) {
            this.serverIds.add( server.getId() );
            messageContextView.addServer( server );
        }
        this.siteIds = new ArrayList<Long>( merchant.getSites().size() );
        for ( Site site : merchant.getSites() ) {
            this.siteIds.add( site.getId() );
            messageContextView.addSite( site );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }
    public void setShortName( String shortName ) {
        this.shortName = shortName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }
    public void setLogoUrl( String logoUrl ) {
        this.logoUrl = logoUrl;
    }

    public List<Long> getServerIds() {
        return serverIds;
    }
    public void setServerIds( List<Long> serverIds ) {
        this.serverIds = serverIds;
    }

    public List<Long> getSiteIds() {
        return siteIds;
    }
    public void setSiteIds( List<Long> siteIds ) {
        this.siteIds = siteIds;
    }

    @Override public String toString() {
        return "MerchantView {" +
               "logoUrl='" + logoUrl + '\'' +
               ", name='" + name + '\'' +
               ", serverIds=" + serverIds +
               ", shortName='" + shortName + '\'' +
               ", siteIds=" + siteIds +
               "} " + super.toString();
    }
}
