package efi.unleashed.view.api;

import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.Site;

import java.util.ArrayList;
import java.util.List;

public class SiteView extends EntityView {

    private String name;
    private Long merchantId;
    private List<Long> floorPlanIds;
    private List<Long> availabilityMatrixIds;

    public SiteView( Site site, MessageContextView messageContextView ) {
        super( site );
        this.name = site.getName();
        this.merchantId = site.getMerchant().getId();
        this.floorPlanIds = new ArrayList<Long>( site.getFloorPlans().size() );
        for ( FloorPlan floorPlan : site.getFloorPlans() ) {
            this.floorPlanIds.add( floorPlan.getId() );
            messageContextView.addFloorPlan( floorPlan );
        }
        this.availabilityMatrixIds = new ArrayList<Long>( site.getAvailabilityMatrixList().size() );
        for ( AvailabilityMatrix matrix : site.getAvailabilityMatrixList() ) {
            this.availabilityMatrixIds.add( matrix.getId() );
            messageContextView.addAvailabilityMatrix( matrix );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public Long getMerchantId() {
        return merchantId;
    }
    public void setMerchantId( Long merchantId ) {
        this.merchantId = merchantId;
    }

    public List<Long> getFloorPlanIds() {
        return floorPlanIds;
    }
    public void setFloorPlanIds( List<Long> floorPlanIds ) {
        this.floorPlanIds = floorPlanIds;
    }

    public List<Long> getAvailabilityMatrixIds() {
        return availabilityMatrixIds;
    }
    public void setAvailabilityMatrixIds( List<Long> availabilityMatrixIds ) {
        this.availabilityMatrixIds = availabilityMatrixIds;
    }

    @Override public String toString() {
        return "SiteView {" +
               "availabilityMatrixIds=" + availabilityMatrixIds +
               ", floorPlanIds=" + floorPlanIds +
               ", merchantId=" + merchantId +
               ", name='" + name + '\'' +
               "} " + super.toString();
    }
}
