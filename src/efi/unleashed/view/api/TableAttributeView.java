package efi.unleashed.view.api;

import efi.unleashed.domain.TableAttribute;

public class TableAttributeView extends EntityView {

    private String name;
    private Long merchantId;

    public TableAttributeView( TableAttribute attribute, MessageContextView messageContextView ) {
        super( attribute );
        this.name = attribute.getName();
        this.merchantId = attribute.getMerchant().getId();
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public Long getMerchantId() {
        return merchantId;
    }
    public void setMerchantId( Long merchantId ) {
        this.merchantId = merchantId;
    }

    @Override public String toString() {
        return "TableAttributeView {" +
               "merchantId=" + merchantId +
               ", name='" + name + '\'' +
               "} " + super.toString();
    }
}
