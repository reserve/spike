package efi.unleashed.view.api;

import javax.validation.constraints.NotNull;

public class SectionTableView implements ApiView {

    @NotNull
    private Long sectionId;

    @NotNull
    private Long diningTableId;

    public SectionTableView() {
    }

    public SectionTableView( Long sectionId, Long diningTableId ) {
        this.sectionId = sectionId;
        this.diningTableId = diningTableId;
    }

    public Long getSectionId() {
        return sectionId;
    }
    public void setSectionId( Long sectionId ) {
        this.sectionId = sectionId;
    }

    public Long getDiningTableId() {
        return diningTableId;
    }
    public void setDiningTableId( Long diningTableId ) {
        this.diningTableId = diningTableId;
    }

    @Override public String toString() {
        return "SectionTableView {" +
               "diningTableId=" + diningTableId +
               ", sectionId=" + sectionId +
               '}';
    }
}
