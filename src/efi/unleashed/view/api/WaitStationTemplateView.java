package efi.unleashed.view.api;

import efi.unleashed.domain.Section;
import efi.unleashed.domain.WaitStationTemplate;

import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

public class WaitStationTemplateView extends RequestableEntityView {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private Long floorPlanId;

    private List<Long> sections;

    public WaitStationTemplateView() {
    }

    public WaitStationTemplateView( WaitStationTemplate template ) {
        super( template );
        this.name = template.getName();
        this.floorPlanId = template.getFloorPlan().getId();
        this.sections = new ArrayList<Long>( template.getSections().size() );
        for ( Section section : template.getSections() ) {
            this.sections.add( section.getId() );
        }
    }

    public WaitStationTemplateView( WaitStationTemplate template, MessageContextView messageContextView ) {
        this( template );
        for ( Section section : template.getSections() ) {
            messageContextView.addSection( section );
        }
    }

    public String getName() {
        return name;
    }
    public void setName( String name ) {
        this.name = name;
    }

    public Long getFloorPlanId() {
        return floorPlanId;
    }
    public void setFloorPlanId( Long floorPlanId ) {
        this.floorPlanId = floorPlanId;
    }

    public List<Long> getSections() {
        return sections;
    }
    public void setSections( List<Long> sections ) {
        this.sections = sections;
    }

    @Override public String toString() {
        return "WaitStationTemplateView {" +
               "floorPlanId=" + floorPlanId +
               ", name='" + name + '\'' +
               ", sections=" + sections +
               "} " + super.toString();
    }
}
