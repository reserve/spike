package efi.unleashed.view.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

public class ServiceUpdateView implements ApiView {

    @Valid
    private List<ApiView> updates = new ArrayList<ApiView>();

    public List<ApiView> getUpdates() {
        return updates;
    }
    public void setUpdates( List<ApiView> updates ) {
        this.updates = updates;
    }
}
