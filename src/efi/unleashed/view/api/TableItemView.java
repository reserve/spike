package efi.unleashed.view.api;

import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlanItem;

import javax.validation.Valid;

public class TableItemView implements ApiView {

    @Valid
    private DiningTableView diningTable;

    @Valid
    private FloorPlanItemView floorPlanItem;

    public TableItemView() {
    }

    public TableItemView( DiningTable table, FloorPlanItem item ) {
        this.diningTable = new DiningTableView( table );
        this.floorPlanItem = new FloorPlanItemView( item );
    }

    public DiningTableView getDiningTable() {
        return diningTable;
    }
    public void setDiningTable( DiningTableView diningTable ) {
        this.diningTable = diningTable;
    }

    public FloorPlanItemView getFloorPlanItem() {
        return floorPlanItem;
    }
    public void setFloorPlanItem( FloorPlanItemView floorPlanItem ) {
        this.floorPlanItem = floorPlanItem;
    }

    @Override public String toString() {
        return "TableItemView {" +
               "diningTable=" + diningTable +
               ", floorPlanItem=" + floorPlanItem +
               '}';
    }
}
