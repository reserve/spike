package efi.unleashed.view.api;

import java.util.List;

public class MerchantConfigurationView implements ApiView {

    private MessageContextView messageContext;
    private ApplicationUserView user;
    private MerchantView merchant;
    private List<AuthorizedSiteView> authorizedSites;

    public MessageContextView getMessageContext() {
        return messageContext;
    }
    public void setMessageContext( MessageContextView messageContext ) {
        this.messageContext = messageContext;
    }

    public ApplicationUserView getUser() {
        return user;
    }
    public void setUser( ApplicationUserView user ) {
        this.user = user;
    }

    public MerchantView getMerchant() {
        return merchant;
    }
    public void setMerchant( MerchantView merchant ) {
        this.merchant = merchant;
    }

    public List<AuthorizedSiteView> getAuthorizedSites() {
        return authorizedSites;
    }
    public void setAuthorizedSites( List<AuthorizedSiteView> authorizedSites ) {
        this.authorizedSites = authorizedSites;
    }
}
