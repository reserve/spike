package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.TableService;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.ServiceUpdateView;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.validation.Errors;

import java.util.List;

/**
 * This interface defines services needed during the day of service.
 */
public interface DayOfServiceService extends GenericService {

    AvailabilityMatrix findAvailabilityMatrixForSite( Long availabilityMatrixId, Long siteId );

    DiningReservation findDiningReservationForMerchant( Long diningReservationId, Long merchantId );

    DiningService findDiningServiceForSite( Long diningServiceId, Long siteId );
    DiningService findForSiteByServiceDate( Long siteId, LocalDate serviceDate, boolean isInService );

    FloorPlan findFloorPlanForSite( Long floorPlanId, Long siteId );

    TableService findTableServiceForMerchant( Long tableServiceId, Long merchantId );

    WaitStationTemplate findWaitStationTemplateForSite( Long templateId, Long siteId );

    List<TableService> findUpdatedTableService( Long siteId, DateTime since );

    List<DiningReservation> findAllReservationsForSite( Long siteId );

    void updateService( ServiceUpdateView serviceUpdateView, MessageContextView messageContextView, Errors errors );
}
