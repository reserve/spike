package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.TableAttribute;

import java.util.List;

/**
 * This interface is the service layer for managing table attributes.
 */
public interface TableAttributeService extends GenericService {

    List<TableAttribute> findByMerchantId( Long merchantId );
    List<TableAttribute> findSystemAttributes();
}
