package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.MessageContextView;

import org.springframework.validation.Errors;

/**
 * This interface defines services needed for configuration.
 */
public interface ConfigurationService extends GenericService {

    Site findSiteForMerchant( Long siteId, Long merchantId );

    FloorPlan findFloorPlanForMerchant( Long floorPlanId, Long merchantId );

    FloorPlanItem findFloorPlanItemForMerchant( Long floorPlanItemId, Long merchantId );

    TableAttribute findTableAttributeForMerchant( Long attributeId, Long merchantId );

    WaitStationTemplate findWaitStationTemplateForMerchant( Long waitStationTemplateId, Long merchantId );

    Section findSectionForMerchant( Long sectionId, Long merchantId );

    Server findServerForMerchant( Long serverId, Long merchantId );

    DiningTable findDiningTableForMerchant( Long tableId, Long merchantId );

    void updateConfiguration( ConfigurationUpdateView configurationUpdateView,
                              MessageContextView messageContextView,
                              Errors errors );
}
