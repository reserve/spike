package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.EventType;

import java.util.List;

/**
 * This interface defines services used in managing events.
 */
public interface EventService extends GenericService {

    List<Event> findEventsForMerchant( Long merchantId );

    List<Event> findEventsForEventType( Long merchantId, String eventTypeString );

    List<EventType> findEventTypesForMerchant( Long merchantId );
}
