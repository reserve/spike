package efi.unleashed.service;

import efi.platform.service.GenericService;

/**
 * This interface defines the service layer for most merchant interactions.
 */
public interface MerchantService extends GenericService {

}
