package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.EventDao;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.EventType;
import efi.unleashed.service.EventService;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class EventServiceImpl extends GenericServiceImpl implements EventService {

    @Resource
    EventDao eventDao;

    @Override
    public GenericDao getDefaultDao() {
        return eventDao;
    }

    @Override
    public List<Event> findEventsForMerchant( Long merchantId ) {
        return eventDao.findEventsForMerchant( merchantId );
    }

    @Override
    public List<Event> findEventsForEventType( Long merchantId, String eventTypeString ) {
        return eventDao.findEventsForEventType( merchantId, eventTypeString );
    }

    @Override
    public List<EventType> findEventTypesForMerchant( Long merchantId ) {
        return eventDao.findEventTypesForMerchant( merchantId );
    }
}
