package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.DiningTableDao;
import efi.unleashed.dao.SectionDao;
import efi.unleashed.dao.ServerDao;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.service.FloorPlanService;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Service
public class FloorPlanServiceImpl extends GenericServiceImpl implements FloorPlanService {

    @Resource
    DiningTableDao diningTableDao;

    @Resource
    SectionDao sectionDao;

    @Resource
    ServerDao serverDao;

    @Override
    public GenericDao getDefaultDao() {
        return sectionDao;
    }

    @Override
    public Map<Long, DiningTable> getDiningTableMap( List<Long> tableIds ) {
        if ( tableIds == null || tableIds.isEmpty() ) {
            return Collections.emptyMap();
        }
        Map<Long, DiningTable> tableMap = new HashMap<Long, DiningTable>();
        List<DiningTable> tables = diningTableDao.findTablesByIds( tableIds );
        for ( DiningTable diningTable : tables ) {
            tableMap.put( diningTable.getId(), diningTable );
        }
        return tableMap;
    }

    @Override
    public Map<Long, Section> getSectionMap( List<Long> sectionIds ) {
        if ( sectionIds == null || sectionIds.isEmpty() ) {
            return Collections.emptyMap();
        }
        Map<Long, Section> sectionMap = new HashMap<Long, Section>();
        List<Section> sections = sectionDao.findSectionsByIds( sectionIds );
        for ( Section section : sections ) {
            sectionMap.put( section.getId(), section );
        }
        return sectionMap;
    }

    @Override
    public Map<Long, Server> getServerMap( List<Long> serverIds ) {
        if ( serverIds == null || serverIds.isEmpty() ) {
            return Collections.emptyMap();
        }
        Map<Long, Server> serverMap = new HashMap<Long, Server>();
        List<Server> servers = serverDao.findServersByIds( serverIds );
        for ( Server server : servers ) {
            serverMap.put( server.getId(), server );
        }
        return serverMap;
    }
}
