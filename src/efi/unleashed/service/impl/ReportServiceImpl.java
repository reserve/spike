package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.ReportDao;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportColumnField;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportType;
import efi.unleashed.service.ReportService;
import efi.unleashed.util.DateRangeIndicator;
import efi.unleashed.view.web.ReportEditView;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

@Service
public class ReportServiceImpl extends GenericServiceImpl implements ReportService {

    @Resource
    ReportDao reportDao;

    @Override
    public GenericDao getDefaultDao() {
        return reportDao;
    }

    @Override
    public List<Report> findReportsForMerchant( Long merchantId ) {
        return reportDao.findReportsForMerchant( merchantId );
    }

    @Override
    public List<Report> findReportsForReportType( Long merchantId, String reportTypeString ) {
        return reportDao.findReportsForReportType( merchantId, reportTypeString );
    }

    @Override
    public Report findReportForId( Long reportId ) {
        return reportDao.findReportForId( reportId );
    }

    @Override
    public ReportType findReportTypeForType( String reportType ) {
        return reportDao.findReportTypeForType( reportType );
    }

    @Override
    public ReportField findReportFieldForId( Long fieldId ) {
        return reportDao.findReportFieldForId( fieldId );
    }

    @Override
    @Transactional
    public void updateReport( ReportEditView reportEditView, BindingResult result ) {
        //TODO if the id is null, then we have to create a new report
        Report report = findReportForId( reportEditView.getId() );
        report.setName( reportEditView.getName() );
        report.getDateRangeField()
                .setDateRangeIndicator( DateRangeIndicator.findByUserString( reportEditView.getDateRange() ) );

        //gather list of IDs for all fields that the report already has as columns
        List<Long> oldColumns = new ArrayList<Long>( report.getReportColumnFields().size() );
        for ( ReportColumnField field : report.getReportColumnFields() ) {
            Long id = field.getReportField().getId();
            oldColumns.add( id );
        }
        //gather list of IDs for all fields that have been chosen as columns
        List<Long> newColumns = new ArrayList<Long>( reportEditView.getColumnIDs().size() );
        for ( String idString : reportEditView.getColumnIDs() ) {
            Long id = Long.valueOf( idString.substring( idString.lastIndexOf( "-" ) + 1 ) );
            newColumns.add( id );
        }
        //find out which columns need to be added, and which need to be removed
        Collection<Long> columnsToAdd = CollectionUtils.subtract( newColumns, oldColumns );
        Collection<Long> columnsToRemove = CollectionUtils.subtract( oldColumns, newColumns );

        //remove the ones that need to be removed
        for ( final Long id : columnsToRemove ) {
            ReportColumnField field =
                    (ReportColumnField) CollectionUtils.find( report.getReportColumnFields(), new Predicate() {
                        public boolean evaluate( Object o ) {
                            ReportColumnField f = (ReportColumnField) o;
                            return f.getReportField().getId() == id;
                        }
                    } );
            report.getReportColumnFields().remove( field );
        }

        //add the ones that need to be added
        for ( Long id : columnsToAdd ) {
            ReportField field = findReportFieldForId( id );
            ReportColumnField newColumn = new ReportColumnField();
            newColumn.setReport( report );
            newColumn.setReportField( field );
            newColumn.setColumnOrder( report.getReportColumnFields().size() );
            report.getReportColumnFields().add( newColumn );
        }

        //now sort the list according to the order the user specified
        int index = 1;
        for ( final Long id : newColumns ) {
            ReportColumnField field =
                    (ReportColumnField) CollectionUtils.find( report.getReportColumnFields(), new Predicate() {
                        public boolean evaluate( Object o ) {
                            ReportColumnField f = (ReportColumnField) o;
                            return f.getReportField().getId() == id;
                        }
                    } );
            field.setColumnOrder( index );
            index++;
        }
    }

}
