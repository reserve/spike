package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.TableAttributeDao;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.security.UserContext;
import efi.unleashed.service.TableAttributeService;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class TableAttributeServiceImpl extends GenericServiceImpl implements TableAttributeService {

    @Resource
    TableAttributeDao tableAttributeDao;

    @Override
    public GenericDao getDefaultDao() {
        return tableAttributeDao;
    }

    @Override
    public List<TableAttribute> findByMerchantId( Long merchantId ) {
        return tableAttributeDao.findByMerchantId( merchantId );
    }

    @Override
    public List<TableAttribute> findSystemAttributes() {
        return findByMerchantId( UserContext.SYSTEM_MERCHANT_ID );
    }
}
