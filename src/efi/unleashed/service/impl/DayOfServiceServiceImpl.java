package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.ConfigurationDao;
import efi.unleashed.dao.DiningServiceDao;
import efi.unleashed.domain.AvailabilityMatrix;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningReservationStatusType;
import efi.unleashed.domain.DiningService;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.TableService;
import efi.unleashed.domain.TableServiceStatusType;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.DayOfServiceService;
import efi.unleashed.service.FloorPlanService;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.ServiceUpdateView;
import efi.unleashed.view.api.TableServiceView;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Service
public class DayOfServiceServiceImpl extends GenericServiceImpl implements DayOfServiceService {

    private static final Logger LOGGER = Logger.getLogger( DayOfServiceServiceImpl.class.getName() );

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    FloorPlanService floorPlanService;

    @Resource
    ConfigurationDao configurationDao;

    @Resource
    DiningServiceDao diningServiceDao;

    @Override
    public GenericDao getDefaultDao() {
        return diningServiceDao;
    }

    @Override
    public AvailabilityMatrix findAvailabilityMatrixForSite( Long availabilityMatrixId, Long siteId ) {
        return configurationDao.findAvailabilityMatrixForSite( availabilityMatrixId, siteId );
    }

    @Override
    public DiningReservation findDiningReservationForMerchant( Long diningReservationId, Long merchantId ) {
        return diningServiceDao.findDiningReservationForMerchant( diningReservationId, merchantId );
    }

    @Override
    public DiningService findDiningServiceForSite( Long diningServiceId, Long siteId ) {
        return diningServiceDao.findDiningServiceForSite( diningServiceId, siteId );
    }

    @Override
    public DiningService findForSiteByServiceDate( Long siteId, LocalDate serviceDate, boolean isInService ) {
        return diningServiceDao.findForSiteByServiceDate( siteId, serviceDate, isInService );
    }

    @Override
    public FloorPlan findFloorPlanForSite( Long floorPlanId, Long siteId ) {
        return configurationDao.findFloorPlanForSite( floorPlanId, siteId );
    }

    @Override
    public TableService findTableServiceForMerchant( Long tableServiceId, Long merchantId ) {
        return diningServiceDao.findTableServiceForMerchant( tableServiceId, merchantId );
    }

    @Override
    public WaitStationTemplate findWaitStationTemplateForSite( Long templateId, Long siteId ) {
        return configurationDao.findWaitStationTemplateForSite( templateId, siteId );
    }

    @Override
    public List<TableService> findUpdatedTableService( Long siteId, DateTime since ) {
        return diningServiceDao.findUpdatedTableService( siteId, since );
    }

    @Override
    public List<DiningReservation> findAllReservationsForSite( Long siteId ) {
        return diningServiceDao.findAllReservationsForSite( siteId );
    }

    @Override
    @Transactional
    public void updateService( ServiceUpdateView serviceUpdateView,
                               MessageContextView messageContextView,
                               Errors errors ) {
        List<ApiView> updates = serviceUpdateView.getUpdates();
        int index = 0;
        for ( ApiView updateView : updates ) {
            LOGGER.debug( "Before validation: " + updateView );
            index++;
            // Yes, this is gross.
            if ( updateView instanceof TableServiceView ) {
                handleTableServiceUpdate( (TableServiceView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof DiningReservationView ) {
                handleDiningReservationUpdate( (DiningReservationView) updateView, errors, index, messageContextView );
            }
            else {
                errors.reject( "api.index.update.unknown-type",
                               new Object[] { index, "." + updateView.getClass().getSimpleName() }, null );
            }
        }
    }

    private void handleTableServiceUpdate( TableServiceView tableServiceView,
                                           Errors errors,
                                           int index,
                                           MessageContextView messageContextView ) {
        TableServiceStatusType type = null;
        String statusType = tableServiceView.getStatus();
        if ( statusType != null ) {
            type = getTableServiceStatusType( errors, statusType, index );
        }

        TableService tableService = findTableServiceForMerchant( tableServiceView.getId(),
                                                                 requestUserContext.getMerchantId() );
        if ( tableService == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "table service" }, null );
        }
        if ( errors.hasErrors() ) {
            return;
        }

        Map<Long, DiningTable> tableMap = floorPlanService.getDiningTableMap( tableServiceView.getTableIds() );
        List<DiningTable> tables = new ArrayList<DiningTable>( tableMap.values() );
        // TODO Should we check to make sure all tables are owned by the table service's reso's site?

        // Synchronize the tables.
        Collection<DiningTable> tablesToRemove = CollectionUtils.subtract( tableService.getTables(), tables );
        Collection<DiningTable> tablesToAdd = CollectionUtils.subtract( tables, tableService.getTables() );
        tableService.getTables().removeAll( tablesToRemove );
        for ( DiningTable diningTable : tablesToAdd ) {
            tableService.addDiningTable( diningTable );
        }

        tableService.setStatus( type );
        tableService.setTimeArrived( tableServiceView.getTimeArrived() );
        tableService.setTimeCleared( tableServiceView.getTimeCleared() );
        tableService.setTimeGreeted( tableServiceView.getTimeGreeted() );
        tableService.setTimeQuoted( tableServiceView.getTimeQuoted() );
        tableService.setTimeSeated( tableServiceView.getTimeSeated() );
        messageContextView.addTableService( tableService, false );
    }

    private TableServiceStatusType getTableServiceStatusType( Errors errors, String statusType, int index ) {
        TableServiceStatusType type = null;
        try {
            type = TableServiceStatusType.valueOf( statusType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.index.tableServiceStatusType.not-found", new Object[] { index, statusType }, null );
        }
        return type;
    }

    private void handleDiningReservationUpdate( DiningReservationView diningReservationView,
                                                Errors errors,
                                                int index,
                                                MessageContextView messageContextView ) {
        DiningReservationStatusType type = null;
        String statusType = diningReservationView.getStatus();
        if ( statusType != null ) {
            type = getDiningReservationStatusType( errors, statusType, index );
        }

        LocalTime time = null;
        try {
            time = LocalTime.parse( diningReservationView.getReservationTime() );
        }
        catch ( Exception e ) {
            errors.reject( "api.index.parse.error", new Object[] { index, "reservation time", e.getMessage() }, null );
        }

        DiningReservation reservation = findDiningReservationForMerchant( diningReservationView.getId(),
                                                                          requestUserContext.getMerchantId() );
        if ( reservation == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "dining reservation" }, null );
        }
        if ( errors.hasErrors() ) {
            return;
        }

        reservation.setReservationDate( diningReservationView.getReservationDate() );
        reservation.setReservationTime( time );
        reservation.setSeatingRequests( diningReservationView.getSeatingRequests() );
        reservation.setServiceOptions( diningReservationView.getServiceOptions() );
        reservation.setComments( diningReservationView.getComments() );
        reservation.setFirstName( diningReservationView.getFirstName() );
        reservation.setLastName( diningReservationView.getLastName() );
        reservation.setMobilePhone( diningReservationView.getMobilePhone() );
        reservation.setPartySize( diningReservationView.getPartySize() );
        Boolean flag = diningReservationView.getSpecialOccasion();
        reservation.setSpecialOccasion( flag == null ? false : flag );
        flag = diningReservationView.getRepeatGuest();
        reservation.setRepeatGuest( flag == null ? false : flag );
        flag = diningReservationView.getCancelled();
        reservation.setCancelled( flag == null ? false : flag );
        flag = diningReservationView.getNoShow();
        reservation.setNoShow( flag == null ? false : flag );
        flag = diningReservationView.getMadeToTable();
        reservation.setMadeToTable( flag == null ? false : flag );
        reservation.setStatus( type );
        messageContextView.addDiningReservation( reservation );
    }

    private DiningReservationStatusType getDiningReservationStatusType( Errors errors, String statusType, int index ) {
        DiningReservationStatusType type = null;
        try {
            type = DiningReservationStatusType.valueOf( statusType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.index.reservationStatusType.not-found", new Object[] { index, statusType }, null );
        }
        return type;
    }
}
