package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.MerchantDao;
import efi.unleashed.service.MerchantService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MerchantServiceImpl extends GenericServiceImpl implements MerchantService {
    
    @Resource
    MerchantDao merchantDao;

    @Override
    public GenericDao getDefaultDao() {
        return merchantDao;
    }
}
