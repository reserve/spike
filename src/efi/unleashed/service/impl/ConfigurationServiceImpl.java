package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.platform.service.impl.GenericServiceImpl;
import efi.unleashed.dao.DiningServiceDao;
import efi.unleashed.dao.DiningTableDao;
import efi.unleashed.dao.FloorPlanDao;
import efi.unleashed.dao.SectionDao;
import efi.unleashed.dao.ServerDao;
import efi.unleashed.dao.SiteDao;
import efi.unleashed.dao.TableAttributeDao;
import efi.unleashed.dao.WaitStationTemplateDao;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.FloorPlanItemType;
import efi.unleashed.domain.GraphicItemType;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.Site;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.domain.TableShapeType;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.ConfigurationService;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.DiningTableView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.util.List;

import javax.annotation.Resource;

@Service
public class ConfigurationServiceImpl extends GenericServiceImpl implements ConfigurationService {

    private static final Logger LOGGER = Logger.getLogger( ConfigurationServiceImpl.class.getName() );

    @Resource
    RequestUserContext requestUserContext;

    @Resource
    SiteDao siteDao;

    @Resource
    FloorPlanDao floorPlanDao;

    @Resource
    TableAttributeDao tableAttributeDao;

    @Resource
    DiningServiceDao diningServiceDao;

    @Resource
    WaitStationTemplateDao waitStationTemplateDao;

    @Resource
    SectionDao sectionDao;

    @Resource
    ServerDao serverDao;

    @Resource
    DiningTableDao diningTableDao;

    @Override
    public GenericDao getDefaultDao() {
        return diningServiceDao;
    }

    @Override
    public Site findSiteForMerchant( Long siteId, Long merchantId ) {
        return siteDao.findSiteForMerchant( siteId, merchantId );
    }

    @Override
    public FloorPlan findFloorPlanForMerchant( Long floorPlanId, Long merchantId ) {
        return floorPlanDao.findFloorPlanForMerchant( floorPlanId, merchantId );
    }

    @Override
    public FloorPlanItem findFloorPlanItemForMerchant( Long floorPlanItemId, Long merchantId ) {
        return floorPlanDao.findFloorPlanItemForMerchant( floorPlanItemId, merchantId );
    }

    @Override
    public TableAttribute findTableAttributeForMerchant( Long attributeId, Long merchantId ) {
        return tableAttributeDao.findTableAttributeForMerchant( attributeId, merchantId );
    }

    @Override
    public WaitStationTemplate findWaitStationTemplateForMerchant( Long waitStationTemplateId, Long merchantId ) {
        return waitStationTemplateDao.findTemplateForMerchant( waitStationTemplateId, merchantId );
    }

    @Override
    public Section findSectionForMerchant( Long sectionId, Long merchantId ) {
        return sectionDao.findSectionForMerchant( sectionId, merchantId );
    }

    @Override
    public Server findServerForMerchant( Long serverId, Long merchantId ) {
        return serverDao.findServerForMerchant( serverId, merchantId );
    }

    @Override
    public DiningTable findDiningTableForMerchant( Long tableId, Long merchantId ) {
        return diningTableDao.findDiningTableForMerchant( tableId, merchantId );
    }

    @Override
    @Transactional
    public void updateConfiguration( ConfigurationUpdateView configurationUpdateView,
                                     MessageContextView messageContextView,
                                     Errors errors ) {
        List<ApiView> updates = configurationUpdateView.getUpdates();
        int index = 0;
        for ( ApiView updateView : updates ) {
            LOGGER.debug( "Before validation: " + updateView );
            index++;
            // Yes, this is gross.
            if ( updateView instanceof FloorPlanView ) {
                handleFloorPlanUpdate( (FloorPlanView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof FloorPlanItemView ) {
                handleFloorPlanItemUpdate( (FloorPlanItemView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof SectionView ) {
                handleSectionUpdate( (SectionView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof ServerView ) {
                handleServerUpdate( (ServerView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof TableItemView ) {
                handleTableItemUpdate( (TableItemView) updateView, errors, index, messageContextView );
            }
            else if ( updateView instanceof WaitStationTemplateView ) {
                handleWaitStationTemplateUpdate( (WaitStationTemplateView) updateView,
                                                 errors, index, messageContextView );
            }
            else {
                errors.reject( "api.index.update.unknown-type",
                               new Object[] { index, "." + updateView.getClass().getSimpleName() }, null );
            }
        }
    }

    private void handleFloorPlanUpdate( FloorPlanView floorPlanView,
                                        Errors errors,
                                        int index,
                                        MessageContextView messageContextView ) {
        FloorPlan floorPlan = findFloorPlanForMerchant( floorPlanView.getId(), requestUserContext.getMerchantId() );
        if ( floorPlan == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "floor plan" }, null );
            return;
        }

        floorPlan.setName( floorPlanView.getName() );
        messageContextView.addFloorPlan( floorPlan );
    }

    private void handleFloorPlanItemUpdate( FloorPlanItemView floorPlanItemView,
                                            Errors errors,
                                            int index,
                                            MessageContextView messageContextView ) {
        FloorPlanItemType type = getFloorPlanItemType( errors, floorPlanItemView.getItemType(), index );

        String graphicItemType = floorPlanItemView.getGraphicType();
        if ( type == FloorPlanItemType.GRAPHIC && StringUtils.isBlank( graphicItemType ) ) {
            errors.reject( "api.index.need.graphic.type", new Object[] { index }, null );
            return;
        }
        GraphicItemType graphicType = null;
        if ( StringUtils.isNotBlank( graphicItemType ) ) {
            graphicType = getGraphicItemType( errors, graphicItemType, index );
        }

        FloorPlanItem floorPlanItem = findFloorPlanItemForMerchant( floorPlanItemView.getId(),
                                                                    requestUserContext.getMerchantId() );
        if ( floorPlanItem == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "floor plan item" }, null );
        }
        if ( errors.hasErrors() ) {
            return;
        }

        updateFloorPlanItem( floorPlanItemView, floorPlanItem, type, graphicType );
        messageContextView.addFloorPlanItem( floorPlanItem );
    }

    private FloorPlanItemType getFloorPlanItemType( Errors errors, String itemType, int index ) {
        FloorPlanItemType type = null;
        try {
            type = FloorPlanItemType.valueOf( itemType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.index.itemType.not-found", new Object[] { index, itemType }, null );
        }
        return type;
    }

    private GraphicItemType getGraphicItemType( Errors errors, String graphicItemType, int index ) {
        GraphicItemType type = null;
        try {
            type = GraphicItemType.valueOf( graphicItemType );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.index.graphicItemType.not-found", new Object[] { index, graphicItemType }, null );
        }
        return type;
    }

    private void updateFloorPlanItem( FloorPlanItemView floorPlanItemView,
                                      FloorPlanItem floorPlanItem,
                                      FloorPlanItemType type,
                                      GraphicItemType graphicType ) {
        floorPlanItem.setName( floorPlanItemView.getName() );
        floorPlanItem.setItemType( type );
        floorPlanItem.setGraphicType( graphicType );
        floorPlanItem.setX( floorPlanItemView.getX() );
        floorPlanItem.setY( floorPlanItemView.getY() );
        floorPlanItem.setWidth( floorPlanItemView.getWidth() );
        floorPlanItem.setHeight( floorPlanItemView.getHeight() );
        floorPlanItem.setRotation( floorPlanItemView.getRotation() );
    }

    private void handleSectionUpdate( SectionView sectionView,
                                      Errors errors,
                                      int index,
                                      MessageContextView messageContextView ) {
        Section section = findSectionForMerchant( sectionView.getId(), requestUserContext.getMerchantId() );
        if ( section == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "section" }, null );
            return;
        }

        section.setBorderColor( sectionView.getBorderColor() );
        section.setName( sectionView.getName() );
        messageContextView.addSection( section );
    }

    private void handleServerUpdate( ServerView serverView,
                                     Errors errors,
                                     int index,
                                     MessageContextView messageContextView ) {
        Server server = findServerForMerchant( serverView.getId(), requestUserContext.getMerchantId() );
        if ( server == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "server" }, null );
            return;
        }

        server.setAlias( serverView.getAlias() );
        server.setFirstName( serverView.getFirstName() );
        server.setLastName( serverView.getLastName() );
        messageContextView.addServer( server );
    }

    private void handleTableItemUpdate( TableItemView tableItemView,
                                        Errors errors,
                                        int index,
                                        MessageContextView messageContextView ) {
        FloorPlanItemView floorPlanItemView = tableItemView.getFloorPlanItem();
        DiningTableView diningTableView = tableItemView.getDiningTable();
        FloorPlanItemType type = getFloorPlanItemType( errors, floorPlanItemView.getItemType(), index );
        TableShapeType shape = getTableShapeType( errors, diningTableView.getShape(), index );

        DiningTable table = findDiningTableForMerchant( diningTableView.getId(), requestUserContext.getMerchantId() );
        if ( table == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "dining table" }, null );
        }

        FloorPlanItem floorPlanItem = findFloorPlanItemForMerchant( floorPlanItemView.getId(),
                                                                    requestUserContext.getMerchantId() );
        if ( floorPlanItem == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "floor plan item" }, null );
        }

        Long attributeId = diningTableView.getAttributeId();
        TableAttribute attribute = null;
        if ( attributeId != null ) {
            attribute = findTableAttributeForMerchant( attributeId, requestUserContext.getMerchantId() );
            if ( attribute == null ) {
                errors.reject( "api.index.merchant.ownership", new Object[] { index, "table attribute" }, null );
            }
        }

        if ( errors.hasErrors() ) {
            return;
        }

        table.setName( diningTableView.getName() );
        table.setShape( shape );
        table.setCapacity( diningTableView.getCapacity() );
        table.setAttribute( attribute );
        updateFloorPlanItem( floorPlanItemView, floorPlanItem, type, null );
        messageContextView.addDiningTable( table );
        messageContextView.addFloorPlanItem( floorPlanItem );
    }

    private TableShapeType getTableShapeType( Errors errors, String shape, int index ) {
        TableShapeType shapeType = null;
        try {
            shapeType = TableShapeType.valueOf( shape );
        }
        catch ( IllegalArgumentException e ) {
            errors.reject( "api.index.tableType.not-found", new Object[] { index, shape }, null );
        }
        return shapeType;
    }

    private void handleWaitStationTemplateUpdate( WaitStationTemplateView waitStationTemplateView,
                                                  Errors errors,
                                                  int index,
                                                  MessageContextView messageContextView ) {
        WaitStationTemplate template =
                findWaitStationTemplateForMerchant( waitStationTemplateView.getId(), requestUserContext.getMerchantId() );
        if ( template == null ) {
            errors.reject( "api.index.merchant.ownership", new Object[] { index, "wait station template" }, null );
            return;
        }

        template.setName( waitStationTemplateView.getName() );
        messageContextView.addWaitStationTemplate( template );
    }
}
