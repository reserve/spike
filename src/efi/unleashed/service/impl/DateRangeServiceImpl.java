package efi.unleashed.service.impl;

import efi.unleashed.domain.DateRange;
import efi.unleashed.service.DateRangeService;
import efi.unleashed.util.DateRangeIndicator;
import efi.unleashed.util.DateRangeUtils;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to ...
 */

@Service
public class DateRangeServiceImpl implements DateRangeService {

    @Override
    public List<String> getDateRangeStrings() {
        List<String> ranges = new ArrayList<String>( );
        for ( DateRangeIndicator indicator : DateRangeIndicator.values() ) {
            ranges.add( indicator.getUserString() );
        }
        return ranges;
    };

    @Override
    public List<String> getDateStringsForRange( String range ) {
        List<String> dates = new ArrayList<String>( 2 );
        DateRangeIndicator indicator = DateRangeIndicator.findByUserString( range );
        if (indicator == null) {
            LocalDate today = new LocalDate();
            dates.add( today.toString("MM/dd/yyyy") );
            dates.add( today.toString("MM/dd/yyyy") );
        }
        else {
            DateRange dateRange = DateRangeUtils.dateRangeForIndicator( indicator );
            dates.add( dateRange.getStartDate().toString("MM/dd/yyyy") );
            dates.add( dateRange.getEndDate().toString("MM/dd/yyyy") );
        }
        return dates;
    }
}
