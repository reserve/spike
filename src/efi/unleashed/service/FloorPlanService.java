package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;

import java.util.List;
import java.util.Map;

/**
 * This interface defines services needed to manage floor plans and its related objects.
 */
public interface FloorPlanService extends GenericService {

    Map<Long,DiningTable> getDiningTableMap( List<Long> tableIds );

    Map<Long,Section> getSectionMap( List<Long> sectionIds );

    Map<Long,Server> getServerMap( List<Long> serverIds );

}
