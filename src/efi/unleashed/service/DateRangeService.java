package efi.unleashed.service;

import efi.platform.service.GenericService;

import java.util.List;

/**
 * Interface for date range utilities
 */
public interface DateRangeService {
    List<String> getDateRangeStrings();

    List<String> getDateStringsForRange (String range );
}

