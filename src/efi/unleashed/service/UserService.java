package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.ApplicationUser;

/**
 * This interface is the service layer for managing application user data.
 */
public interface UserService extends GenericService {

    ApplicationUser findByUsername( String username );
    ApplicationUser findByUserId( Long userId );
}
