package efi.unleashed.service;

import efi.platform.service.GenericService;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportField;
import efi.unleashed.domain.ReportType;
import efi.unleashed.view.web.ReportEditView;

import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * This interface defines the service layer for interacting with reports.
 */
public interface ReportService extends GenericService {

    List<Report> findReportsForMerchant( Long merchantId );

    List<Report> findReportsForReportType( Long merchantId, String reportTypeString );

    Report findReportForId( Long reportId );

    ReportType findReportTypeForType( String reportType );

    void updateReport( ReportEditView reportEditView, BindingResult result );

    ReportField findReportFieldForId( Long fieldId );
}
