package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table( name = "APPLICATION_RIGHT" )
@org.hibernate.annotations.Table( appliesTo = "APPLICATION_RIGHT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "APPLICATION_RIGHT_ID_SEQ" ) } )
public class Right extends BaseEntity {

    private RightType rightType;

    private Set<Role> roles;

    @Column( name = "RIGHT_TYPE", length = 64, nullable = false, columnDefinition = "varchar(64)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.RightType" ) )
    public RightType getRightType() {
        return rightType;
    }

    public void setRightType( RightType rightType ) {
        this.rightType = rightType;
    }

    @ManyToMany( mappedBy = "rights" )
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles( Set<Role> roles ) {
        this.roles = roles;
    }

    @Override public String toString() {
        return "Right {" +
               "rightType=" + rightType +
               "} " + super.toString();
    }
}
