package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * This class is used to map ReportFields to a Report. These are fields are that are to be used as filters in the
 * Report.
 */

@Entity
@Table( name = "REPORT_FILTER_FIELD" )
@org.hibernate.annotations.Table( appliesTo = "REPORT_FILTER_FIELD" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "REPORT_FILTER_FIELD_ID_SEQ" ) } )

public class ReportFilterField extends BaseEntity {

    private String filterValue;
    private ReportFilterOperation operation;

    @NotNull
    private Report report;

    @NotNull
    private ReportField reportField;

    @Column( name = "FILTER_VALUE", length = 64 )
    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue( String filterValue ) {
        this.filterValue = filterValue;
    }

    @Column( name = "OPERATION", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.ReportFilterOperation" ) )
    public ReportFilterOperation getOperation() {
        return operation;
    }

    public void setOperation( ReportFilterOperation operation ) {
        this.operation = operation;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_ID" )
    @ForeignKey( name = "FK_REPORT_FILTER_FIELD_TO_REPORT" )
    public Report getReport() {
        return report;
    }

    public void setReport( Report report ) {
        this.report = report;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_FIELD_ID" )
    @ForeignKey( name = "FK_REPORT_FILTER_FIELD_TO_REPORT_FIELD" )
    public ReportField getReportField() {
        return reportField;
    }

    public void setReportField( ReportField reportField ) {
        this.reportField = reportField;
    }

    public static Comparator<ReportFilterField> FilterFieldComparator
            = new Comparator<ReportFilterField>() {
        public int compare( ReportFilterField filter1, ReportFilterField filter2 ) {
            /**
             * we want to ensure that the filters are always in the same order. first sort
             * by the field type and name. if they match, then use the unique db id of the filter.
             */
            int fieldCompare = filter1.getReportField().compareTypeAndNameTo( filter2.getReportField() );
            if (fieldCompare == 0) {
                return filter1.getId().compareTo( filter2.getId() );
            }
            else{
                return fieldCompare;
            }
        }
    };

    @Override public String toString() {
        return "ReportFilterField {" +
               ", filterValue= " + filterValue + '\'' +
               ", operation= " + operation + '\'' +
               "} " + super.toString();
    }
}
