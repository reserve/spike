package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "TableAttribute.findForMerchant",
                 query = "from TableAttribute as ta where ta.id = :attributeId and ta.merchant.id = :merchantId" ),
    @NamedQuery( name = "TableAttribute.findByMerchantId",
                 query = "from TableAttribute as ta where ta.merchant.id = :merchantId" ) } )

@Entity
@Table( name = "TABLE_ATTRIBUTE" )
@org.hibernate.annotations.Table( appliesTo = "TABLE_ATTRIBUTE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "TABLE_ATTRIBUTE_ID_SEQ" ) } )
public class TableAttribute extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private Merchant merchant;

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_TABLE_ATTRIBUTE_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @Override public String toString() {
        return "TableAttribute {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
