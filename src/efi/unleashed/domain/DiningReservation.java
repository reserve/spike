package efi.unleashed.domain;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "DiningReservation.findForMerchant",
                 query = " from DiningReservation as dr " +
                         "where dr.id = :diningReservationId and dr.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "DiningReservation.findUpdates",
                 query = "from DiningReservation as dr where dr.site.id = :siteId" ),
    @NamedQuery( name = "DiningReservation.findAllForSite",
                 query = "from DiningReservation as dr where dr.site.id = :siteId" ) } )

@Entity
@Table( name = "DINING_RESERVATION" )
@org.hibernate.annotations.Table( appliesTo = "DINING_RESERVATION" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "DINING_RESERVATION_ID_SEQ" ) } )
public class DiningReservation extends RequestableEntity {

    @NotNull
    private LocalDate reservationDate;
    
    @NotNull
    private LocalTime reservationTime;
    
    private DateTime dateTimeMade;
    
    private String confirmationNumber;
    
    private String seatingRequests;
    private String serviceOptions;
    private String servicePeriodName;
    private String comments;

    private String source;

    private String guestDisplayName;
    private String firstName;
    private String lastName;
    private String mobilePhone;
    
    private int partySize;

    private boolean isRepeatGuest;
    private boolean isSpecialOccasion;
    private boolean isCancelled;
    private boolean isNoShow;
    private boolean isMadeToTable;

    private DiningReservationStatusType status;

    private Site site;

    @Column( name = "RESERVATION_DATE", nullable = false )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate" )
    public LocalDate getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate( LocalDate reservationDate ) {
        this.reservationDate = reservationDate;
    }

    @Column( name = "RESERVATION_TIME", nullable = false )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime( LocalTime reservationTime ) {
        this.reservationTime = reservationTime;
    }

    @Column( name = "DATE_TIME_MADE" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getDateTimeMade() {
        return dateTimeMade;
    }

    public void setDateTimeMade( DateTime dateTimeMade ) {
        this.dateTimeMade = dateTimeMade;
    }

    @Column( name = "CONFIRMATION_NUMBER", length = 64 )
    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber( String confirmationNumber ) {
        this.confirmationNumber = confirmationNumber;
    }

    @Column( name = "SEATING_REQUESTS", length = 1024 )
    public String getSeatingRequests() {
        return seatingRequests;
    }

    public void setSeatingRequests( String seatingRequests ) {
        this.seatingRequests = seatingRequests;
    }

    @Column( name = "SERVICE_OPTIONS", length = 1024 )
    public String getServiceOptions() {
        return serviceOptions;
    }

    public void setServiceOptions( String serviceOptions ) {
        this.serviceOptions = serviceOptions;
    }

    @Column( name = "SERVICE_PERIOD_NAME", length = 16 )
    public String getServicePeriodName() {
        return servicePeriodName;
    }

    public void setServicePeriodName( String servicePeriodName ) {
        this.servicePeriodName = servicePeriodName;
    }

    @Column( name = "COMMENTS", length = 4096 )
    public String getComments() {
        return comments;
    }

    public void setComments( String comments ) {
        this.comments = comments;
    }

    @Column( name = "SOURCE", length = 128 )
    public String getSource() {
        return source;
    }

    public void setSource( String source ) {
        this.source = source;
    }

    @Column( name = "GUEST_DISPLAY_NAME", length = 256 )
    public String getGuestDisplayName() {
        return guestDisplayName;
    }

    public void setGuestDisplayName( String guestDisplayName ) {
        this.guestDisplayName = guestDisplayName;
    }

    @Column( name = "FIRST_NAME", length = 128 )
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    @Column( name = "LAST_NAME", length = 128 )
    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    @Column( name = "MOBILE_PHONE", length = 32 )
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone( String mobilePhone ) {
        this.mobilePhone = mobilePhone;
    }

    @Column( name = "PARTY_SIZE", nullable = false )
    public int getPartySize() {
        return partySize;
    }

    public void setPartySize( int partySize ) {
        this.partySize = partySize;
    }

    @Column( name = "IS_REPEAT_GUEST", length = 1 )
    @Type( type = "yes_no" )
    public boolean isRepeatGuest() {
        return isRepeatGuest;
    }

    public void setRepeatGuest( boolean repeatGuest ) {
        isRepeatGuest = repeatGuest;
    }

    @Column( name = "IS_SPECIAL_OCCASION", length = 1 )
    @Type( type = "yes_no" )
    public boolean isSpecialOccasion() {
        return isSpecialOccasion;
    }

    public void setSpecialOccasion( boolean specialOccasion ) {
        isSpecialOccasion = specialOccasion;
    }

    @Column( name = "IS_CANCELLED", length = 1 )
    @Type( type = "yes_no" )
    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled( boolean cancelled ) {
        isCancelled = cancelled;
    }

    @Column( name = "IS_NO_SHOW", length = 1 )
    @Type( type = "yes_no" )
    public boolean isNoShow() {
        return isNoShow;
    }

    public void setNoShow( boolean noShow ) {
        isNoShow = noShow;
    }

    @Column( name = "IS_MADE_TO_TABLE", length = 1 )
    @Type( type = "yes_no" )
    public boolean isMadeToTable() {
        return isMadeToTable;
    }

    public void setMadeToTable( boolean madeToTable ) {
        isMadeToTable = madeToTable;
    }

    @Column( name = "STATUS_TYPE", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.DiningReservationStatusType" ) )
    public DiningReservationStatusType getStatus() {
        return status;
    }

    public void setStatus( DiningReservationStatusType status ) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID" )
    @ForeignKey( name = "FK_DINING_RESO_TO_SITE" )
    public Site getSite() {
        return site;
    }

    public void setSite( Site site ) {
        this.site = site;
    }

    @Override public String toString() {
        return "DiningReservation {" +
               "comments='" + comments + '\'' +
               ", confirmationNumber='" + confirmationNumber + '\'' +
               ", dateTimeMade=" + dateTimeMade +
               ", firstName='" + firstName + '\'' +
               ", guestDisplayName='" + guestDisplayName + '\'' +
               ", isCancelled=" + isCancelled +
               ", isMadeToTable=" + isMadeToTable +
               ", isNoShow=" + isNoShow +
               ", isRepeatGuest=" + isRepeatGuest +
               ", isSpecialOccasion=" + isSpecialOccasion +
               ", lastName='" + lastName + '\'' +
               ", mobilePhone='" + mobilePhone + '\'' +
               ", partySize=" + partySize +
               ", reservationDate=" + reservationDate +
               ", reservationTime=" + reservationTime +
               ", seatingRequests='" + seatingRequests + '\'' +
               ", serviceOptions='" + serviceOptions + '\'' +
               ", servicePeriodName='" + servicePeriodName + '\'' +
               ", source='" + source + '\'' +
               ", status=" + status +
               "} " + super.toString();
    }

    @Transient
    public String getDisplayName() {
        if ( StringUtils.isNotBlank( guestDisplayName ) ) {
            return guestDisplayName;
        }
        StringBuilder builder = new StringBuilder();
        if ( StringUtils.isNotBlank( lastName ) ) {
            builder.append( lastName );
        }
        if ( builder.length() > 0 && StringUtils.isNotBlank( firstName ) ) {
            builder.append( ", " );
        }
        if ( StringUtils.isNotBlank( firstName ) ) {
            builder.append( firstName );
        }
        if ( builder.length() == 0 ) {
            return "Walk In Guest";
        }
        return builder.toString();
    }

    @Transient
    public String getGraphicalIndicator() {
        // TODO: Refactor this after NRA.
        StringBuilder builder = new StringBuilder();
        if ( isRepeatGuest ) {
            builder.append( 'R' );
        }
        if ( StringUtils.isNotBlank( seatingRequests ) ) {
            builder.append( 'M' );
        }
        if ( isSpecialOccasion ) {
            builder.append( 'C' );
        }
        if ( StringUtils.isNotBlank( serviceOptions ) ) {
            builder.append( 'Y' );
        }
        // TODO: calculate if table is pre-set
        return builder.toString();
    }
}
