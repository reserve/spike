package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "ReportType.findForType",
                 query = "from ReportType as t where t.type = :reportType" ) } )

@Entity
@Table( name = "REPORT_TYPE" )
@org.hibernate.annotations.Table( appliesTo = "REPORT_TYPE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
        parameters = { @Parameter( name = "sequence", value = "REPORT_TYPE_ID_SEQ" ) } )
public class ReportType extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String type;

    private Set<ReportField> availableFields;

    public static ReportType newInstance() {
        ReportType reportType = new ReportType();
        reportType.setAvailableFields( new HashSet<ReportField>() );
        return reportType;
    }

    public ReportField addReportField ( ReportField reportField ) {
        getAvailableFields().add( reportField );
        return reportField;
    }

    @Column( name = "TYPE", length = 128, nullable = false )
    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "REPORT_TYPE_REPORT_FIELD",
                joinColumns = { @JoinColumn( name = "REPORT_TYPE_ID" ) },
                inverseJoinColumns = { @JoinColumn( name = "REPORT_FIELD_ID" ) } )
    @ForeignKey( name = "FK_REPORT_TYPE_TO_REPORT_FIELD", inverseName = "FK_REPORT_FIELD_TO_REPORT_TYPE" )
    public Set<ReportField> getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields( Set<ReportField> availableFields ) {
        this.availableFields = availableFields;
    }

    @Override public String toString() {
        return "ReportType {" +
                "type='" + type + '\'' +
                "} " + super.toString();
    }
}