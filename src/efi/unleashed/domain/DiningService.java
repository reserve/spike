package efi.unleashed.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@NamedQueries( {
    @NamedQuery( name = "DiningService.findForSite",
                 query = " from DiningService as ds " +
                         "where ds.id = :diningServiceId and ds.site.id = :siteId" ),
    @NamedQuery( name = "DiningService.findForSiteByServiceDate",
                 query = " from DiningService as ds " +
                         "where ds.site.id = :siteId " +
                         "  and ds.serviceDate = :serviceDate and ds.inService = :isInService" ) } )

@Entity
@Table( name = "DINING_SERVICE" )
@org.hibernate.annotations.Table( appliesTo = "DINING_SERVICE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "DINING_SERVICE_ID_SEQ" ) } )
public class DiningService extends RequestableEntity {

    private LocalDate serviceDate;
    private DateTime startTime;
    private boolean isInService;

    private Site site;
    private AvailabilityMatrix availabilityMatrix;
    private FloorPlan floorPlan;
    private WaitStationTemplate waitStationTemplate;

    private Set<ScheduledServer> scheduledServers;
    
    public static DiningService newInstance() {
        DiningService service = new DiningService();
        service.setScheduledServers( new HashSet<ScheduledServer>() );
        return service;
    }

    public ScheduledServer addScheduledServer( ScheduledServer server ) {
        server.setDiningService( this );
        getScheduledServers().add( server );
        return server;
    }

    @Column( name = "SERVICE_DATE" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate" )
    public LocalDate getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate( LocalDate serviceDate ) {
        this.serviceDate = serviceDate;
    }

    @Column( name = "START_TIME" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime( DateTime startTime ) {
        this.startTime = startTime;
    }

    @Column( name = "IS_IN_SERVICE", length = 1 )
    @Type( type = "yes_no" )
    public boolean isInService() {
        return isInService;
    }

    public void setInService( boolean inService ) {
        isInService = inService;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID", nullable = false )
    @ForeignKey( name = "FK_DIN_SERVICE_TO_SITE" )
    public Site getSite() {
        return site;
    }

    public void setSite( Site site ) {
        this.site = site;
    }

    @ManyToOne
    @JoinColumn( name = "AVAILABILITY_MATRIX_ID", nullable = false )
    @ForeignKey( name = "FK_DIN_SERVICE_TO_AVAIL_MATRIX" )
    public AvailabilityMatrix getAvailabilityMatrix() {
        return availabilityMatrix;
    }

    public void setAvailabilityMatrix( AvailabilityMatrix availabilityMatrix ) {
        this.availabilityMatrix = availabilityMatrix;
    }

    @ManyToOne
    @JoinColumn( name = "FLOOR_PLAN_ID", nullable = false )
    @ForeignKey( name = "FK_DIN_SERVICE_TO_FLOOR_PLAN" )
    public FloorPlan getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan( FloorPlan floorPlan ) {
        this.floorPlan = floorPlan;
    }

    @ManyToOne
    @JoinColumn( name = "WAIT_STATION_DEF_ID", nullable = false )
    @ForeignKey( name = "FK_DIN_SERVICE_TO_WAIT_STA_DEF" )
    public WaitStationTemplate getWaitStationTemplate() {
        return waitStationTemplate;
    }

    public void setWaitStationTemplate( WaitStationTemplate waitStationTemplate ) {
        this.waitStationTemplate = waitStationTemplate;
    }

    @OneToMany( targetEntity = ScheduledServer.class, mappedBy = "diningService", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_DIN_SERVICE_TO_SCH_SERVER" )
    public Set<ScheduledServer> getScheduledServers() {
        return scheduledServers;
    }

    public void setScheduledServers( Set<ScheduledServer> scheduledServers ) {
        this.scheduledServers = scheduledServers;
    }

    @Override public String toString() {
        return "DiningService {" +
               "isInService=" + isInService +
               ", serviceDate=" + serviceDate +
               ", startTime=" + startTime +
               "} " + super.toString();
    }
}
