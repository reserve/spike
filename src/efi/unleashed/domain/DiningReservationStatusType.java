package efi.unleashed.domain;

/**
 * Enumerates the different status values for a dining reservation.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum DiningReservationStatusType {
    ARRIVED,
    CALL_AHEAD,
    CALL_AHEAD_OVERFLOW,
    CANCELED,
    DONE,
    HOLD,
    INTERNET,
    NO_SHOW,
    PAGED,
    PARTIALLY_ARRIVED,
    PARTIALLY_SEATED,
    RESERVED,
    RESERVED_OVERFLOW,
    SEATED,
    WALK_IN,
    WALK_IN_OVERFLOW,
    WALK_IN_PARTIALLY_ARRIVED
}
