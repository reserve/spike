package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table( name = "APPLICATION_ROLE" )
@org.hibernate.annotations.Table( appliesTo = "APPLICATION_ROLE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "APPLICATION_ROLE_ID_SEQ" ) } )
public class Role extends BaseEntity {

    @Length( max = 64 )
    private String name;

    private Set<Right> rights;

    @Column( name = "NAME", length = 64, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToMany( fetch = FetchType.EAGER )
    @JoinTable( name = "ROLE_RIGHT",
                joinColumns = { @JoinColumn( name = "ROLE_ID", referencedColumnName = "ID" ) },
                inverseJoinColumns = { @JoinColumn( name = "RIGHT_ID", referencedColumnName = "ID" ) } )
    @ForeignKey( name = "FK_ROLE_TO_RIGHT", inverseName = "FK_RIGHT_TO_ROLE" )
    public Set<Right> getRights() {
        return rights;
    }

    public void setRights( Set<Right> rights ) {
        this.rights = rights;
    }

    @Override public String toString() {
        return "Role {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
