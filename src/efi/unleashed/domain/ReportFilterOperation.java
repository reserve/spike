package efi.unleashed.domain;

/**
 * Enumerates the different operations for a ReportFilterField.
 */
public enum ReportFilterOperation {
    LESS_THAN( "Less Than", " < " ),
    LESS_THAN_EQUAL_TO( "Less Than or Equal To", " <= " ),
    EQUAL_TO( "Equal To", " = " ),
    NOT_EQUAL_TO( "Not Equal To", " <> " ),
    GREATER_THAN( "Greater Than", " > " ),
    GREATER_THAN_EQUAL_TO( "Greater Than or Equal To", " >= " ),
    BEGINS_WITH( "Begins With", " like " ),
    CONTAINS( "Contains", " like " );

    private String userString;
    private String sqlString;

    ReportFilterOperation( String userString, String sqlString ){
        this.userString = userString;
        this.sqlString = sqlString;
    }

    public String getUserString() {
        return userString;
    }

    public String getSqlString() {
        return sqlString;
    }
}
