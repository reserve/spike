package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "AVAILABILITY_SLOT" )
@org.hibernate.annotations.Table( appliesTo = "AVAILABILITY_SLOT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "AVAILABILITY_SLOT_ID_SEQ" ) } )
public class AvailabilitySlot extends BaseEntity {

    @NotNull
    @Length( max = 16 )
    private String timeSlot;  // TODO determine how this will be stored

    @NotNull
    private int partySize;

    @NotNull
    private int numberAvailable;

    private boolean isMaxSlot;

    @NotNull
    private AvailabilityMatrix matrix;

    @Column( name = "TIME_SLOT", length = 16, nullable = false )
    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot( String timeSlot ) {
        this.timeSlot = timeSlot;
    }

    @Column( name = "PARTY_SIZE", nullable = false )
    public int getPartySize() {
        return partySize;
    }

    public void setPartySize( int partySize ) {
        this.partySize = partySize;
    }

    @Column( name = "NUMBER_AVAILABLE", nullable = false )
    public int getNumberAvailable() {
        return numberAvailable;
    }

    public void setNumberAvailable( int numberAvailable ) {
        this.numberAvailable = numberAvailable;
    }

    @Column( name = "IS_MAX_SLOT", length = 1 )
    @Type( type = "yes_no" )
    public boolean isMaxSlot() {
        return isMaxSlot;
    }

    public void setMaxSlot( boolean maxSlot ) {
        isMaxSlot = maxSlot;
    }

    @ManyToOne
    @JoinColumn( name = "AVAILABILITY_MATRIX_ID", nullable = false )
    @ForeignKey( name = "FK_AVAIL_SLOT_TO_MATRIX" )
    public AvailabilityMatrix getMatrix() {
        return matrix;
    }

    public void setMatrix( AvailabilityMatrix matrix ) {
        this.matrix = matrix;
    }

    @Override public String toString() {
        return "AvailabilitySlot {" +
               "isMaxSlot=" + isMaxSlot +
               ", numberAvailable=" + numberAvailable +
               ", partySize=" + partySize +
               ", timeSlot='" + timeSlot + '\'' +
               "} " + super.toString();
    }
}
