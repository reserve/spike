package efi.unleashed.domain;

/**
 * Enumerates the authorizations in the application.
 * A Java enum is used to make security annotations cleaner.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum RightType {
    SYS_ADMIN,
    APP_USER,
    APP_ADMIN
}
