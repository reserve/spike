package efi.unleashed.domain;

/**
 * Enumerates the different status values for a table during service.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum TableServiceStatusType {
    PRE_ASSIGNED,
    PAGED,
    PARTIALLY_SEATED,
    SEATED,
    TABLE_GREETED,
    APPETIZER_SERVED,
    ENTREE_SERVED,
    DESSERT_SERVED,
    CHECK_PRESENTED,
    CHECK_PAID,
    BUS_REQUEST,
    CLEARED,
    CLOSED,
    OPENED
}
