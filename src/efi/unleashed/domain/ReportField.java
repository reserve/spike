package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NamedQueries( {
                       @NamedQuery( name = "ReportField.findForId",
                                    query = "from ReportField as rf where rf.id = :fieldId" )
               } )
/**
 * This class is used to identify a field that is available to be inserted into a report, either as a column, filter, or
 * group by field.
 */

@Entity
@Table( name = "REPORT_FIELD" )
@org.hibernate.annotations.Table( appliesTo = "REPORT_FIELD" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "REPORT_FIELD_ID_SEQ" ) } )
public class ReportField extends BaseEntity {

    private ReportFieldType fieldType;
    private ReportFieldDataType fieldDataType;

    private String fieldName;
    private String dbColumnName;
    private String dbTableName;
    private String dbAliasName;

    private boolean isAvailableColumn;
    private boolean isAvailableFilter;
    private boolean isAvailableGroupBy;


    @Column( name = "FIELD_NAME", length = 64 )
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName( String fieldName ) {
        this.fieldName = fieldName;
    }

    @Column( name = "DB_COLUMN_NAME", length = 64 )
    public String getDbColumnName() {
        return dbColumnName;
    }

    public void setDbColumnName( String dbColumnName ) {
        this.dbColumnName = dbColumnName;
    }

    @Column( name = "DB_TABLE_NAME", length = 64 )
    public String getDbTableName() {
        return dbTableName;
    }

    public void setDbTableName( String dbTableName ) {
        this.dbTableName = dbTableName;
    }

    @Column( name = "DB_ALIAS_NAME", length = 64 )
    public String getDbAliasName() {
        return dbAliasName;
    }

    public void setDbAliasName( String dbAliasName ) {
        this.dbAliasName = dbAliasName;
    }

    @Column( name = "FIELD_TYPE", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.ReportFieldType" ) )
    public ReportFieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType( ReportFieldType fieldType ) {
        this.fieldType = fieldType;
    }

    @Column( name = "FIELD_DATA_TYPE", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.ReportFieldDataType" ) )
    public ReportFieldDataType getFieldDataType() {
        return fieldDataType;
    }

    public void setFieldDataType( ReportFieldDataType fieldDataType ) {
        this.fieldDataType = fieldDataType;
    }

    @Column( name = "IS_AVAILABLE_COLUMN", length = 1 )
    @Type( type = "yes_no" )
    public boolean isAvailableColumn() {
        return isAvailableColumn;
    }

    public void setAvailableColumn( boolean availableColumn ) {
        isAvailableColumn = availableColumn;
    }

    @Column( name = "IS_AVAILABLE_FILTER", length = 1 )
    @Type( type = "yes_no" )
    public boolean isAvailableFilter() {
        return isAvailableFilter;
    }

    public void setAvailableFilter( boolean availableFilter ) {
        isAvailableFilter = availableFilter;
    }

    @Column( name = "IS_AVAILABLE_GROUP_BY", length = 1 )
    @Type( type = "yes_no" )
    public boolean isAvailableGroupBy() {
        return isAvailableGroupBy;
    }

    public void setAvailableGroupBy( boolean availableGroupBy ) {
        isAvailableGroupBy = availableGroupBy;
    }

    public int compareTypeAndNameTo( ReportField aField ) {
        if ( fieldType.name().equals( aField.fieldType.name() ) ) {
            return fieldName.compareTo( aField.fieldName );
        }
        else {
            return fieldType.name().compareTo( aField.fieldType.name() );
        }
    }

    @Override public String toString() {
        return "ReportField {" +
               "fieldName='" + fieldName + '\'' +
               ", fieldType=" + fieldType + '\'' +
               ", fieldDataType=" + fieldDataType + '\'' +
               ", isAvailableColumn=" + isAvailableColumn +
               ", isAvailableFilter=" + isAvailableFilter +
               ", isAvailableGroupBy=" + isAvailableGroupBy + '\'' +
               "} " + super.toString();
    }
}
