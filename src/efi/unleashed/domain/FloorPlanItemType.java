package efi.unleashed.domain;

/**
 * Enumerates the different kinds of floor plan item types (across all specific discriminated types).
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum FloorPlanItemType {
    LABEL,
    TABLE,
    GRAPHIC,
}
