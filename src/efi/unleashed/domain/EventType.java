package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "EventType.findForMerchant",
                 query = "from EventType as et where et.merchant.id = :merchantId" ) } )

@Entity
@Table( name = "EVENT_TYPE" )
@org.hibernate.annotations.Table( appliesTo = "EVENT_TYPE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "EVENT_TYPE_ID_SEQ" ) } )
public class EventType extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private Merchant merchant;

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_EVENT_TYPE_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @Override public String toString() {
        return "EventType {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
