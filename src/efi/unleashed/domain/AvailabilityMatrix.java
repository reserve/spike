package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "AvailabilityMatrix.findForSite",
                 query = "from AvailabilityMatrix as am where am.id = :matrixId and am.site.id = :siteId" ) } )

@Entity
@Table( name = "AVAILABILITY_MATRIX" )
@org.hibernate.annotations.Table( appliesTo = "AVAILABILITY_MATRIX" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "AVAILABILITY_MATRIX_ID_SEQ" ) } )
public class AvailabilityMatrix extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private Site site;

    private Set<AvailabilitySlot> slots;

    public static AvailabilityMatrix newInstance() {
        AvailabilityMatrix matrix = new AvailabilityMatrix();
        matrix.setSlots( new HashSet<AvailabilitySlot>() );
        return matrix;
    }

    public AvailabilitySlot addAvailabilitySlot( AvailabilitySlot availabilitySlot ) {
        availabilitySlot.setMatrix( this );
        getSlots().add( availabilitySlot );
        return availabilitySlot;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID", nullable = false )
    @ForeignKey( name = "FK_AVAIL_MATRIX_TO_SITE" )
    public Site getSite() {
        return site;
    }

    public void setSite( Site site ) {
        this.site = site;
    }

    @OneToMany( targetEntity = AvailabilitySlot.class, mappedBy = "matrix", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_AVAIL_MATRIX_TO_SLOT" )
    public Set<AvailabilitySlot> getSlots() {
        return slots;
    }

    public void setSlots( Set<AvailabilitySlot> slots ) {
        this.slots = slots;
    }

    @Override public String toString() {
        return "AvailabilityMatrix {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
