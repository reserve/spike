package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "EVENT_FUNCTION" )
@org.hibernate.annotations.Table( appliesTo = "EVENT_FUNCTION" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "EVENT_FUNCTION_ID_SEQ" ) } )
public class EventFunction extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private LocalDate functionDate;

    private LocalTime startTime;
    private LocalTime endTime;

    private String location;

    @NotNull
    private Event event;

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "FUNCTION_DATE", nullable = false )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate" )
    public LocalDate getFunctionDate() {
        return functionDate;
    }

    public void setFunctionDate( LocalDate functionDate ) {
        this.functionDate = functionDate;
    }

    @Column( name = "START_TIME" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime( LocalTime startTime ) {
        this.startTime = startTime;
    }

    @Column( name = "END_TIME" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime( LocalTime endTime ) {
        this.endTime = endTime;
    }

    @Column( name = "LOCATION", length = 128 )
    public String getLocation() {
        return location;
    }

    public void setLocation( String location ) {
        this.location = location;
    }

    @ManyToOne
    @JoinColumn( name = "EVENT_ID" )
    @ForeignKey( name = "FK_EVENT_FUNCTION_TO_EVENT" )
    public Event getEvent() {
        return event;
    }

    public void setEvent( Event event ) {
        this.event = event;
    }

    @Override public String toString() {
        return "EventFunction {" +
               "endTime=" + endTime +
               ", functionDate=" + functionDate +
               ", location='" + location + '\'' +
               ", name='" + name + '\'' +
               ", startTime=" + startTime +
               "} " + super.toString();
    }
}
