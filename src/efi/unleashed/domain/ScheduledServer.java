package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "SCHEDULED_SERVER" )
@org.hibernate.annotations.Table( appliesTo = "SCHEDULED_SERVER" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "SCHEDULED_SERVER_ID_SEQ" ) } )
public class ScheduledServer extends BaseEntity {

    private LocalTime timeIn;
    private LocalTime timeCut;
    private LocalTime lastSeatTime;

    private int preAssignedCovers;
    private int totalCovers;
    private int activeCovers;
    private int activeTables;
    
    private Server server;
    private Section section;
    private DiningService diningService;

    @Column( name = "TIME_IN" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getTimeIn() {
        return timeIn;
    }

    public void setTimeIn( LocalTime timeIn ) {
        this.timeIn = timeIn;
    }

    @Column( name = "TIME_CUT" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getTimeCut() {
        return timeCut;
    }

    public void setTimeCut( LocalTime timeCut ) {
        this.timeCut = timeCut;
    }

    @Column( name = "LAST_SEAT_TIME" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime" )
    public LocalTime getLastSeatTime() {
        return lastSeatTime;
    }

    public void setLastSeatTime( LocalTime lastSeatTime ) {
        this.lastSeatTime = lastSeatTime;
    }

    @Column( name = "PRE_ASSIGNED_COVERS" )
    public int getPreAssignedCovers() {
        return preAssignedCovers;
    }

    public void setPreAssignedCovers( int preAssignedCovers ) {
        this.preAssignedCovers = preAssignedCovers;
    }

    @Column( name = "TOTAL_COVERS" )
    public int getTotalCovers() {
        return totalCovers;
    }

    public void setTotalCovers( int totalCovers ) {
        this.totalCovers = totalCovers;
    }

    @Column( name = "ACTIVE_COVERS" )
    public int getActiveCovers() {
        return activeCovers;
    }

    public void setActiveCovers( int activeCovers ) {
        this.activeCovers = activeCovers;
    }

    @Column( name = "ACTIVE_TABLES" )
    public int getActiveTables() {
        return activeTables;
    }

    public void setActiveTables( int activeTables ) {
        this.activeTables = activeTables;
    }

    @ManyToOne
    @JoinColumn( name = "SERVER_ID", nullable = false )
    @ForeignKey( name = "FK_SCHED_SERVER_TO_SERVER" )
    public Server getServer() {
        return server;
    }

    public void setServer( Server server ) {
        this.server = server;
    }

    @ManyToOne
    @JoinColumn( name = "SECTION_ID", nullable = false )
    @ForeignKey( name = "FK_SCHED_SERVER_TO_SECTION" )
    public Section getSection() {
        return section;
    }

    public void setSection( Section section ) {
        this.section = section;
    }

    @ManyToOne
    @JoinColumn( name = "DINING_SERVICE_ID", nullable = false )
    @ForeignKey( name = "FK_SCHED_SERVER_TO_DIN_SERVICE" )
    public DiningService getDiningService() {
        return diningService;
    }

    public void setDiningService( DiningService diningService ) {
        this.diningService = diningService;
    }

    @Override public String toString() {
        return "ScheduledServer {" +
               "activeCovers=" + activeCovers +
               ", activeTables=" + activeTables +
               ", lastSeatTime=" + lastSeatTime +
               ", preAssignedCovers=" + preAssignedCovers +
               ", timeCut=" + timeCut +
               ", timeIn=" + timeIn +
               ", totalCovers=" + totalCovers +
               "} " + super.toString();
    }
}
