package efi.unleashed.domain;

/**
 * Enumerates the different data type values for a ReportField.
 */
public enum ReportFieldDataType {
    STRING,
    DATE,
    TIME,
    DATE_TIME,
    NUMERIC

}
