package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;
import efi.unleashed.util.DateRangeIndicator;
import efi.unleashed.util.DateRangeUtils;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * This class is used to specify a date range for the report. The user may
 * select one date field from the list of available date fields for the
 * report and then select a date range for that field to filter the report.
 */
@Entity
@Table( name = "REPORT_DATE_RANGE_FIELD" )
@org.hibernate.annotations.Table( appliesTo = "REPORT_DATE_RANGE_FIELD" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "REPORT_DATE_RANGE_FIELD_ID_SEQ" ) } )

public class ReportDateRangeField extends BaseEntity {

    @NotNull
    private ReportField reportField;

    @NotNull
    private DateRangeIndicator dateRangeIndicator;

    @ManyToOne
    @JoinColumn( name = "REPORT_FIELD_ID" )
    @ForeignKey( name = "FK_REPORT_DATE_RANGE_FIELD_TO_REPORT_FIELD" )
    public ReportField getReportField() {
        return reportField;
    }

    public void setReportField( ReportField reportField ) {
        this.reportField = reportField;
    }

    @Column( name = "RANGE_INDICATOR", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.util.DateRangeIndicator" ) )
    public DateRangeIndicator getDateRangeIndicator() {
        return dateRangeIndicator;
    }

    public void setDateRangeIndicator( DateRangeIndicator dateRangeIndicator ) {
        this.dateRangeIndicator = dateRangeIndicator;
    }

    public DateRange dateRange() {
        return DateRangeUtils.dateRangeForIndicator( dateRangeIndicator );
    }

    public DateRange dateRange( LocalDate date ) {
        return DateRangeUtils.dateRangeForIndicator( dateRangeIndicator, date );
    }

    @Override public String toString() {
        return "ReportDateRangeField {" +
               "dateRangeIndicator='" + dateRangeIndicator + '\'' +
               "} " + super.toString();
    }
}
