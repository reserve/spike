package efi.unleashed.domain;

/**
 * Enumerates the different kinds of graphic item types available on the tablet.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum GraphicItemType {
    BUSH1,
    BUSH2,
    BUSH3,
    BUSH4,
    BUSH5
}
