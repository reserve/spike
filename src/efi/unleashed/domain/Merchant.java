package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "Merchant.findByShortName",
                 query = "from Merchant as m where lower(m.shortName) = lower(:shortName)" ) } )

@Entity
@Table( name = "MERCHANT" )
@org.hibernate.annotations.Table( appliesTo = "MERCHANT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "MERCHANT_ID_SEQ" ) } )
public class Merchant extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    @Length( max = 64 )
    private String shortName;

    private BinaryMetadata logo;

    private Set<Server> servers;
    private Set<Site> sites;

    public static Merchant newInstance() {
        Merchant merchant = new Merchant();
        merchant.setServers( new HashSet<Server>() );
        merchant.setSites( new HashSet<Site>() );
        return merchant;
    }

    public Server addServer( Server server ) {
        server.setMerchant( this );
        getServers().add( server );
        return server;
    }

    public Site addSite( Site site ) {
        site.setMerchant( this );
        getSites().add( site );
        return site;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "SHORT_NAME", length = 64, nullable = false )
    public String getShortName() {
        return shortName;
    }

    public void setShortName( String shortName ) {
        this.shortName = shortName;
    }

    @ManyToOne
    @JoinColumn( name = "LOGO_METADATA_ID" )
    @ForeignKey( name = "FK_MERCHANT_TO_LOGO_DATA" )
    public BinaryMetadata getLogo() {
        return logo;
    }

    public void setLogo( BinaryMetadata logo ) {
        this.logo = logo;
    }

    @OneToMany( targetEntity = Server.class, mappedBy = "merchant", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_MERCHANT_TO_SERVER" )
    public Set<Server> getServers() {
        return servers;
    }

    public void setServers( Set<Server> servers ) {
        this.servers = servers;
    }

    @OneToMany( targetEntity = Site.class, mappedBy = "merchant", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_MERCHANT_TO_SITE" )
    public Set<Site> getSites() {
        return sites;
    }

    public void setSites( Set<Site> sites ) {
        this.sites = sites;
    }

    @Override public String toString() {
        return "Merchant {" +
               "name='" + name + '\'' +
               ", shortName='" + shortName + '\'' +
               "} " + super.toString();
    }
}
