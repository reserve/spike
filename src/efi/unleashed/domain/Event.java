package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
                       @NamedQuery( name = "Event.findForMerchant",
                                    query = "from Event as e where e.merchant.id = :merchantId" ),
                       @NamedQuery( name = "Event.findForEventType",
                                    query = "from Event as e " +
                                            "where e.merchant.id = :merchantId and e.eventType.name = :eventTypeString" ) } )

@Entity
@Table( name = "EVENT" )
@org.hibernate.annotations.Table( appliesTo = "EVENT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "EVENT_ID_SEQ" ) } )
public class Event extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private LocalDate eventDate;

    private String contact;
    private String salesperson;

    private BigDecimal food;
    private BigDecimal beverage;
    private BigDecimal setupService;
    private BigDecimal labor;
    private BigDecimal tax;
    private BigDecimal total;

    @NotNull
    private EventType eventType;

    @NotNull
    private Merchant merchant;

    private Set<EventFunction> eventFunctions;

    public static Event newInstance() {
        Event event = new Event();
        event.setEventFunctions( new HashSet<EventFunction>() );
        event.setFood( BigDecimal.ZERO );
        event.setBeverage( BigDecimal.ZERO );
        event.setSetupService( BigDecimal.ZERO );
        event.setLabor( BigDecimal.ZERO );
        event.setTax( BigDecimal.ZERO );
        event.setTotal( BigDecimal.ZERO );
        return event;
    }

    public EventFunction addEventFunction( EventFunction eventFunction ) {
        eventFunction.setEvent( this );
        getEventFunctions().add( eventFunction );
        return eventFunction;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "EVENT_DATE", nullable = false )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate" )
    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate( LocalDate eventDate ) {
        this.eventDate = eventDate;
    }

    @Column( name = "CONTACT", length = 128 )
    public String getContact() {
        return contact;
    }

    public void setContact( String contact ) {
        this.contact = contact;
    }

    @Column( name = "SALESPERSON", length = 128 )
    public String getSalesperson() {
        return salesperson;
    }

    public void setSalesperson( String salesperson ) {
        this.salesperson = salesperson;
    }

    @Column( name = "FOOD", precision = 12, scale = 2 )
    public BigDecimal getFood() {
        return food;
    }

    public void setFood( BigDecimal food ) {
        this.food = food;
    }

    @Column( name = "BEVERAGE", precision = 12, scale = 2 )
    public BigDecimal getBeverage() {
        return beverage;
    }

    public void setBeverage( BigDecimal beverage ) {
        this.beverage = beverage;
    }

    @Column( name = "SETUP_SERVICE", precision = 12, scale = 2 )
    public BigDecimal getSetupService() {
        return setupService;
    }

    public void setSetupService( BigDecimal setupService ) {
        this.setupService = setupService;
    }

    @Column( name = "LABOR", precision = 12, scale = 2 )
    public BigDecimal getLabor() {
        return labor;
    }

    public void setLabor( BigDecimal labor ) {
        this.labor = labor;
    }

    @Column( name = "TAX", precision = 12, scale = 2 )
    public BigDecimal getTax() {
        return tax;
    }

    public void setTax( BigDecimal tax ) {
        this.tax = tax;
    }

    @Column( name = "TOTAL", precision = 12, scale = 2 )
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal( BigDecimal total ) {
        this.total = total;
    }

    @ManyToOne
    @JoinColumn( name = "EVENT_TYPE_ID", nullable = false )
    @ForeignKey( name = "FK_EVENT_TO_TYPE" )
    public EventType getEventType() {
        return eventType;
    }

    public void setEventType( EventType eventType ) {
        this.eventType = eventType;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_EVENT_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @OneToMany( targetEntity = EventFunction.class, mappedBy = "event", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_EVENT_TO_FUNCTION" )
    public Set<EventFunction> getEventFunctions() {
        return eventFunctions;
    }

    public void setEventFunctions( Set<EventFunction> eventFunctions ) {
        this.eventFunctions = eventFunctions;
    }

    @Override public String toString() {
        return "Event {" +
               "beverage=" + beverage +
               ", contact='" + contact + '\'' +
               ", eventDate=" + eventDate +
               ", food=" + food +
               ", name='" + name + '\'' +
               ", salesperson='" + salesperson + '\'' +
               ", setupService=" + setupService +
               ", tax=" + tax +
               ", total=" + total +
               "} " + super.toString();
    }
}
