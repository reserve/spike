package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@NamedQueries( {
    @NamedQuery( name = "Server.findForMerchant",
                 query = "from Server as s where s.id = :serverId and s.merchant.id = :merchantId" ),
    @NamedQuery( name = "Server.findServersByIds",
                 query = "from Server as s where s.id in (:ids)" ) } )

@Entity
@Table( name = "SERVER" )
@org.hibernate.annotations.Table( appliesTo = "SERVER" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "SERVER_ID_SEQ" ) } )
public class Server extends RequestableEntity {

    @Length( max = 32 )
    private String firstName;

    @NotNull
    @Length( max = 32 )
    private String lastName;

    @Length( max = 32 )
    private String alias;

    @NotNull
    private Merchant merchant;

    @Column( name = "FIRST_NAME", length = 32 )
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    @Column( name = "LAST_NAME", length = 32, nullable = false )
    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    @Column( name = "ALIAS", length = 32 )
    public String getAlias() {
        return alias;
    }

    public void setAlias( String alias ) {
        this.alias = alias;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_SERVER_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @Override public String toString() {
        return "Server {" +
               "alias='" + alias + '\'' +
               ", firstName='" + firstName + '\'' +
               ", lastName='" + lastName + '\'' +
               "} " + super.toString();
    }
}
