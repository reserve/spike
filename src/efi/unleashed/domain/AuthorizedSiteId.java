package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class AuthorizedSiteId implements Serializable {

    private ApplicationUser user;
    private Site site;

    @ManyToOne
    @ForeignKey( name = "FK_AUTHORIZED_SITE_TO_USER" )
    public ApplicationUser getUser() {
        return user;
    }

    public void setUser( ApplicationUser user ) {
        this.user = user;
    }

    @ManyToOne
    @ForeignKey( name = "FK_AUTHORIZED_SITE_TO_SITE" )
    public Site getSite() {
        return site;
    }

    public void setSite( Site site ) {
        this.site = site;
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( !( o instanceof AuthorizedSiteId ) ) return false;

        AuthorizedSiteId that = (AuthorizedSiteId) o;

        if ( site != null ? !site.equals( that.site ) : that.site != null ) return false;
        if ( user != null ? !user.equals( that.user ) : that.user != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + ( site != null ? site.hashCode() : 0 );
        return result;
    }
}
