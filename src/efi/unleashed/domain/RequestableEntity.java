package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * This class is used to store a unique request ID for a new object until it is stored and has a database ID.
 */
@MappedSuperclass
public abstract class RequestableEntity extends BaseEntity {

    @Length( max = 64 )
    private String requestId;

    @Column( name = "REQUEST_ID", length = 64)
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId( String requestId ) {
        this.requestId = requestId;
    }

    @Override public String toString() {
        return "RequestableEntity {" +
               "requestId='" + requestId + '\'' +
               "} " + super.toString();
    }
}
