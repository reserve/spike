package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "FloorPlanItem.findForMerchant",
                 query = " from FloorPlanItem as fpi " +
                         "where fpi.id = :floorPlanItemId and fpi.floorPlan.site.merchant.id = :merchantId" ) } )

@Entity
@Table( name = "FLOOR_PLAN_ITEM" )
@org.hibernate.annotations.Table( appliesTo = "FLOOR_PLAN_ITEM" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "FLOOR_PLAN_ITEM_ID_SEQ" ) } )
public class FloorPlanItem extends RequestableEntity {

    @Length( max = 128 )
    private String name;

    private FloorPlanItemType itemType;
    private GraphicItemType graphicType;

    @NotNull
    private int x;

    @NotNull
    private int y;

    @NotNull
    private int width;

    @NotNull
    private int height;

    @NotNull
    private int rotation; // in degrees

    private DiningTable table;

    @NotNull
    private FloorPlan floorPlan;

    @Column( name = "NAME", length = 128 )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "ITEM_TYPE", length = 64, nullable = false, columnDefinition = "varchar(64)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.FloorPlanItemType" ) )
    public FloorPlanItemType getItemType() {
        return itemType;
    }

    public void setItemType( FloorPlanItemType itemType ) {
        this.itemType = itemType;
    }

    @Column( name = "GRAPHIC_TYPE", length = 64, columnDefinition = "varchar(64)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.GraphicItemType" ) )
    public GraphicItemType getGraphicType() {
        return graphicType;
    }

    public void setGraphicType( GraphicItemType graphicType ) {
        this.graphicType = graphicType;
    }

    @Column( name = "X_COORDINATE", nullable = false )
    public int getX() {
        return x;
    }

    public void setX( int x ) {
        this.x = x;
    }

    @Column( name = "Y_COORDINATE", nullable = false )
    public int getY() {
        return y;
    }

    public void setY( int y ) {
        this.y = y;
    }

    @Column( name = "WIDTH", nullable = false )
    public int getWidth() {
        return width;
    }

    public void setWidth( int width ) {
        this.width = width;
    }

    @Column( name = "HEIGHT", nullable = false )
    public int getHeight() {
        return height;
    }

    public void setHeight( int height ) {
        this.height = height;
    }

    @Column( name = "ROTATION", nullable = false )
    public int getRotation() {
        return rotation;
    }

    public void setRotation( int rotation ) {
        this.rotation = rotation;
    }

    @ManyToOne
    @JoinColumn( name = "DINING_TABLE_ID" )
    @ForeignKey( name = "FK_FP_ITEM_TO_DINING_TABLE" )
    public DiningTable getTable() {
        return table;
    }

    public void setTable( DiningTable table ) {
        this.table = table;
    }

    @ManyToOne
    @JoinColumn( name = "FLOOR_PLAN_ID", nullable = false )
    @ForeignKey( name = "FK_FP_ITEM_TO_FLOOR_PLAN" )
    public FloorPlan getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan( FloorPlan floorPlan ) {
        this.floorPlan = floorPlan;
    }

    @Override public String toString() {
        return "FloorPlanItem {" +
               "name='" + name + '\'' +
               ", itemType=" + itemType +
               ", x=" + x +
               ", y=" + y +
               ", width=" + width +
               ", height=" + height +
               ", rotation=" + rotation +
               "} " + super.toString();
    }
}
