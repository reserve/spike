package efi.unleashed.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "FloorPlan.findForMerchant",
                 query = "from FloorPlan as fp where fp.id = :floorPlanId and fp.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "FloorPlan.findForSite",
                 query = "from FloorPlan as fp where fp.id = :floorPlanId and fp.site.id = :siteId" ) } )

@Entity
@Table( name = "FLOOR_PLAN" )
@org.hibernate.annotations.Table( appliesTo = "FLOOR_PLAN" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "FLOOR_PLAN_ID_SEQ" ) } )
public class FloorPlan extends RequestableEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    private BinaryMetadata diagram;

    @NotNull
    private Site site;
    
    private Set<WaitStationTemplate> waitStationTemplates;
    private Set<DiningTable> tables;
    private Set<FloorPlanItem> floorPlanItems;
    
    public static FloorPlan newInstance() {
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.setWaitStationTemplates( new HashSet<WaitStationTemplate>() );
        floorPlan.setTables( new HashSet<DiningTable>() );
        floorPlan.setFloorPlanItems( new HashSet<FloorPlanItem>() );
        return floorPlan;
    }

    public WaitStationTemplate addWaitStationTemplate( WaitStationTemplate waitStationTemplate ) {
        waitStationTemplate.setFloorPlan( this );
        getWaitStationTemplates().add( waitStationTemplate );
        return waitStationTemplate;
    }

    public DiningTable addDiningTable( DiningTable table ) {
        table.setFloorPlan( this );
        getTables().add( table );
        return table;
    }

    public FloorPlanItem addFloorPlanItem( FloorPlanItem item ) {
        item.setFloorPlan( this );
        getFloorPlanItems().add( item );
        return item;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "DIAGRAM_METADATA_ID" )
    @ForeignKey( name = "FK_FLOOR_PLAN_TO_DIAGRAM_DATA" )
    public BinaryMetadata getDiagram() {
        return diagram;
    }

    public void setDiagram( BinaryMetadata diagram ) {
        this.diagram = diagram;
    }

    @ManyToOne
    @JoinColumn( name = "SITE_ID", nullable = false )
    @ForeignKey( name = "FK_FLOOR_PLAN_TO_SITE" )
    public Site getSite() {
        return site;
    }

    public void setSite( Site site ) {
        this.site = site;
    }

    @OneToMany( targetEntity = WaitStationTemplate.class, mappedBy = "floorPlan", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_FLOOR_PLAN_TO_WAIT_STA_DEF" )
    public Set<WaitStationTemplate> getWaitStationTemplates() {
        return waitStationTemplates;
    }

    public void setWaitStationTemplates( Set<WaitStationTemplate> waitStationTemplates ) {
        this.waitStationTemplates = waitStationTemplates;
    }

    @OneToMany( targetEntity = DiningTable.class, mappedBy = "floorPlan", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_FLOOR_PLAN_TO_DIN_TABLE" )
    public Set<DiningTable> getTables() {
        return tables;
    }

    public void setTables( Set<DiningTable> tables ) {
        this.tables = tables;
    }

    @OneToMany( targetEntity = FloorPlanItem.class, mappedBy = "floorPlan", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_FLOOR_PLAN_TO_FP_ITEM" )
    public Set<FloorPlanItem> getFloorPlanItems() {
        return floorPlanItems;
    }

    public void setFloorPlanItems( Set<FloorPlanItem> floorPlanItems ) {
        this.floorPlanItems = floorPlanItems;
    }

    @Override public String toString() {
        return "FloorPlan {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
