package efi.unleashed.domain;

/**
 * Enumerates the different field type values for a ReportField.
 */
public enum ReportFieldType {
    EVENT,
    EVENT_FUNCTION
}
