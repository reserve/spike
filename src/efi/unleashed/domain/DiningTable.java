package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@NamedQueries( {
    @NamedQuery( name = "DiningTable.findForMerchant",
                 query = " from DiningTable as dt " +
                         "where dt.id = :tableId and dt.floorPlan.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "DiningTable.findTablesByIds",
                 query = "from DiningTable as dt where dt.id in (:ids)" ) } )

@Entity
@Table( name = "DINING_TABLE" )
@org.hibernate.annotations.Table( appliesTo = "DINING_TABLE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "DINING_TABLE_ID_SEQ" ) } )
public class DiningTable extends RequestableEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private TableShapeType shape;

    @NotNull
    private int capacity;

    @NotNull
    private FloorPlan floorPlan;

    private TableAttribute attribute;

    private Set<Section> sections;
    
    public static DiningTable newInstance() {
        DiningTable table = new DiningTable();
        table.setSections( new HashSet<Section>() );
        return table;
    }

    public Section addSection( Section section ) {
        section.getTables().add( this );
        getSections().add( section );
        return section;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "SHAPE", length = 64, nullable = false, columnDefinition = "varchar(64)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.TableShapeType" ) )
    public TableShapeType getShape() {
        return shape;
    }

    public void setShape( TableShapeType shape ) {
        this.shape = shape;
    }

    @Column( name = "CAPACITY", nullable = false )
    public int getCapacity() {
        return capacity;
    }

    public void setCapacity( int capacity ) {
        this.capacity = capacity;
    }

    @ManyToOne
    @JoinColumn( name = "FLOOR_PLAN_ID", nullable = false )
    @ForeignKey( name = "FK_DIN_TABLE_TO_FLOOR_PLAN" )
    public FloorPlan getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan( FloorPlan floorPlan ) {
        this.floorPlan = floorPlan;
    }

    @ManyToOne
    @JoinColumn( name = "TABLE_ATTRIBUTE_ID" )
    @ForeignKey( name = "FK_DIN_TABLE_TO_ATTRIBUTE" )
    public TableAttribute getAttribute() {
        return attribute;
    }

    public void setAttribute( TableAttribute attribute ) {
        this.attribute = attribute;
    }

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "DINING_TABLE_SECTION",
                joinColumns = { @JoinColumn( name = "DINING_TABLE_ID" ) },
                inverseJoinColumns = { @JoinColumn( name = "SECTION_ID" ) } )
    @ForeignKey( name = "FK_DINING_TABLE_TO_SECTION", inverseName = "FK_SECTION_TO_DINING_TABLE" )
    public Set<Section> getSections() {
        return sections;
    }

    public void setSections( Set<Section> sections ) {
        this.sections = sections;
    }

    @Override public String toString() {
        return "DiningTable {" +
               "attribute='" + attribute + '\'' +
               ", capacity=" + capacity +
               ", name='" + name + '\'' +
               ", shape='" + shape + '\'' +
               "} " + super.toString();
    }
}
