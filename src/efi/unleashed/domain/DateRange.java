package efi.unleashed.domain;

import org.joda.time.LocalDate;


/**
 * This class is used to hold two date objects representing a date range.
 */
public class DateRange {

    private LocalDate startDate;
    private LocalDate endDate;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate( LocalDate startDate ) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate( LocalDate endDate ) {
        this.endDate = endDate;
    }

    @Override public String toString() {
        return "DateRange {" +
               "startDate=" + startDate +
               ", endDate=" + endDate +
               '}';
    }
}
