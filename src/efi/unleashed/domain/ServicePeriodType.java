package efi.unleashed.domain;

/**
 * Enumerates the different service periods; useful for interfacing with other reservation sources.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum ServicePeriodType {
    BREAKFAST,
    BRUNCH,
    LUNCH,
    DINNER
}
