package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@NamedQueries( {
    @NamedQuery( name = "TableService.findForMerchant",
                 query = " from TableService as ts " +
                         "where ts.id = :tableServiceId and ts.reservation.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "TableService.findUpdates",
                 query = " from TableService as ts " +
                         "where ts.reservation.site.id = :siteId" +
                         "  and ts.reservation.lastUpdated >= :since" ) } )

@Entity
@Table( name = "TABLE_SERVICE" )
@org.hibernate.annotations.Table( appliesTo = "TABLE_SERVICE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "TABLE_SERVICE_ID_SEQ" ) } )
public class TableService extends RequestableEntity {

    private DateTime timeQuoted;
    private DateTime timeGreeted;
    private DateTime timeSeated;
    private DateTime timeArrived;
    private DateTime timeCleared;
    
    private TableServiceStatusType status;

    private DiningReservation reservation;
    
    private Set<DiningTable> tables;
    
    public static TableService newInstance() {
        TableService tableService = new TableService();
        tableService.setTables( new HashSet<DiningTable>() );
        return tableService;
    }

    public DiningTable addDiningTable( DiningTable table ) {
        getTables().add( table );
        return table;
    }

    @Column( name = "TIME_QUOTED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getTimeQuoted() {
        return timeQuoted;
    }

    public void setTimeQuoted( DateTime timeQuoted ) {
        this.timeQuoted = timeQuoted;
    }

    @Column( name = "TIME_GREETED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getTimeGreeted() {
        return timeGreeted;
    }

    public void setTimeGreeted( DateTime timeGreeted ) {
        this.timeGreeted = timeGreeted;
    }

    @Column( name = "TIME_SEATED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getTimeSeated() {
        return timeSeated;
    }

    public void setTimeSeated( DateTime timeSeated ) {
        this.timeSeated = timeSeated;
    }

    @Column( name = "TIME_ARRIVED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getTimeArrived() {
        return timeArrived;
    }

    public void setTimeArrived( DateTime timeArrived ) {
        this.timeArrived = timeArrived;
    }

    @Column( name = "TIME_CLEARED" )
    @Type( type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime" )
    public DateTime getTimeCleared() {
        return timeCleared;
    }

    public void setTimeCleared( DateTime timeCleared ) {
        this.timeCleared = timeCleared;
    }

    @Column( name = "STATUS_TYPE", length = 32, columnDefinition = "varchar(32)" )
    @Type( type = "efi.platform.domain.hibernate.StringEnumUserType",
           parameters = @Parameter( name = "enumClassName", value = "efi.unleashed.domain.TableServiceStatusType" ) )
    public TableServiceStatusType getStatus() {
        return status;
    }

    public void setStatus( TableServiceStatusType status ) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn( name = "DINING_RESERVATION_ID", nullable = false )
    @ForeignKey( name = "FK_TABLE_SERVICE_TO_DIN_RESO" )
    public DiningReservation getReservation() {
        return reservation;
    }

    public void setReservation( DiningReservation reservation ) {
        this.reservation = reservation;
    }

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "TABLE_SERVICE_DINING_TABLE",
                joinColumns = { @JoinColumn( name = "TABLE_SERVICE_ID" ) },
                inverseJoinColumns = { @JoinColumn( name = "DINING_TABLE_ID" ) } )
    @ForeignKey( name = "FK_TABLE_SERVICE_TO_DIN_TABLE", inverseName = "FK_DIN_TABLE_TO_TABLE_SERVICE" )
    public Set<DiningTable> getTables() {
        return tables;
    }

    public void setTables( Set<DiningTable> tables ) {
        this.tables = tables;
    }

    @Override public String toString() {
        return "TableService {" +
               "status='" + status + '\'' +
               ", timeArrived=" + timeArrived +
               ", timeCleared=" + timeCleared +
               ", timeGreeted=" + timeGreeted +
               ", timeQuoted=" + timeQuoted +
               ", timeSeated=" + timeSeated +
               "} " + super.toString();
    }
}
