package efi.unleashed.domain;

/**
 * Enumerates the different kinds of table shapes.
 *
 * NOTE: DO NOT CHANGE the enum values, since they are persisted in the database.
 */
public enum TableShapeType {
    BANQUETTE,
    BOOTH,
    BOOTH_U_SHAPE,
    RECTANGLE,
    ROUND,
    SQUARE
}
