package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * This class is used to map ReportFields to a Report. These are fields are
 * that are to be used as columns in the Report.
 */

@Entity
@Table( name = "REPORT_COLUMN_FIELD" )
@org.hibernate.annotations.Table( appliesTo = "REPORT_COLUMN_FIELD" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "REPORT_COLUMN_FIELD_ID_SEQ" ) } )
public class ReportColumnField extends BaseEntity {

    @NotNull
    private int columnOrder;

    @NotNull
    private Report report;

    @NotNull
    private ReportField reportField;

    @Column( name = "COLUMN_ORDER", nullable = false )
    public int getColumnOrder() {
        return columnOrder;
    }

    public void setColumnOrder( int columnOrder ) {
        this.columnOrder = columnOrder;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_ID" )
    @ForeignKey( name = "FK_REPORT_COLUMN_FIELD_TO_REPORT" )
    public Report getReport() {
        return report;
    }

    public void setReport( Report report ) {
        this.report = report;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_FIELD_ID" )
    @ForeignKey( name = "FK_REPORT_COLUMN_FIELD_TO_REPORT_FIELD" )
    public ReportField getReportField() {
        return reportField;
    }

    public void setReportField( ReportField reportField ) {
        this.reportField = reportField;
    }

    @Override public String toString() {
        return "ReportColumnField {" +
               "columnOrder" + columnOrder +
               "} " + super.toString();
    }
}
