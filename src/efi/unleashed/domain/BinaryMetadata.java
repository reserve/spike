package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "BINARY_METADATA" )
@org.hibernate.annotations.Table( appliesTo = "BINARY_METADATA" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "BINARY_METADATA_ID_SEQ" ) } )
public class BinaryMetadata extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    @Length( max = 128 )
    private String contentType;

    @NotNull
    private long size;

    @NotNull
    private int width;

    @NotNull
    private int height;

    @NotNull
    private BinaryData binaryData;

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "CONTENT_TYPE", length = 128, nullable = false )
    public String getContentType() {
        return contentType;
    }

    public void setContentType( String contentType ) {
        this.contentType = contentType;
    }

    @Column( name = "DATA_SIZE", nullable = false )
    public long getSize() {
        return size;
    }

    public void setSize( long size ) {
        this.size = size;
    }

    @Column( name = "WIDTH", nullable = false )
    public int getWidth() {
        return width;
    }

    public void setWidth( int width ) {
        this.width = width;
    }

    @Column( name = "HEIGHT", nullable = false )
    public int getHeight() {
        return height;
    }

    public void setHeight( int height ) {
        this.height = height;
    }

    @ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY )
    @JoinColumn( name = "BINARY_DATA_ID", nullable = false )
    @ForeignKey( name = "FK_BINARY_METADATA_TO_DATA" )
    public BinaryData getBinaryData() {
        return binaryData;
    }

    public void setBinaryData( BinaryData binaryData ) {
        this.binaryData = binaryData;
    }

    @Override public String toString() {
        return "BinaryMetadata {" +
               "contentType='" + contentType + '\'' +
               ", height=" + height +
               ", name='" + name + '\'' +
               ", size=" + size +
               ", width=" + width +
               "} " + super.toString();
    }
}
