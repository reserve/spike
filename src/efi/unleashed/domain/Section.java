package efi.unleashed.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "Section.findForMerchant",
                 query = " from Section as s " +
                         "where s.id = :sectionId and s.waitStationTemplate.floorPlan.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "Section.findSectionsByIds",
                 query = "from Section as s where s.id in (:ids)" ) } )

@Entity
@Table( name = "SECTION" )
@org.hibernate.annotations.Table( appliesTo = "SECTION" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "SECTION_ID_SEQ" ) } )
public class Section extends RequestableEntity {

    @NotNull
    @Length( max = 128 )
    private String name;
    
    @NotNull
    @Length( max = 32 )
    private String borderColor;

    @NotNull
    private WaitStationTemplate waitStationTemplate;

    private Set<DiningTable> tables;

    public static Section newInstance() {
        Section section = new Section();
        section.setTables( new HashSet<DiningTable>() );
        return section;
    }

    public DiningTable addDiningTable( DiningTable table ) {
        table.getSections().add( this );
        getTables().add( table );
        return table;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @Column( name = "BORDER_COLOR", length = 32, nullable = false )
    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor( String borderColor ) {
        this.borderColor = borderColor;
    }

    @ManyToOne
    @JoinColumn( name = "WAIT_STA_DEF_ID", nullable = false )
    @ForeignKey( name = "FK_SECTION_TO_WAIT_STA_DEF" )
    public WaitStationTemplate getWaitStationTemplate() {
        return waitStationTemplate;
    }

    public void setWaitStationTemplate( WaitStationTemplate waitStationTemplate ) {
        this.waitStationTemplate = waitStationTemplate;
    }

    @ManyToMany( mappedBy = "sections" )
    public Set<DiningTable> getTables() {
        return tables;
    }

    public void setTables( Set<DiningTable> tables ) {
        this.tables = tables;
    }

    @Override public String toString() {
        return "Section {" +
               "borderColor='" + borderColor + '\'' +
               ", name='" + name + '\'' +
               "} " + super.toString();
    }
}
