package efi.unleashed.domain;

import org.hibernate.annotations.Type;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table( name = "AUTHORIZED_SITE" )
@org.hibernate.annotations.Table( appliesTo = "AUTHORIZED_SITE" )
@AssociationOverrides( { @AssociationOverride( name = "pk.user", joinColumns = @JoinColumn( name = "USER_ID" ) ),
                         @AssociationOverride( name = "pk.site", joinColumns = @JoinColumn( name = "SITE_ID" ) ) } )
public class AuthorizedSite implements Serializable {

    private AuthorizedSiteId pk = new AuthorizedSiteId();
    private boolean isAdmin;

    @EmbeddedId
    public AuthorizedSiteId getPk() {
        return pk;
    }

    public void setPk( AuthorizedSiteId pk ) {
        this.pk = pk;
    }

    @Column( name = "IS_ADMIN", length = 1, nullable = false)
    @Type( type = "yes_no" )
    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin( boolean admin ) {
        isAdmin = admin;
    }
    
    @Transient
    public ApplicationUser getUser() {
        return getPk().getUser();
    }

    public void setUser( ApplicationUser user ) {
        getPk().setUser( user );
    }
    
    @Transient
    public Site getSite() {
        return getPk().getSite();
    }

    public void setSite( Site site ) {
        getPk().setSite( site );
    }

    @Override
    public boolean equals( Object o ) {
        if ( this == o ) return true;
        if ( !( o instanceof AuthorizedSite ) ) return false;

        AuthorizedSite that = (AuthorizedSite) o;

        if ( getPk() != null ? !getPk().equals( that.getPk() ) : that.getPk() != null ) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getPk() != null ? getPk().hashCode() : 0;
    }

    @Override public String toString() {
        return "AuthorizedSite {" +
               "isAdmin=" + isAdmin +
               '}';
    }
}
