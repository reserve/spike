package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "BINARY_DATA" )
@org.hibernate.annotations.Table( appliesTo = "BINARY_DATA" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "BINARY_DATA_ID_SEQ" ) } )
public class BinaryData extends BaseEntity {

    private byte[] data;

    @Column( name = "DATA_BYTES", nullable = false )
    @Type( type = "org.hibernate.type.MaterializedBlobType" )
    public byte[] getData() {
        return data;
    }

    public void setData( byte[] data ) {
        this.data = data;
    }
}
