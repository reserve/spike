package efi.unleashed.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "WaitStationTemplate.findForMerchant",
                 query = " from WaitStationTemplate as wst " +
                         "where wst.id = :waitStationTemplateId and wst.floorPlan.site.merchant.id = :merchantId" ),
    @NamedQuery( name = "WaitStationTemplate.findForSite",
                 query = " from WaitStationTemplate as wst " +
                         "where wst.id = :waitStationTemplateId and wst.floorPlan.site.id = :siteId" ) } )

@Entity
@Table( name = "WAIT_STATION_DEF" )
@org.hibernate.annotations.Table( appliesTo = "WAIT_STATION_DEF" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "WAIT_STATION_DEF_ID_SEQ" ) } )
public class WaitStationTemplate extends RequestableEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private FloorPlan floorPlan;

    private Set<Section> sections;

    public static WaitStationTemplate newInstance() {
        WaitStationTemplate template = new WaitStationTemplate();
        template.setSections( new HashSet<Section>() );
        return template;
    }

    public Section addSection( Section section ) {
        section.setWaitStationTemplate( this );
        getSections().add( section );
        return section;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "FLOOR_PLAN_ID", nullable = false )
    @ForeignKey( name = "FK_WAIT_STA_DEF_TO_FLOOR_PLAN" )
    public FloorPlan getFloorPlan() {
        return floorPlan;
    }

    public void setFloorPlan( FloorPlan floorPlan ) {
        this.floorPlan = floorPlan;
    }

    @OneToMany( targetEntity = Section.class, mappedBy = "waitStationTemplate", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_WAIT_STA_DEF_TO_SECTION" )
    public Set<Section> getSections() {
        return sections;
    }

    public void setSections( Set<Section> sections ) {
        this.sections = sections;
    }

    @Override public String toString() {
        return "WaitStationTemplate {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
