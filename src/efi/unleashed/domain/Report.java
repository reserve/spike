package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "Report.findForId",
                 query = "from Report as r where r.id = :reportId" ),
    @NamedQuery( name = "Report.findForMerchant",
                 query = "from Report as r where r.merchant.id = :merchantId" ),
    @NamedQuery( name = "Report.findForReportType",
                 query = "from Report as r " +
                         "where r.merchant.id = :merchantId and r.reportType.type = :reportTypeString" ) } )

@Entity
@Table( name = "REPORT" )
@org.hibernate.annotations.Table( appliesTo = "REPORT" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "REPORT_ID_SEQ" ) } )
public class Report extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;

    @NotNull
    private ReportType reportType;

    @NotNull
    private Merchant merchant;

    private Set<ReportColumnField> reportColumnFields;
    private Set<ReportFilterField> reportFilterFields;

    @NotNull
    private ReportDateRangeField dateRangeField;

    //TODO add reportGroupByFields

    public static Report newInstance() {
        Report report = new Report();
        report.setReportColumnFields( new HashSet<ReportColumnField>() );
        report.setReportFilterFields( new HashSet<ReportFilterField>() );
        return report;
    }

    public ReportColumnField addReportColumnField( ReportColumnField columnField) {
        columnField.setReport( this );
        getReportColumnFields().add( columnField );
        return columnField;
    }

    public ReportFilterField addReportFilterField( ReportFilterField filterField) {
        filterField.setReport( this );
        getReportFilterFields().add( filterField );
        return filterField;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_TYPE_ID", nullable = false )
    @ForeignKey( name = "FK_REPORT_TO_TYPE" )
    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType( ReportType reportType ) {
        this.reportType = reportType;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_REPORT_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @OneToMany( targetEntity = ReportColumnField.class, mappedBy = "report", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_REPORT_TO_REPORT_COLUMN_FIELD" )
    public Set<ReportColumnField> getReportColumnFields() {
        return reportColumnFields;
    }

    public void setReportColumnFields( Set<ReportColumnField> reportColumnFields ) {
        this.reportColumnFields = reportColumnFields;
    }

    @OneToMany( targetEntity = ReportFilterField.class, mappedBy = "report", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_REPORT_TO_REPORT_FILTER_FIELD" )
    public Set<ReportFilterField> getReportFilterFields() {
        return reportFilterFields;
    }

    public void setReportFilterFields( Set<ReportFilterField> reportFilterFields ) {
        this.reportFilterFields = reportFilterFields;
    }

    @ManyToOne
    @JoinColumn( name = "REPORT_DATE_RANGE_FIELD_ID" )
    @ForeignKey( name = "FK_REPORT_TO_REPORT_DATE_RANGE_FIELD" )
    public ReportDateRangeField getDateRangeField() {
        return dateRangeField;
    }

    public void setDateRangeField( ReportDateRangeField dateRangeField ) {
        this.dateRangeField = dateRangeField;
    }

    @Override public String toString() {
        return "Report {" +
               "name='" + name + '\'' +
               "reportType='" + reportType + '\'' +
               "dateRange='" + dateRangeField + '\'' +
               "} " + super.toString();
    }
}
