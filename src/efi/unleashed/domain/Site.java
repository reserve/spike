package efi.unleashed.domain;

import efi.platform.domain.BaseEntity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NamedQueries( {
    @NamedQuery( name = "Site.findForMerchant",
                 query = "from Site as s where s.id = :siteId and s.merchant.id = :merchantId" ) } )

@Entity
@Table( name = "SITE" )
@org.hibernate.annotations.Table( appliesTo = "SITE" )
@GenericGenerator( name = "SequenceGenerator", strategy = "sequence",
                   parameters = { @Parameter( name = "sequence", value = "SITE_ID_SEQ" ) } )
public class Site extends BaseEntity {

    @NotNull
    @Length( max = 128 )
    private String name;
    
    @NotNull
    private Merchant merchant;
    
    private Set<FloorPlan> floorPlans;
    private Set<AvailabilityMatrix> availabilityMatrixList;
    private Set<AuthorizedSite> authorizedSites;
    
    public static Site newInstance() {
        Site site = new Site();
        site.setFloorPlans( new HashSet<FloorPlan>() );
        site.setAvailabilityMatrixList( new HashSet<AvailabilityMatrix>() );
        site.setAuthorizedSites( new HashSet<AuthorizedSite>() );
        return site;
    }

    public FloorPlan addFloorPlan( FloorPlan floorPlan ) {
        floorPlan.setSite( this );
        getFloorPlans().add( floorPlan );
        return floorPlan;
    }

    public AvailabilityMatrix addAvailabilityMatrix( AvailabilityMatrix availabilityMatrix ) {
        availabilityMatrix.setSite( this );
        getAvailabilityMatrixList().add( availabilityMatrix );
        return availabilityMatrix;
    }

    public AuthorizedSite addAuthorizedSite( AuthorizedSite authorizedSite ) {
        authorizedSite.setSite( this );
        getAuthorizedSites().add( authorizedSite );
        return authorizedSite;
    }

    @Column( name = "NAME", length = 128, nullable = false )
    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn( name = "MERCHANT_ID", nullable = false )
    @ForeignKey( name = "FK_SITE_TO_MERCHANT" )
    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant( Merchant merchant ) {
        this.merchant = merchant;
    }

    @OneToMany( targetEntity = FloorPlan.class, mappedBy = "site", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_SITE_TO_FLOOR_PLAN" )
    public Set<FloorPlan> getFloorPlans() {
        return floorPlans;
    }

    public void setFloorPlans( Set<FloorPlan> floorPlans ) {
        this.floorPlans = floorPlans;
    }

    @OneToMany( targetEntity = AvailabilityMatrix.class, mappedBy = "site", orphanRemoval = true )
    @Cascade( { CascadeType.SAVE_UPDATE, CascadeType.DELETE } )
    @ForeignKey( name = "FK_SITE_TO_AVAIL_MATRIX" )
    public Set<AvailabilityMatrix> getAvailabilityMatrixList() {
        return availabilityMatrixList;
    }

    public void setAvailabilityMatrixList( Set<AvailabilityMatrix> availabilityMatrixList ) {
        this.availabilityMatrixList = availabilityMatrixList;
    }

    @OneToMany( mappedBy = "pk.site", fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL )
    public Set<AuthorizedSite> getAuthorizedSites() {
        return authorizedSites;
    }

    public void setAuthorizedSites( Set<AuthorizedSite> authorizedSites ) {
        this.authorizedSites = authorizedSites;
    }

    @Override
    public String toString() {
        return "Site {" +
               "name='" + name + '\'' +
               "} " + super.toString();
    }
}
