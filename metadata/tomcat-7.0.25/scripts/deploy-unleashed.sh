#!/bin/sh

case "$1" in
  test)
        ;;
  prod)
        ;;
  dev)
        ;;
  int)
        ;;
  *)
        echo -e "Usage: $0 {test|prod|dev|int}\a"
        exit 1
esac

sudo efi-chown-chmod > /dev/null

cd /usr/local/efi/tomcat7/webapps

rm -rf ROOT
if [ -d ROOT ]; then
    echo "------"
    echo "Deployment failure - cannot remove old deployment directory - needs manual attention."
    exit 1
fi

rm -f ROOT.war
if [ -f ROOT.war ]; then
    echo "------"
    echo "Deployment failure - cannot remove old WAR file - needs manual attention."
    exit 1
fi

cd /usr/local/efi

echo "Running Unleashed deploy for environ: ${1}"

echo "stopping Unleashed..."
sudo /etc/init.d/unleashed stop

tar xf staging/${1}-unleashed-install.tar

sudo efi-chown-chmod > /dev/null

echo ""
echo "Deployed installation package with version data:"
unzip -c tomcat7/webapps/ROOT.war *MF | grep -e ^Build -e ^Version
echo ""
echo "Tomcat has been stopped and the staged install has been applied."
echo "You must now start Tomcat with 'sudo /etc/init.d/unleashed start'"

exit 0
