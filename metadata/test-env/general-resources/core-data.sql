-- Use PostgreSQL syntax
set DATABASE SQL SYNTAX PGS TRUE;

-- Need to drop sequences because Hibernate will create them from annotations.
-- Follows the naming convention of PostgreSQL BIGSERIAL data type.
drop sequence APPLICATION_RIGHT_ID_SEQ;
drop sequence APPLICATION_ROLE_ID_SEQ;
drop sequence APPLICATION_USER_ID_SEQ;
drop sequence AVAILABILITY_MATRIX_ID_SEQ;
drop sequence AVAILABILITY_SLOT_ID_SEQ;
drop sequence BINARY_DATA_ID_SEQ;
drop sequence BINARY_METADATA_ID_SEQ;
drop sequence DINING_RESERVATION_ID_SEQ;
drop sequence DINING_SERVICE_ID_SEQ;
drop sequence DINING_TABLE_ID_SEQ;
drop sequence EVENT_ID_SEQ;
drop sequence EVENT_FUNCTION_ID_SEQ;
drop sequence EVENT_TYPE_ID_SEQ;
drop sequence FLOOR_PLAN_ID_SEQ;
drop sequence FLOOR_PLAN_ITEM_ID_SEQ;
drop sequence MERCHANT_ID_SEQ;
drop sequence REPORT_ID_SEQ;
drop sequence SCHEDULED_SERVER_ID_SEQ;
drop sequence SECTION_ID_SEQ;
drop sequence SERVER_ID_SEQ;
drop sequence SITE_ID_SEQ;
drop sequence TABLE_ATTRIBUTE_ID_SEQ;
drop sequence TABLE_SERVICE_ID_SEQ;
drop sequence WAIT_STATION_DEF_ID_SEQ;
create sequence APPLICATION_RIGHT_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence APPLICATION_ROLE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence APPLICATION_USER_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence AVAILABILITY_MATRIX_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence AVAILABILITY_SLOT_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence BINARY_DATA_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence BINARY_METADATA_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence DINING_RESERVATION_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence DINING_SERVICE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence DINING_TABLE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence EVENT_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence EVENT_FUNCTION_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence EVENT_TYPE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence FLOOR_PLAN_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence FLOOR_PLAN_ITEM_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence MERCHANT_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence REPORT_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence SCHEDULED_SERVER_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence SECTION_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence SERVER_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence SITE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence TABLE_ATTRIBUTE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence TABLE_SERVICE_ID_SEQ as BIGINT start with 10000 increment by 1;
create sequence WAIT_STATION_DEF_ID_SEQ as BIGINT start with 10000 increment by 1;

-- Roles and rights.
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1', 'System Admin User');
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1000, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1000', 'Application User');
insert into APPLICATION_ROLE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME ) VALUES
(1001, 1, TIMESTAMP '2012-01-01 18:00:00', 'role-1001', 'Application Admin User');

insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1', 'SYS_ADMIN');
insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1000, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1000', 'APP_USER');
insert into APPLICATION_RIGHT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, RIGHT_TYPE ) VALUES
(1001, 1, TIMESTAMP '2012-01-01 18:00:00', 'right-1001', 'APP_ADMIN');

insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1, 1001);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1000, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1001, 1000);
insert into ROLE_RIGHT(ROLE_ID, RIGHT_ID) VALUES (1001, 1001);

-- System merchant.
insert into MERCHANT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SHORT_NAME) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'merchant-1', 'System Merchant', '-SystemMerchant-');

-- System user for use in background processing.
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-1',
 'system', 'WILLNEVERLOGIN', '2112', 'System', 'User', 'Y', 1, 1);

-- See spring-context-security.xml for the value of <realm>.
-- 070e54accad5aaf410a0b417dc16d773 == password of 'rgreinke' salted with 'rgreinke:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(2, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-2',
 'rgreinke', '070e54accad5aaf410a0b417dc16d773', '1234', 'Ross', 'Greinke', 'Y', 1, 1);

-- Table attributes; master list is owned by the system merchant.
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-1', 'Booth', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(2, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-2', 'Chef''s Table', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(3, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-3', 'Outside', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(4, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-4', 'Private', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(5, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-5', 'Window', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(6, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-6', 'Stage', 1);
insert into TABLE_ATTRIBUTE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(7, 1, TIMESTAMP '2012-01-01 18:00:00', 'table-attribute-7', 'Fireplace', 1);

-- Event types; master list is owned my the system merchant.
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(1, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-1', 'Anniversary', 1);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(2, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-2', 'Birthday', 1);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(3, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-3', 'Business', 1);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(4, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-4', 'Conference', 1);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(5, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-5', 'Wedding', 1);

