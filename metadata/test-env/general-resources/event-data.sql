
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-100', 'Anniversary', 100);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-101', 'Birthday', 100);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(102, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-102', 'Business', 100);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(103, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-103', 'Conference', 100);
insert into EVENT_TYPE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(104, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-type-104', 'Wedding', 100);

insert into EVENT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, EVENT_DATE, CONTACT, SALESPERSON,
       FOOD, BEVERAGE, SETUP_SERVICE, LABOR, TAX, TOTAL, EVENT_TYPE_ID, MERCHANT_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-100', 'Birthday Party for Fred', DATE '2013-01-01', 'Fred Jones', 'Sally',
 250.00, 125.50, 110.00, 125.00, 51.89, 662.39, 101, 100);
insert into EVENT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, EVENT_DATE, CONTACT, SALESPERSON,
       FOOD, BEVERAGE, SETUP_SERVICE, LABOR, TAX, TOTAL, EVENT_TYPE_ID, MERCHANT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'EVENT-101', 'Lisa''s Birthday Party', DATE '2013-01-01', 'Lisa Balastreri', 'Sally',
375.25, 295.55, 180.00, 178.58, 87.50,1116.88, 101, 100);
insert into EVENT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, EVENT_DATE, CONTACT, SALESPERSON,
       FOOD, BEVERAGE, SETUP_SERVICE, LABOR, TAX, TOTAL, EVENT_TYPE_ID, MERCHANT_ID) VALUES
(102, 1, TIMESTAMP '2012-01-01 18:00:00', 'EVENT-102', 'HP Conference', DATE '2013-01-01', 'Harry Canary', 'Suzie',
852.00, 185.25, 156.00, 155.00, 114.60, 1462.85, 103, 100);
insert into EVENT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, EVENT_DATE, CONTACT, SALESPERSON,
       FOOD, BEVERAGE, SETUP_SERVICE, LABOR, TAX, TOTAL, EVENT_TYPE_ID, MERCHANT_ID) VALUES
(103, 1, TIMESTAMP '2012-01-01 18:00:00', 'EVENT-103', 'Kohler Meeting', DATE '2013-01-01', 'Scott Wonscoff', 'Suzie',
665.00, 169.25, 158.60, 198.00, 101.22, 1292.07, 103, 100);
insert into EVENT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, EVENT_DATE, CONTACT, SALESPERSON,
       FOOD, BEVERAGE, SETUP_SERVICE, LABOR, TAX, TOTAL, EVENT_TYPE_ID, MERCHANT_ID) VALUES
(104, 1, TIMESTAMP '2012-01-01 18:00:00', 'EVENT-104', 'Daly Devices Tour', DATE '2013-01-01', 'Larry Daly', 'Jerry',
458.25, 358.58, 256.30, 257.26, 113.08, 1443.47, 103, 100);

insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-100', 'Reception', DATE '2013-01-01',
 TIME '17:00:00', TIME '18:00:00', 'Patio', 100);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-101', 'Dinner', DATE '2013-01-01',
 TIME '18:00:00', TIME '19:00:00', 'Veranda Dining Room', 100);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(102, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-102', 'Reception',DATE '2013-01-01',
TIME '11:00:00',TIME '12:00:00', 'Patio',101);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(103, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-103', 'Luncheon',DATE '2013-01-01',
TIME '12:00:00',TIME '13:00:00', 'Veranda Dining Room',101);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(104, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-104', 'Meeting',DATE '2013-01-01',
TIME '08:00:00',TIME '17:00:00', 'Walden Conference Room',102);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(105, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-105', 'Break',DATE '2013-01-01',
TIME '10:00:00',TIME '10:15:00', 'Walden Conference Room',102);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(106, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-106', 'Meeting',DATE '2013-01-01',
TIME '08:00:00',TIME '17:00:00', 'Zachman Conference Room',103);
insert into EVENT_FUNCTION (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, FUNCTION_DATE,
       START_TIME, END_TIME, LOCATION, EVENT_ID) VALUES
(107, 1, TIMESTAMP '2012-01-01 18:00:00', 'event-function-107', 'Meeting',DATE '2013-01-01',
TIME '08:00:00',TIME '17:00:00', 'Greinke Conference Room',104);