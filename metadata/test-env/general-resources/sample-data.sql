
-- Basic data; merchants, sites, servers
insert into MERCHANT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SHORT_NAME) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'merchant-100', 'Frontier Vineyards', 'Frontier');
insert into MERCHANT (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, SHORT_NAME) VALUES
(200, 1, TIMESTAMP '2012-01-01 18:00:00', 'merchant-200', 'House of Eats', 'Eats');

insert into SITE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'site-100', 'Frontier Vineyards', 100);
insert into SITE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'site-101', 'Carter Club', 100);
insert into SITE (ID, VERSION, LAST_UPDATED, UNIQUE_ID, NAME, MERCHANT_ID) VALUES
(200, 1, TIMESTAMP '2012-01-01 18:00:00', 'site-200', 'Snack Room', 200);

insert into SERVER (ID, VERSION, LAST_UPDATED, UNIQUE_ID, FIRST_NAME, LAST_NAME, ALIAS, MERCHANT_ID) VALUES
(100, 1, TIMESTAMP '2012-01-01 18:00:00', 'server-100', 'Anne', 'Smith', 'Annie', 100);
insert into SERVER (ID, VERSION, LAST_UPDATED, UNIQUE_ID, FIRST_NAME, LAST_NAME, ALIAS, MERCHANT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'server-101', 'Jeff', 'Nelson', 'Jeff', 100);
insert into SERVER (ID, VERSION, LAST_UPDATED, UNIQUE_ID, FIRST_NAME, LAST_NAME, ALIAS, MERCHANT_ID) VALUES
(102, 1, TIMESTAMP '2012-01-01 18:00:00', 'server-102', 'Andrea', 'Wilson', 'Andy', 100);
insert into SERVER (ID, VERSION, LAST_UPDATED, UNIQUE_ID, FIRST_NAME, LAST_NAME, ALIAS, MERCHANT_ID) VALUES
(103, 1, TIMESTAMP '2012-01-01 18:00:00', 'server-103', 'Daniel', 'Thompson', 'Dan', 100);
insert into SERVER (ID, VERSION, LAST_UPDATED, UNIQUE_ID, FIRST_NAME, LAST_NAME, ALIAS, MERCHANT_ID) VALUES
(200, 1, TIMESTAMP '2012-01-01 18:00:00', 'server-200', 'Johnny', 'Munch', 'Johnny', 200);

-- e86e942a0fcee19984a24f1605c0994a == password of 'tabitha' salted with 'tabitha:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(101, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-101',
 'tabitha', 'e86e942a0fcee19984a24f1605c0994a', '1234', 'Tablet', 'Itha', 'Y', 1001, 100);

-- 2aabd2a4bcf35c2c33d2d9d09ab5cb7d == password of 'jackie' salted with 'jackie:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(102, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-102',
 'jackie', '2aabd2a4bcf35c2c33d2d9d09ab5cb7d', '1234', 'Jackie', 'Zachman', 'Y', 1001, 100);

-- f61e0d4ccbd45d989d42522fb9f407dc == password of 'jeff' salted with 'jeff:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(103, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-103',
 'jeff', 'f61e0d4ccbd45d989d42522fb9f407dc', '1234', 'Jeff', 'Walden', 'Y', 1001, 100);

-- 22a30c28eef19d551cbf1eb2869f4562 == password of 'ross' salted with 'ross:<realm>'
insert into APPLICATION_USER (ID, VERSION, LAST_UPDATED, UNIQUE_ID,
       USERNAME, PASSWORD, PASSCODE, FIRST_NAME, LAST_NAME, IS_ACTIVE, ROLE_ID, PRIMARY_MERCHANT_ID) VALUES
(104, 1, TIMESTAMP '2012-01-01 18:00:00', 'user-104',
 'ross', '22a30c28eef19d551cbf1eb2869f4562', '1234', 'Ross', 'Greinke', 'Y', 1001, 100);

-- Link up users to their authorized sites.
insert into AUTHORIZED_SITE( USER_ID, SITE_ID, IS_ADMIN ) VALUES (101, 100, 'Y');
insert into AUTHORIZED_SITE( USER_ID, SITE_ID, IS_ADMIN ) VALUES (102, 100, 'Y');
insert into AUTHORIZED_SITE( USER_ID, SITE_ID, IS_ADMIN ) VALUES (103, 100, 'Y');
insert into AUTHORIZED_SITE( USER_ID, SITE_ID, IS_ADMIN ) VALUES (104, 100, 'Y');