<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:security="http://www.springframework.org/schema/security"
       xsi:schemaLocation="
     http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
     http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.1.xsd">

    <!-- Completely ignore URLs pointing to assets. -->
    <security:http pattern="/css/**" security="none"/>
    <security:http pattern="/help/**" security="none"/>
    <security:http pattern="/images-*/**" security="none"/>
    <security:http pattern="/styles-*/**" security="none"/>
    <security:http pattern="/scripts-*/**" security="none"/>
    
    <!-- Secure REST API calls with Digest Authentication. -->
    <security:http pattern="/api/**" create-session="stateless"  use-expressions="true" entry-point-ref="digestEntryPoint">
        <security:custom-filter ref="digestFilter" position="BASIC_AUTH_FILTER"/>
        <security:intercept-url pattern="/api/**" access="isAuthenticated()"/>
    </security:http>

    <!-- Everything else (web application) uses form-based authentication. -->
    <security:http auto-config="false" use-expressions="true">
        <security:intercept-url pattern="/login*" access="permitAll"/>
        <security:intercept-url pattern="/**" access="isAuthenticated()"/>
        <security:form-login authentication-failure-url="/login?auth_failure=true" login-page="/login"/>
        <security:logout logout-success-url="/index"/>
        <security:remember-me user-service-ref="userDetailsService"/>
    </security:http>

    <!-- Create a simple string bean to hold our realm name. -->
    <bean id="reserveRealm" class="java.lang.String">
        <constructor-arg value="ReServe IQ Digest Security Realm"/>
    </bean>

    <!-- Configure the digest entry point to setup the HTTP headers for Unauthorized responses. -->
    <bean id="digestEntryPoint"
          class="org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint">
        <property name="realmName" ref="reserveRealm"/>
        <property name="key" value="ReServeIQisSmart"/>
        <property name="nonceValiditySeconds" value="3600"/>
    </bean>

    <!-- Explicitly create a digest auth filter, rather than use the built-in basic auth filter. -->
    <bean id="digestFilter" class="org.springframework.security.web.authentication.www.DigestAuthenticationFilter">
        <property name="userDetailsService" ref="userDetailsService"/>
        <property name="authenticationEntryPoint" ref="digestEntryPoint"/>
        <property name="passwordAlreadyEncoded" value="true"/>
    </bean>

    <!-- Set the role prefix to empty so that role names do not need to begin with "ROLE". -->
    <bean id="roleVoter" class="org.springframework.security.access.vote.RoleVoter">
        <property name="rolePrefix" value=""/>
    </bean>
    
    <!-- Use the role voter above for security access decisions. -->
    <bean id="affirmativeBased" class="org.springframework.security.access.vote.AffirmativeBased">
        <constructor-arg>
            <list>
                <ref bean="roleVoter"/>
                <bean class="org.springframework.security.access.vote.AuthenticatedVoter"/>
            </list>
        </constructor-arg>
    </bean>

    <!-- Use the access decision manager above for @Secured annotations on methods. -->
    <security:global-method-security secured-annotations="enabled" access-decision-manager-ref="affirmativeBased"/>

    <!-- Create our our salt source for the password encoder. -->
    <bean id="saltSource" class="efi.unleashed.security.DigestAuthenticationAwareSaltSource">
        <property name="digestRealm" ref="reserveRealm"/>
    </bean>
    
    <!-- Use our own password encoder that works with both form-based and digest-based authentication. -->
    <bean id="passwordEncoder" class="efi.unleashed.security.DigestAuthenticationAwarePasswordEncoder"/>

    <!-- Use our own user details service bean to start the authentication process. -->
    <security:authentication-manager>
        <security:authentication-provider user-service-ref="userDetailsService">
            <security:password-encoder ref="passwordEncoder">
                <security:salt-source ref="saltSource"/>
            </security:password-encoder>
        </security:authentication-provider>
    </security:authentication-manager>
</beans>