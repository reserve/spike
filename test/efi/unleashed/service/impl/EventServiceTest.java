package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.EventType;
import efi.unleashed.service.EventService;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class EventServiceTest {
    
    @Resource
    EventService eventService;
    
    @Test
    @SuppressWarnings( "unchecked" )
    public void testFindEventTypes() throws Exception {
        List<EventType> eventTypes = eventService.findEventTypesForMerchant( TestConstants.TEST_MERCHANT_ID );
        assertNotNull( eventTypes );
        assertFalse( eventTypes.isEmpty() );
        List<String> names =
                (List<String>) CollectionUtils.collect( eventTypes, new BeanToPropertyValueTransformer( "name" ) );
        assertTrue( names.contains( "Birthday" ) );
        assertTrue( names.contains( "Wedding" ) );
    }

    @Test
    public void testFindEvents() throws Exception {
        List<Event> events = eventService.findEventsForMerchant( TestConstants.TEST_MERCHANT_ID );
        assertNotNull( events );
        assertFalse( events.isEmpty() );
    }

    @Test
    public void testFindEventsForType() throws Exception {
        List<Event> events = eventService.findEventsForEventType( TestConstants.TEST_MERCHANT_ID, "Birthday" );
        assertNotNull( events );
        assertFalse( events.isEmpty() );
    }
}
