package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.DiningTable;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.service.FloorPlanService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class FloorPlanServiceTest {

    @Resource
    FloorPlanService floorPlanService;

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( floorPlanService.getDefaultDao() );
    }

    @Test
    public void testGetDiningTableMap() throws Exception {
        Map<Long, DiningTable> map = floorPlanService.getDiningTableMap( null );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        map = floorPlanService.getDiningTableMap( new ArrayList<Long>() );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        List<Long> list = Arrays.asList( TestConstants.TEST_DINING_TABLE_ID );
        map = floorPlanService.getDiningTableMap( list );
        assertNotNull( map );
        assertEquals( 1, map.size() );
        Map.Entry<Long, DiningTable> next = map.entrySet().iterator().next();
        assertEquals( TestConstants.TEST_DINING_TABLE_ID, next.getKey() );
        assertEquals( "10", next.getValue().getName() );

        list = Arrays.asList( 100L, 101L );
        map = floorPlanService.getDiningTableMap( list );
        assertEquals( 2, map.size() );

        list = Arrays.asList( TestConstants.TEST_DINING_TABLE_ID, TestConstants.TEST_UNUSED_ID );
        map = floorPlanService.getDiningTableMap( list );
        assertEquals( 1, map.size() );
    }

    @Test
    public void testGetSectionMap() throws Exception {
        Map<Long, Section> map = floorPlanService.getSectionMap( null );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        map = floorPlanService.getSectionMap( new ArrayList<Long>() );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        List<Long> list = Arrays.asList( TestConstants.TEST_SECTION_ID );
        map = floorPlanService.getSectionMap( list );
        assertNotNull( map );
        assertEquals( 1, map.size() );
        Map.Entry<Long, Section> next = map.entrySet().iterator().next();
        assertEquals( TestConstants.TEST_SECTION_ID, next.getKey() );
        assertEquals( "Section 1", next.getValue().getName() );

        list = Arrays.asList( 100L, 101L );
        map = floorPlanService.getSectionMap( list );
        assertEquals( 2, map.size() );

        list = Arrays.asList( TestConstants.TEST_SECTION_ID, TestConstants.TEST_UNUSED_ID );
        map = floorPlanService.getSectionMap( list );
        assertEquals( 1, map.size() );
    }

    @Test
    public void testGetServerMap() throws Exception {
        Map<Long, Server> map = floorPlanService.getServerMap( null );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        map = floorPlanService.getServerMap( new ArrayList<Long>() );
        assertNotNull( map );
        assertTrue( map.isEmpty() );

        List<Long> list = Arrays.asList( TestConstants.TEST_SERVER_ID );
        map = floorPlanService.getServerMap( list );
        assertNotNull( map );
        assertEquals( 1, map.size() );
        Map.Entry<Long, Server> next = map.entrySet().iterator().next();
        assertEquals( TestConstants.TEST_SERVER_ID, next.getKey() );
        assertEquals( "Annie", next.getValue().getAlias() );

        list = Arrays.asList( 100L, 101L );
        map = floorPlanService.getServerMap( list );
        assertEquals( 2, map.size() );

        list = Arrays.asList( TestConstants.TEST_SERVER_ID, TestConstants.TEST_UNUSED_ID );
        map = floorPlanService.getServerMap( list );
        assertEquals( 1, map.size() );
    }
}
