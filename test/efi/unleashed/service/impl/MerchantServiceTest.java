package efi.unleashed.service.impl;

import efi.platform.dao.GenericDao;
import efi.unleashed.domain.Merchant;
import efi.unleashed.service.MerchantService;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class MerchantServiceTest {

    @Resource
    MerchantService merchantService;

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( merchantService.getDefaultDao() );
    }

    @Test
    public void testFindAllMerchants() throws Exception {
        List<Merchant> merchants = merchantService.loadAllEntities( Merchant.class );
        assertNotNull( merchants );
        assertFalse( merchants.isEmpty() );
    }

    @Test
    public void testUpdate() throws Exception {
        Merchant merchant = merchantService.loadAllEntities( Merchant.class ).get( 0 );
        DateTime previousDateTime = merchant.getLastUpdated();
        int previousVersion = merchant.getVersion();
        merchant.setName( "New name" );
        GenericDao dao = merchantService.getDefaultDao();
        dao.flushEntities();
        dao.evictEntity( merchant );
        merchant = merchantService.loadAllEntities( Merchant.class ).get( 0 );
        assertTrue( merchant.getLastUpdated().isAfter( previousDateTime ) );
        assertTrue( merchant.getVersion() > previousVersion );
    }
}
