package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.TableAttribute;
import efi.unleashed.security.UserContext;
import efi.unleashed.service.TableAttributeService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class TableAttributeServiceTest {

    @Resource
    TableAttributeService tableAttributeService;

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( tableAttributeService.getDefaultDao() );
    }
    
    @Test
    public void testFindSystemAttributes() throws Exception {
        List<TableAttribute> attributes = tableAttributeService.findSystemAttributes();
        assertNotNull( attributes );
        assertFalse( attributes.isEmpty() );
        assertEquals( UserContext.SYSTEM_MERCHANT_ID, attributes.get( 0 ).getMerchant().getId() );
    }
    
    @Test
    public void testFindForMerchant() throws Exception {
        List<TableAttribute> attributes = tableAttributeService.findByMerchantId( TestConstants.TEST_MERCHANT_ID );
        assertNotNull( attributes );
        assertFalse( attributes.isEmpty() );
        final String KNOWN_NAME = "Fireplace";
        List<String> names = new ArrayList<String>( attributes.size() );
        for ( TableAttribute attribute : attributes ) {
            names.add( attribute.getName() );
        }
        assertTrue( names.contains( KNOWN_NAME ) );
    }
}
