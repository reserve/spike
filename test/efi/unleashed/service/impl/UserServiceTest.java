package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.ApplicationUser;
import efi.unleashed.service.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class UserServiceTest {

    @Resource
    UserService userService;

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( userService.getDefaultDao() );
    }

    @Test
    public void testFindByUsername() throws Exception {
        ApplicationUser user = userService.findByUsername( TestConstants.TEST_USERNAME );
        assertNotNull( user );
        assertEquals( "Itha", user.getLastName() );

        user = userService.findByUsername( "__BOGUS__" );
        assertNull( user );
    }

    @Test
    public void testFindByUserId() throws Exception {
        ApplicationUser user = userService.findByUserId( TestConstants.TEST_USER_ID );
        assertNotNull( user );
        assertEquals( "1234", user.getPasscode() );

        user = userService.findByUserId( TestConstants.TEST_UNUSED_ID );
        assertNull( user );
    }
}
