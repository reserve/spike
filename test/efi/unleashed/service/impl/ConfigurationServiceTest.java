package efi.unleashed.service.impl;

import efi.platform.TestingUtils;
import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.FloorPlanItem;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.domain.WaitStationTemplate;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.ConfigurationService;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.DiningTableView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ConfigurationServiceTest {

    @Resource( name = "scopedTarget.requestUserContext" )
    RequestUserContext requestUserContext;

    @Resource
    ConfigurationService configurationService;

    @Resource
    ErrorMessageService errorMessageService;

    @Before
    public void setupUserContext() {
        requestUserContext.setUserId( TestConstants.TEST_USER_ID );
        requestUserContext.setMerchantId( TestConstants.TEST_MERCHANT_ID );
    }

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( configurationService.getDefaultDao() );
    }

    // Many methods are tested via the configuration workflow; these tests are to provide better coverage.

    @Test
    public void testUpdateConfigurationDispatch() throws Exception {
        ConfigurationUpdateView view = getUpdateViewShell();
        CloseServiceView badView = new CloseServiceView();
        view.getUpdates().add( badView );
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView context = new MessageContextView();
        configurationService.updateConfiguration( view, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Unknown update type: .CloseServiceView" ) );
    }

    @Test
    public void testFloorPlanUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        FloorPlanView view = new FloorPlanView();
        view.setId( TestConstants.TEST_UNUSED_ID );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan is not owned" ) );

        FloorPlan domainObject = configurationService.getEntity( FloorPlan.class,
                                                                 TestConstants.TEST_FLOOR_PLAN_ID );
        view = new FloorPlanView( domainObject );
        view.setName( "Updated" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testFloorPlanItemUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        FloorPlanItemView view = new FloorPlanItemView();
        view.setId( TestConstants.TEST_UNUSED_ID );
        view.setItemType( TestConstants.TEST_BAD_ENUM_VALUE );
        view.setGraphicType( TestConstants.TEST_BAD_ENUM_VALUE );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan item is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Floor plan item type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Graphic item type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );

        FloorPlanItem domainObject = configurationService.getEntity( FloorPlanItem.class,
                                                                     TestConstants.TEST_FLOOR_PLAN_GRAPHIC_ID );
        view = new FloorPlanItemView( domainObject );
        view.setName( "Updated" );
        view.setGraphicType( null );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "need a graphic item type" ) );
        view.setGraphicType( "BUSH3" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );

        domainObject = configurationService.getDefaultDao()
                .getEntity( FloorPlanItem.class, TestConstants.TEST_FLOOR_PLAN_LABEL_ID );
        view = new FloorPlanItemView( domainObject );
        view.setName( "Updated" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testSectionUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        SectionView view = new SectionView();
        view.setId( TestConstants.TEST_UNUSED_ID );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "section is not owned" ) );

        Section domainObject = configurationService.getEntity( Section.class, TestConstants.TEST_SECTION_ID );
        view = new SectionView( domainObject );
        view.setName( "Updated" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testServerUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        ServerView view = new ServerView();
        view.setId( TestConstants.TEST_UNUSED_ID );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "server is not owned" ) );

        Server domainObject = configurationService.getEntity( Server.class, TestConstants.TEST_SERVER_ID );
        view = new ServerView( domainObject );
        view.setAlias( "Updated" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testTableItemUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        FloorPlanItemView fpiView = new FloorPlanItemView();
        fpiView.setId( TestConstants.TEST_UNUSED_ID );
        fpiView.setItemType( TestConstants.TEST_BAD_ENUM_VALUE );
        fpiView.setGraphicType( TestConstants.TEST_BAD_ENUM_VALUE );
        DiningTableView dtView = new DiningTableView();
        dtView.setId( TestConstants.TEST_UNUSED_ID );
        dtView.setShape( TestConstants.TEST_BAD_ENUM_VALUE );
        dtView.setAttributeId( TestConstants.TEST_UNUSED_ID );
        TableItemView view = new TableItemView();
        view.setDiningTable( dtView );
        view.setFloorPlanItem( fpiView );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "dining table is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "table attribute is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Floor plan item type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Dining table type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );

        FloorPlanItem domainObject = configurationService.getEntity( FloorPlanItem.class,
                                                                     TestConstants.TEST_FLOOR_PLAN_ID );
        fpiView = new FloorPlanItemView( domainObject );
        fpiView.setName( "Updated" );
        dtView = new DiningTableView( domainObject.getTable() );
        dtView.setName( "Updated" );
        dtView.setAttributeId( TestConstants.TEST_TABLE_ATTRIBUTE_ID );
        view.setDiningTable( dtView );
        view.setFloorPlanItem( fpiView );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testWaitStationTemplateUpdate() throws Exception {
        ConfigurationUpdateView cuView = getUpdateViewShell();
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        WaitStationTemplateView view = new WaitStationTemplateView();
        view.setId( TestConstants.TEST_UNUSED_ID );
        cuView.getUpdates().add( view );
        configurationService.updateConfiguration( cuView, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "wait station template is not owned" ) );

        WaitStationTemplate domainObject = configurationService.getEntity( WaitStationTemplate.class,
                                                                           TestConstants.TEST_WAIT_STATION_TEMPLATE );
        view = new WaitStationTemplateView( domainObject );
        view.setName( "Updated" );
        cuView.getUpdates().clear();
        cuView.getUpdates().add( view );
        errors = new WorkflowErrors();
        configurationService.updateConfiguration( cuView, context, errors );
        assertFalse( errors.hasErrors() );
    }

    private ConfigurationUpdateView getUpdateViewShell() {
        ConfigurationUpdateView view = new ConfigurationUpdateView();
        List<ApiView> updates = new ArrayList<ApiView>();
        view.setUpdates( updates );
        return view;
    }
}
