package efi.unleashed.service.impl;

import efi.platform.TestingUtils;
import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.TableService;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.service.DayOfServiceService;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.ServiceUpdateView;
import efi.unleashed.view.api.TableServiceView;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class DayOfServiceServiceTest {

    @Resource( name = "scopedTarget.requestUserContext" )
    RequestUserContext requestUserContext;

    @Resource
    DayOfServiceService dayOfServiceService;

    @Resource
    ErrorMessageService errorMessageService;

    @Before
    public void setupUserContext() {
        requestUserContext.setUserId( TestConstants.TEST_USER_ID );
        requestUserContext.setMerchantId( TestConstants.TEST_MERCHANT_ID );
    }

    @Test
    public void testForCompleteness() throws Exception {
        assertNotNull( dayOfServiceService.getDefaultDao() );
    }

    // Many methods are tested via the service workflow; these tests are to provide better coverage.

    @Test
    public void testUpdateServiceDispatch() throws Exception {
        ServiceUpdateView view = new ServiceUpdateView();
        List<ApiView> updates = new ArrayList<ApiView>();
        CloseServiceView badView = new CloseServiceView();
        updates.add( badView );
        view.setUpdates( updates );
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView context = new MessageContextView();
        dayOfServiceService.updateService( view, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Unknown update type: .CloseServiceView" ) );
    }

    @Test
    public void testTableServiceUpdate() throws Exception {
        ServiceUpdateView view = new ServiceUpdateView();
        List<ApiView> updateViews = new ArrayList<ApiView>();
        view.setUpdates( updateViews );
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        TableServiceView tsView = new TableServiceView();
        tsView.setId( TestConstants.TEST_UNUSED_ID );
        tsView.setStatus( TestConstants.TEST_BAD_ENUM_VALUE );
        updateViews.add( tsView );
        dayOfServiceService.updateService( view, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Table service status type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "table service is not owned" ) );

        TableService service = dayOfServiceService.getEntity( TableService.class,
                                                              TestConstants.TEST_TABLE_SERVICE_ID );
        tsView = new TableServiceView( service );
        tsView.setTimeGreeted( DateTime.now() );
        tsView.setStatus( null );
        tsView.setTableIds( Arrays.asList( TestConstants.TEST_DINING_TABLE_ID ) );
        updateViews.clear();
        updateViews.add( tsView );
        errors = new WorkflowErrors();
        dayOfServiceService.updateService( view, context, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testDiningReservationUpdate() throws Exception {
        ServiceUpdateView view = new ServiceUpdateView();
        List<ApiView> updateViews = new ArrayList<ApiView>();
        view.setUpdates( updateViews );
        MessageContextView context = new MessageContextView();
        WorkflowErrors errors = new WorkflowErrors();

        DiningReservationView drView = new DiningReservationView();
        drView.setId( TestConstants.TEST_UNUSED_ID );
        drView.setStatus( TestConstants.TEST_BAD_ENUM_VALUE );
        updateViews.add( drView );
        dayOfServiceService.updateService( view, context, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Dining reservation status type " +
                                                                              TestConstants.TEST_BAD_ENUM_VALUE +
                                                                              " not found" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "dining reservation is not owned" ) );

        DiningReservation reso = dayOfServiceService.getEntity( DiningReservation.class,
                                                                TestConstants.TEST_DINING_RESERVATION_ID );
        drView = new DiningReservationView( reso );
        drView.setFirstName( "Bobby" );
        drView.setStatus( null );
        drView.setCancelled( false );
        drView.setNoShow( false );
        drView.setMadeToTable( false );
        drView.setRepeatGuest( false );
        drView.setSpecialOccasion( true );
        updateViews.clear();
        updateViews.add( drView );
        errors = new WorkflowErrors();
        dayOfServiceService.updateService( view, context, errors );
        assertFalse( errors.hasErrors() );
    }
}
