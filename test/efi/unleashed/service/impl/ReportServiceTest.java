package efi.unleashed.service.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportColumnField;
import efi.unleashed.domain.ReportType;
import efi.unleashed.service.ReportService;
import efi.unleashed.util.DateRangeIndicator;
import efi.unleashed.view.web.ReportEditView;
import efi.unleashed.workflow.ReportsWorkflow;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.MapBindingResult;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ReportServiceTest {

    @Resource
    ReportService reportService;

    @Resource
    ReportsWorkflow reportsWorkflow;

    @Test
    public void testFindReports() throws Exception {
        List<Report> reports = reportService.findReportsForMerchant( TestConstants.TEST_MERCHANT_ID );
        assertNotNull( reports );
        assertFalse( reports.isEmpty() );
    }

    @Test
    public void testFindReportsForType() throws Exception {
        List<Report> reports =
                reportService.findReportsForReportType( TestConstants.TEST_MERCHANT_ID, "Events and Functions" );
        assertNotNull( reports );
        assertFalse( reports.isEmpty() );
        for ( Report report : reports ) {
            Set<ReportColumnField> columnFields = report.getReportColumnFields();
            assertNotNull( columnFields );
            assertFalse( columnFields.isEmpty() );
        }
    }

    @Test
    public void testUpdateReport() throws Exception {
        ReportEditView view = reportsWorkflow.prepareToEditReport( TestConstants.TEST_EVENT_REPORT_ID );
        MapBindingResult result = new MapBindingResult( new HashMap(), "service" );
        view.setName( "Edited by Tester" );
        view.setDateRange( DateRangeIndicator.LAST_WEEK.getUserString() );
        List<String> ids = Arrays.asList( "col-100", "col-109" );
        view.setColumnIDs( ids );
        reportService.updateReport( view, result );
        assertFalse( result.hasErrors() );
        Report report = reportService.findReportForId( TestConstants.TEST_EVENT_REPORT_ID );
        assertTrue( report.getReportColumnFields().size() == 2 );

    }
}