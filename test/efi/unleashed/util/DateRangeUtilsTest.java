package efi.unleashed.util;

import efi.unleashed.domain.DateRange;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * This class is used to test the DateRangeUtils methods.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class DateRangeUtilsTest {

    @Test
    public void testDateRanges() throws Exception {
        LocalDate referenceDate = new LocalDate( 2012, 12, 1);

        LocalDate start = new LocalDate( 2012, 11, 25 );
        LocalDate end = new LocalDate( 2012, 12, 1 );

        DateRange dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.CURRENT_WEEK, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 12, 1 );
        end = new LocalDate( 2012, 12, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.CURRENT_MONTH, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 10, 1 );
        end = new LocalDate( 2012, 12, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.CURRENT_QUARTER, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 1, 1 );
        end = new LocalDate( 2012, 12, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.CURRENT_YEAR, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 11, 18 );
        end = new LocalDate( 2012, 11, 24 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.LAST_WEEK, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 11, 1 );
        end = new LocalDate( 2012, 11, 30 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.LAST_MONTH, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 7, 1 );
        end = new LocalDate( 2012, 9, 30 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.LAST_QUARTER, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2011, 1, 1 );
        end = new LocalDate( 2011, 12, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.LAST_YEAR, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 12, 2 );
        end = new LocalDate( 2012, 12, 8 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.NEXT_WEEK, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2013, 1, 1 );
        end = new LocalDate( 2013, 1, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.NEXT_MONTH, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2013, 1, 1 );
        end = new LocalDate( 2013, 3, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.NEXT_QUARTER, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2013, 1, 1 );
        end = new LocalDate( 2013, 12, 31 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.NEXT_YEAR, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 12, 1 );
        end = new LocalDate( 2012, 12, 1 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.MONTH_TO_DATE, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 10, 1 );
        end = new LocalDate( 2012, 12, 1 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.QUARTER_TO_DATE, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 1, 1 );
        end = new LocalDate( 2012, 12, 1 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.YEAR_TO_DATE, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 12, 1 );
        end = new LocalDate( 2012, 12, 1 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.TODAY, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 12, 2 );
        end = new LocalDate( 2012, 12, 2 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.TOMORROW, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );

        start = new LocalDate( 2012, 11, 30 );
        end = new LocalDate( 2012, 11, 30 );

        dr = DateRangeUtils.dateRangeForIndicator( DateRangeIndicator.YESTERDAY, referenceDate );
        assertEquals( start, dr.getStartDate() );
        assertEquals( end, dr.getEndDate() );
    }

}
