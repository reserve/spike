package efi.unleashed;

import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.domain.BinaryData;
import efi.unleashed.domain.BinaryMetadata;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.Merchant;
import efi.unleashed.domain.Site;
import efi.unleashed.service.MerchantService;
import efi.unleashed.view.api.PrepareForServiceView;
import efi.unleashed.view.api.ServerSectionView;
import efi.unleashed.workflow.ApiServiceWorkflow;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

/**
 * This class is used to load up some binary data for testing, since we can't do that with straight SQL.
 * Additional data is loaded to get around some shortcomings during development.
 */
@Component
public class TestDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    
    private static final Logger LOGGER = Logger.getLogger( TestDataLoader.class.getName() );

    @Resource
    MerchantService merchantService;

    @Resource
    ApiServiceWorkflow apiServiceWorkflow;

    @Override
    @Transactional
    public void onApplicationEvent( ContextRefreshedEvent event ) {
        if ( event.getApplicationContext().getParent() == null ) {
            loadFloorPlanData();
            loadMerchantLogoData();
            prepareForService();
        }
    }

    private void loadFloorPlanData() {
        InputStream stream = getClass().getResourceAsStream( "/floorplans/fp100.png" );
        if ( stream == null ) {
            throw new RuntimeException( "cannot find floorplan graphics" );
        }

        FloorPlan floorPlan = merchantService.getDefaultDao().getEntity( FloorPlan.class, 100L );
        BinaryData data = new BinaryData();
        BinaryMetadata metadata = new BinaryMetadata();
        try {
            data.setData( IOUtils.toByteArray( stream ) );
            metadata.setBinaryData( data );
            metadata.setName( "fp100.png" );
            metadata.setContentType( "image/png" );
            metadata.setSize( data.getData().length );
            metadata.setHeight( -1 );
            metadata.setWidth( -1 );
            floorPlan.setDiagram( metadata );
        }
        catch ( IOException ioe ) {
            throw new RuntimeException( "bad file load" );
        }
        merchantService.getDefaultDao().saveEntity( metadata );
    }

    private void loadMerchantLogoData() {
        InputStream stream = getClass().getResourceAsStream( "/merchants/frontierlogo.png" );
        if ( stream == null ) {
            throw new RuntimeException( "cannot find merchant logo graphics" );
        }

        Merchant merchant = merchantService.getDefaultDao().getEntity( Merchant.class, 100L );
        BinaryData data = new BinaryData();
        BinaryMetadata metadata = new BinaryMetadata();
        try {
            data.setData( IOUtils.toByteArray( stream ) );
            metadata.setBinaryData( data );
            metadata.setName( "frontierlogo.png" );
            metadata.setContentType( "image/png" );
            metadata.setSize( data.getData().length );
            metadata.setHeight( -1 );
            metadata.setWidth( -1 );
            merchant.setLogo( metadata );
        }
        catch ( IOException ioe ) {
            throw new RuntimeException( "bad file load" );
        }
        merchantService.getDefaultDao().saveEntity( metadata );
    }

    private void prepareForService() {
        Site site = merchantService.getDefaultDao().getEntity( Site.class, 100L );
        PrepareForServiceView view = new PrepareForServiceView();
        view.setAvailabilityMatrixId( 100L );
        view.setFloorPlanId( 100L );
        view.setServiceDate( LocalDate.now() );
        view.setWaitStationTemplateId( 100L );
        List<ServerSectionView> ssView = new ArrayList<ServerSectionView>();
        ssView.add( new ServerSectionView( 100L, 100L ) );
        ssView.add( new ServerSectionView( 101L, 101L ) );
        view.setScheduledServers( ssView );
        WorkflowErrors errors = new WorkflowErrors();

        // prepare for service 14 days out
        for ( int i = 1; i <= 14; i++ ) {
            apiServiceWorkflow.internalPrepareForService( view, site, 100L, errors );
            if ( errors.hasErrors() ) {
                throw new RuntimeException( "cannot prepare for service" );
            }
            view.setServiceDate( LocalDate.now().plusDays( i ) );
        }
    }
}
