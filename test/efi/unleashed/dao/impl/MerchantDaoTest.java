package efi.unleashed.dao.impl;

import efi.unleashed.dao.MerchantDao;
import efi.unleashed.domain.Merchant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class MerchantDaoTest {
    
    @Resource
    MerchantDao merchantDao;

    @Test
    public void testFindByShortName() throws Exception {
        final String SHORT_NAME = "Frontier";
        final String NAME = "Frontier Vineyards";
        Merchant merchant = merchantDao.findByShortName( SHORT_NAME );
        assertNotNull( merchant );
        assertEquals( NAME, merchant.getName() );
    }
}
