package efi.unleashed.dao.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.dao.DiningServiceDao;
import efi.unleashed.domain.DiningReservation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class DiningServiceDaoTest {

    @Resource
    DiningServiceDao diningServiceDao;

    @Test
    public void testFindByStatuses() throws Exception {
        final Long siteId = TestConstants.TEST_SITE_ID;
        final List<String> statuses = new ArrayList<String>();
        statuses.add( "ARRIVED" );
        statuses.add( "HOLD" );
        List<DiningReservation> reservations = diningServiceDao.findReservationsByStatus( siteId, statuses );
        assertNotNull( reservations );
        assertFalse( reservations.isEmpty() );
    }

    @Test
    public void testResoCount() throws Exception {
        final Long siteId = TestConstants.TEST_SITE_ID;
        Integer count = diningServiceDao.getReservationCount( siteId );
        assertNotNull( count );
        assertTrue( count > 10 );
    }
}
