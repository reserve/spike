package efi.unleashed.dao.impl;


import efi.unleashed.TestConstants;
import efi.unleashed.dao.EventDao;
import efi.unleashed.dao.ReportDao;
import efi.unleashed.domain.Event;
import efi.unleashed.domain.Report;
import efi.unleashed.domain.ReportFilterField;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class EventDaoTest {

    @Resource
    ReportDao reportDao;

    @Resource
    EventDao eventDao;

    @Test
    public void testEventsFilters() throws Exception {
        final Long reportId = TestConstants.TEST_EVENT_REPORT_ID;
        final Report report = reportDao.findReportForId( reportId );
        List <Event> events = eventDao.findEventsForReport( report );
        assertNotNull( events );
        assertFalse( events.isEmpty() );
    }
}
