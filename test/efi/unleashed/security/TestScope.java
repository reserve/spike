package efi.unleashed.security;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class is used to implement a map-based scope to hold beans in a particular scope.
 * This is useful for the user context, since it has various scopes, and lets unit tests get
 * access to it outside a web container.
 */
@Component
public class TestScope implements Scope {

    private final Map<String, Object> scopeMap = new ConcurrentHashMap<String, Object>();

    @Override
    public Object get( String name, ObjectFactory<?> objectFactory ) {
        Object object = scopeMap.get( name );
        if ( object == null ) {
            object = objectFactory.getObject();
            scopeMap.put( name, object );
        }
        return object;
    }

    @Override
    public Object remove( String name ) {
        return scopeMap.remove( name );
    }

    @Override
    public void registerDestructionCallback( String name, Runnable callback ) {
        // Nothing to do; optional and not required.
    }

    @Override
    public Object resolveContextualObject( String key ) {
        return null;  // Not used.
    }

    @Override
    public String getConversationId() {
        return null;  // Not used.
    }
}
