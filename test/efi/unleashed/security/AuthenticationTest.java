package efi.unleashed.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class AuthenticationTest {
    
    @Resource
    DaoAuthenticationProvider springAuthenticationProvider;
    
    @Test
    public void testAuthenticationUsingSpringSecurity() throws Exception {
        final UsernamePasswordAuthenticationToken loginToken =
                new UsernamePasswordAuthenticationToken( "rgreinke", "rgreinke" );
        Authentication authentication = springAuthenticationProvider.authenticate( loginToken );
        assertNotNull( authentication );
        assertTrue( authentication.isAuthenticated() );
    }

    @Test
    public void testUnknownUser() throws Exception {
        final UsernamePasswordAuthenticationToken loginToken =
                new UsernamePasswordAuthenticationToken( "__unknown__", "__unknown__" );
        try {
            springAuthenticationProvider.authenticate( loginToken );
            fail();
        }
        catch ( AuthenticationException e ) {
            // Pass
        }
    }
}
