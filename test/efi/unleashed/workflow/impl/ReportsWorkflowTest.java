package efi.unleashed.workflow.impl;

import efi.unleashed.TestConstants;
import efi.unleashed.security.SessionUserContext;
import efi.unleashed.security.TestSecurityContext;
import efi.unleashed.view.web.ReportEditView;
import efi.unleashed.view.web.ReportsView;
import efi.unleashed.workflow.ReportsWorkflow;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ReportsWorkflowTest {

    @Resource( name = "scopedTarget.sessionUserContext" )
    SessionUserContext sessionUserContext;

    @Resource
    ReportsWorkflow reportsWorkflow;

    @Before
    public void setupUserContext() {
        sessionUserContext.setUserId( TestConstants.TEST_USER_ID );
        sessionUserContext.setMerchantId( TestConstants.TEST_MERCHANT_ID );
        SecurityContextHolder.setContext( new TestSecurityContext() ); // Bypass security checks.
    }

    @Test
    public void testGetReports() throws Exception {
        final ReportsView view = reportsWorkflow.getReports();
        assertNotNull( view );
        assertEquals( 2, view.getReports().size() );
    }

    @Test
    public void testPrepareToEditReport() throws Exception {
        final Long reportId = TestConstants.TEST_EVENT_REPORT_ID;
        final ReportEditView view = reportsWorkflow.prepareToEditReport( reportId );
        assertNotNull( view );
        assertFalse( view.getAllRanges().isEmpty() );
        assertFalse( view.getAvailableFieldGroups().isEmpty() );
        for ( ReportEditView.AvailableFieldGroupView groupView : view.getAvailableFieldGroups() ){
            assertFalse( groupView.getAvailableFields().isEmpty() );
        }
        assertFalse( view.getColumns().isEmpty() );
        assertFalse( view.getFilters().isEmpty() );

        final String reportType = TestConstants.TEST_REPORT_TYPE;
        final ReportEditView view2 = reportsWorkflow.prepareToEditReport( reportType );
        assertNotNull( view2 );
        assertFalse( view2.getAvailableFieldGroups().isEmpty() );
        assertTrue( view2.getColumns().isEmpty() );
        assertTrue( view2.getFilters().isEmpty() );
    }
}
