package efi.unleashed.workflow.impl;

import efi.platform.TestingUtils;
import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.dao.SectionDao;
import efi.unleashed.domain.FloorPlan;
import efi.unleashed.domain.Section;
import efi.unleashed.domain.Server;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.security.TestSecurityContext;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.AssignSectionTableView;
import efi.unleashed.view.api.AssignedSectionTableView;
import efi.unleashed.view.api.BinaryDataView;
import efi.unleashed.view.api.ConfigurationUpdateView;
import efi.unleashed.view.api.DiningTableView;
import efi.unleashed.view.api.FloorPlanItemView;
import efi.unleashed.view.api.FloorPlanView;
import efi.unleashed.view.api.MerchantConfigurationView;
import efi.unleashed.view.api.MerchantView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.SectionTableView;
import efi.unleashed.view.api.SectionView;
import efi.unleashed.view.api.ServerView;
import efi.unleashed.view.api.TableItemView;
import efi.unleashed.view.api.WaitStationTemplateView;
import efi.unleashed.workflow.ApiConfigurationWorkflow;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ApiConfigurationWorkflowTest {

    @Resource( name = "scopedTarget.requestUserContext" )
    RequestUserContext requestUserContext;

    @Resource
    SectionDao sectionDao;

    @Resource
    ApiConfigurationWorkflow apiConfigurationWorkflow;

    @Resource
    ErrorMessageService errorMessageService;

    @Before
    public void setupUserContext() {
        requestUserContext.setUserId( TestConstants.TEST_USER_ID );
        requestUserContext.setMerchantId( TestConstants.TEST_MERCHANT_ID );
        SecurityContextHolder.setContext( new TestSecurityContext() ); // Bypass security checks.
    }

    @Test
    public void testGetConfiguration() throws Exception {
        final MerchantConfigurationView view = apiConfigurationWorkflow.getMerchantConfigurationView();
        assertNotNull( view );
        assertTrue( view.getMerchant().getName().contains( "Frontier" ) );
        assertTrue( view.getUser().getUsername().contains( "tabitha" ) );
        assertEquals( 1, view.getAuthorizedSites().size() );

        final MessageContextView context = view.getMessageContext();
        // Check the enumerations.
        assertTrue( context.getFloorPlanItemTypeMap().containsKey( "TABLE" ) );
        assertTrue( context.getTableShapeTypeMap().containsKey( "ROUND" ) );
        assertTrue( context.getReservationStatusTypeMap().containsKey( "PARTIALLY_ARRIVED" ) );
        assertTrue( context.getTableServiceStatusTypeMap().containsKey( "ENTREE_SERVED" ) );
        assertTrue( context.getGraphicItemTypeMap().containsKey( "BUSH2" ) );
        // Spot check the other maps.
        assertTrue( context.getTableAttributes().containsKey( 106L ) );
        assertTrue( context.getTables().size() > 0 );
        assertTrue( context.getAvailabilityMatrixMap().size() > 0 );
    }

    @Test
    public void testMerchantLogo() throws Exception {
        final MerchantConfigurationView view = apiConfigurationWorkflow.getMerchantConfigurationView();
        final MerchantView merchant = view.getMerchant();
        assertNotNull( merchant.getLogoUrl() );
        String[] chunks = merchant.getLogoUrl().split( "/" );
        Long logoId = Long.valueOf( chunks[chunks.length-1] );
        WorkflowErrors errors = new WorkflowErrors();
        final BinaryDataView binaryData = apiConfigurationWorkflow.getMerchantLogo( logoId, errors );
        assertNotNull( binaryData );
        assertTrue( binaryData.getSize() > 0 );
    }

    @Test
    public void testBadLogoId() throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.getMerchantLogo( TestConstants.TEST_UNUSED_ID, errors );
        assertTrue( errors.hasErrors() );
    }

    @Test
    public void testAddFloorPlan() throws Exception {
        FloorPlanView view = new FloorPlanView();
        view.setRequestId( "request-54321" );
        view.setName( "New Floor Plan" );
        view.setSiteId( TestConstants.TEST_SITE_ID );
        WorkflowErrors errors = new WorkflowErrors();
        FloorPlanView newView = apiConfigurationWorkflow.addFloorPlan( view, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        assertTrue( newView.getId() >= 10000L );
        assertEquals( newView.getRequestId(), "request-54321" );
    }

    @Test
    public void testNotOwnedAddFloorPlan() throws Exception {
        FloorPlanView view = new FloorPlanView();
        view.setName( "New Floor Plan" );
        view.setSiteId( TestConstants.TEST_UNUSED_ID );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addFloorPlan( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );
    }

    @Test
    public void testInvalidAddFloorPlan() throws Exception {
        FloorPlanView view = new FloorPlanView();
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addFloorPlan( view, errors );
        assertTrue( errors.hasErrors() );
        List<String> messages = errorMessageService.getDisplayableErrors( errors );
        assertEquals( 2, messages.size() );
        for ( String message : messages ) {
            assertTrue( message.contains( "may not be null" ) );
        }
    }

    @Test
    public void testFloorPlanDiagram() throws Exception {
        FloorPlan fp = sectionDao.getEntity( FloorPlan.class, TestConstants.TEST_FLOOR_PLAN_ID );
        FloorPlanView view = new FloorPlanView( fp );
        assertNotNull( view.getDiagramUrl() );
        String[] chunks = view.getDiagramUrl().split( "/" );
        Long diagramId = Long.valueOf( chunks[chunks.length - 1] );
        WorkflowErrors errors = new WorkflowErrors();
        final BinaryDataView binaryData = apiConfigurationWorkflow.getFloorPlanDiagram( diagramId, errors );
        assertNotNull( binaryData );
        assertTrue( binaryData.getSize() > 0 );
    }

    @Test
    public void testBadDiagramId() throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.getFloorPlanDiagram( TestConstants.TEST_UNUSED_ID, errors );
        assertTrue( errors.hasErrors() );
    }

    @Test
    public void testAddLabelFloorPlanItem() throws Exception {
        FloorPlanItemView view = getFloorPlanItemViewShell();
        view.setName( "New Label" );
        view.setItemType( "LABEL" );
        WorkflowErrors errors = new WorkflowErrors();
        FloorPlanItemView newView = apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        assertTrue( newView.getId() >= 10000L );
    }

    @Test
    public void testAddGraphicFloorPlanItem() throws Exception {
        FloorPlanItemView view = getFloorPlanItemViewShell();
        view.setName( "New Graphic" );
        view.setItemType( "GRAPHIC" );
        WorkflowErrors errors = new WorkflowErrors();
        FloorPlanItemView newView = apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertNull( newView );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "need a graphic item type" ) );
        view.setGraphicType( "BUSH2" );
        errors = new WorkflowErrors();
        newView = apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        assertTrue( newView.getId() >= 10000L );
    }

    @Test
    public void testNotOwnedAddFloorPlanItem() throws Exception {
        FloorPlanItemView view = getFloorPlanItemViewShell();
        view.setName( "New Label" );
        view.setItemType( "LABEL" );
        view.setFloorPlanId( TestConstants.TEST_UNUSED_ID );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan is not owned" ) );
    }

    @Test
    public void testBadEnumsForFloorPlanItem() throws Exception {
        FloorPlanItemView view = getFloorPlanItemViewShell();
        view.setItemType( TestConstants.TEST_BAD_ENUM_VALUE );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService,
                                                 "Floor plan item type " + TestConstants.TEST_BAD_ENUM_VALUE + " not found" ) );

        view.setItemType( "GRAPHIC" );
        view.setGraphicType( TestConstants.TEST_BAD_ENUM_VALUE );
        errors = new WorkflowErrors();
        apiConfigurationWorkflow.addFloorPlanItem( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService,
                                                 "Graphic item type " + TestConstants.TEST_BAD_ENUM_VALUE + " not found" ) );
    }

    // TODO do we need to test the validation annotations?

    private FloorPlanItemView getFloorPlanItemViewShell() {
        FloorPlanItemView view = new FloorPlanItemView();
        view.setName( "New Floor Plan Item" );
        view.setItemType( "LABEL" );
        view.setX( 10 );
        view.setY( 10 );
        view.setWidth( 10 );
        view.setHeight( 10 );
        view.setRotation( 90 );
        view.setFloorPlanId( TestConstants.TEST_FLOOR_PLAN_ID );
        return view;
    }

    private DiningTableView getDiningTableViewShell() {
        DiningTableView view = new DiningTableView();
        view.setName( "New Table" );
        view.setShape( "ROUND" );
        view.setCapacity( 4 );
        view.setFloorPlanId( TestConstants.TEST_FLOOR_PLAN_ID );
        return view;
    }

    @Test
    public void testAddDiningTable() throws Exception {
        FloorPlanItemView fpView = getFloorPlanItemViewShell();
        DiningTableView dtView = getDiningTableViewShell();
        TableItemView view = new TableItemView();
        view.setDiningTable( dtView );
        view.setFloorPlanItem( fpView );
        WorkflowErrors errors = new WorkflowErrors();
        TableItemView newView = apiConfigurationWorkflow.addDiningTable( view, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        Long tableId = newView.getDiningTable().getId();
        Long itemId = newView.getFloorPlanItem().getId();
        assertTrue( tableId >= 10000L );
        assertTrue( itemId >= 10000L );
        assertEquals( tableId, newView.getFloorPlanItem().getTableId() );
        assertEquals( 90, newView.getFloorPlanItem().getRotation().intValue() );

        dtView.setAttributeId( TestConstants.TEST_TABLE_ATTRIBUTE_ID );
        errors = new WorkflowErrors();
        apiConfigurationWorkflow.addDiningTable( view, errors );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testNotOwnedAddDiningTable() throws Exception {
        FloorPlanItemView fpView = getFloorPlanItemViewShell();
        fpView.setFloorPlanId( TestConstants.TEST_UNUSED_ID );
        DiningTableView dtView = getDiningTableViewShell();
        dtView.setFloorPlanId( TestConstants.TEST_UNUSED_ID );
        dtView.setAttributeId( TestConstants.TEST_UNUSED_ID );
        TableItemView view = new TableItemView();
        view.setDiningTable( dtView );
        view.setFloorPlanItem( fpView );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addDiningTable( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "table attribute is not owned" ) );
    }

    @Test
    public void testBadEnumForDiningTable() throws Exception {
        FloorPlanItemView fpView = getFloorPlanItemViewShell();
        DiningTableView dtView = getDiningTableViewShell();
        dtView.setShape( TestConstants.TEST_BAD_ENUM_VALUE );
        TableItemView view = new TableItemView();
        view.setDiningTable( dtView );
        view.setFloorPlanItem( fpView );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addDiningTable( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService,
                                                 "Dining table type " + TestConstants.TEST_BAD_ENUM_VALUE + " not found" ) );
    }

    @Test
    public void testNotOwnedNewTemplate() throws Exception {
        // The happy day scenario is already tested below as part of the assign section tables tests.
        WaitStationTemplateView templateView = new WaitStationTemplateView();
        templateView.setName( "Test Template" );
        templateView.setFloorPlanId( TestConstants.TEST_UNUSED_ID );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addWaitStationTemplate( templateView, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan is not owned" ) );
    }

    @Test
    public void testNotOwnedNewSection() throws Exception {
        // The happy day scenario is already tested below as part of the assign section tables tests.
        SectionView sectionView = new SectionView();
        sectionView.setName( "New Section" );
        sectionView.setBorderColor( "#FFEECC" );
        sectionView.setWaitStationTemplateId( TestConstants.TEST_UNUSED_ID );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addSection( sectionView, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "wait station template is not owned" ) );
    }

    @Test
    public void testAddNewServer() throws Exception {
        ServerView view = new ServerView();
        view.setFirstName( "First" );
        view.setLastName( "Last" );
        view.setAlias( "Alias" );
        view.setMerchantId( TestConstants.TEST_MERCHANT_ID );
        WorkflowErrors errors = new WorkflowErrors();
        ServerView newView = apiConfigurationWorkflow.addServer( view, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        assertTrue( newView.getId() >= 10000L );
    }

    @Test
    public void testNotOwnedNewServer() throws Exception {
        ServerView view = new ServerView();
        view.setFirstName( "First" );
        view.setLastName( "Last" );
        view.setAlias( "Alias" );
        view.setMerchantId( TestConstants.TEST_UNUSED_ID );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.addServer( view, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "submitted merchant does not match" ) );
    }

    @Test
    public void testAssignSectionTables() throws Exception {
        // Create a new wait station template.
        Long templateId = createWaitStationTemplate();

        // Add a sections to it.
        Long section1Id = createSection( "Section 1", templateId );

        // Assign some tables.
        AssignSectionTableView assignView = new AssignSectionTableView();
        assignView.setWaitStationTemplateId( templateId );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 100L ) );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 101L ) );
        WorkflowErrors errors = new WorkflowErrors();
        AssignedSectionTableView resultView = apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( resultView );
        assertEquals( 1, resultView.getAssignedSections().size() );
        Section section1 = sectionDao.getEntity( Section.class, section1Id );
        assertNotNull( section1 );
        assertEquals( 2, section1.getTables().size() );
        Collection<Long> section1TableIds = collectIds( section1.getTables() );
        assertTrue( section1TableIds.contains( 100L ) );
        assertTrue( section1TableIds.contains( 101L ) );

        // Assign different tables.
        assignView.getSectionTables().clear();
        assignView.getSectionTables().add( new SectionTableView( section1Id, 101L ) );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 102L ) );
        errors = new WorkflowErrors();
        resultView = apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( resultView );
        assertEquals( 1, resultView.getAssignedSections().size() );
        sectionDao.refreshEntity( section1 );
        assertNotNull( section1 );
        assertEquals( 2, section1.getTables().size() );
        section1TableIds = collectIds( section1.getTables() );
        assertFalse( section1TableIds.contains( 100L ) );
        assertTrue( section1TableIds.contains( 101L ) );
        assertTrue( section1TableIds.contains( 102L ) );
    }

    @Test
    public void testAssignWithMultipleSections() throws Exception {
        // Create a new wait station template.
        Long templateId = createWaitStationTemplate();

        // Add some sections to it.
        Long section1Id = createSection( "Section 1", templateId );
        Long section2Id = createSection( "Section 2", templateId );

        // Assign some tables.
        AssignSectionTableView assignView = new AssignSectionTableView();
        assignView.setWaitStationTemplateId( templateId );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 100L ) );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 101L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 102L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 103L ) );
        WorkflowErrors errors = new WorkflowErrors();
        AssignedSectionTableView resultView = apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( resultView );
        assertEquals( 2, resultView.getAssignedSections().size() );

        Section section1 = sectionDao.getEntity( Section.class, section1Id );
        assertNotNull( section1 );
        assertEquals( 2, section1.getTables().size() );
        Collection<Long> section1TableIds = collectIds( section1.getTables() );
        assertTrue( section1TableIds.contains( 100L ) );
        assertTrue( section1TableIds.contains( 101L ) );

        Section section2 = sectionDao.getEntity( Section.class, section2Id );
        assertNotNull( section2 );
        assertEquals( 2, section2.getTables().size() );
        Collection<Long> section2TableIds = collectIds( section2.getTables() );
        assertTrue( section2TableIds.contains( 102L ) );
        assertTrue( section2TableIds.contains( 103L ) );
    }

    @Test
    public void testAssignWithBadSection() throws Exception {
        Long templateId = createWaitStationTemplate();

        // Create sections in different templates.
        Long section1Id = createSection( "Section 1 in " + templateId, templateId );
        Long testTemplateId = TestConstants.TEST_WAIT_STATION_TEMPLATE;
        Long section2Id = createSection( "Section 2 in " + testTemplateId, testTemplateId );

        // Assign tables to both sections.
        AssignSectionTableView assignView = new AssignSectionTableView();
        assignView.setWaitStationTemplateId( templateId );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 100L ) );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 101L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 102L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 103L ) );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, section2Id + " is not a child" ) );
    }

    @Test
    public void testAssignWithMultipleTableMappings() throws Exception {
        Long templateId = createWaitStationTemplate();

        // Add some sections to it.
        Long section1Id = createSection( "Section 1", templateId );
        Long section2Id = createSection( "Section 2", templateId );

        // Assign the same table to more than one section.
        AssignSectionTableView assignView = new AssignSectionTableView();
        assignView.setWaitStationTemplateId( templateId );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 100L ) );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 101L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 101L ) );
        assignView.getSectionTables().add( new SectionTableView( section2Id, 102L ) );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "101 appears in more than one" ) );
    }

    @Test
    public void testNotOwnedTemplateForAssign() throws Exception {
        Long templateId = createWaitStationTemplate();
        Long section1Id = createSection( "Section 1", templateId );
        AssignSectionTableView assignView = new AssignSectionTableView();
        // Use a bad template ID.
        assignView.setWaitStationTemplateId( TestConstants.TEST_UNUSED_ID );
        assignView.getSectionTables().add( new SectionTableView( section1Id, 100L ) );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.assignSectionTables( assignView, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "wait station template is not owned" ) );
    }

    private Long createWaitStationTemplate() {
        WaitStationTemplateView templateView = new WaitStationTemplateView();
        templateView.setName( "Test Template" );
        templateView.setFloorPlanId( TestConstants.TEST_FLOOR_PLAN_ID );
        WorkflowErrors errors = new WorkflowErrors();
        templateView = apiConfigurationWorkflow.addWaitStationTemplate( templateView, errors );
        assertNotNull( templateView );
        return templateView.getId();
    }

    private Long createSection( String name, Long templateId ) {
        SectionView sectionView = new SectionView();
        sectionView.setName( name );
        sectionView.setBorderColor( "#FFEECC" );
        sectionView.setWaitStationTemplateId( templateId );
        WorkflowErrors errors = new WorkflowErrors();
        sectionView = apiConfigurationWorkflow.addSection( sectionView, errors );
        assertNotNull( sectionView );
        return sectionView.getId();
    }

    @SuppressWarnings( "unchecked" )
    private List<Long> collectIds( Collection collection ) {
        return (List<Long>) CollectionUtils.collect( collection, new BeanToPropertyValueTransformer( "id" ) );
    }

    @Test
    public void testConfigurationUpdate() throws Exception {
        ConfigurationUpdateView view = new ConfigurationUpdateView();
        List<ApiView> updateViews = new ArrayList<ApiView>();

        FloorPlanView floorPlanView = new FloorPlanView();
        floorPlanView.setId( TestConstants.TEST_FLOOR_PLAN_ID );
        floorPlanView.setName( "Updated Floor Plan Name" );
        floorPlanView.setSiteId( TestConstants.TEST_SITE_ID );
        updateViews.add( floorPlanView );

        ServerView serverView = new ServerView();
        serverView.setId( TestConstants.TEST_SERVER_ID );
        serverView.setAlias( "Updated Alias" );
        serverView.setFirstName( "First" );
        serverView.setLastName( "Last" );
        serverView.setMerchantId( TestConstants.TEST_MERCHANT_ID );
        updateViews.add( serverView );

        view.setUpdates( updateViews );
        WorkflowErrors errors = new WorkflowErrors();
        apiConfigurationWorkflow.update( view, errors );
        assertFalse( errors.hasErrors() );

        FloorPlan floorPlan = sectionDao.getEntity( FloorPlan.class, TestConstants.TEST_FLOOR_PLAN_ID );
        assertNotNull( floorPlan );
        assertTrue( floorPlan.getName().contains( "Updated" ) );

        Server server = sectionDao.getEntity( Server.class, TestConstants.TEST_SERVER_ID );
        assertNotNull( server );
        assertTrue( server.getAlias().contains( "Updated" ) );
    }
}
