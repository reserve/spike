package efi.unleashed.workflow.impl;

import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.view.api.ApplicationUserView;
import efi.unleashed.workflow.ApiUserWorkflow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ApiUserWorkflowTest {

    @Resource( name = "scopedTarget.requestUserContext" )
    RequestUserContext requestUserContext;
    
    @Resource
    ApiUserWorkflow userWorkflow;
    
    @Test
    public void testFoundUser() throws Exception {
        requestUserContext.setUserId( TestConstants.TEST_USER_ID );
        WorkflowErrors errors = new WorkflowErrors();
        ApplicationUserView view = userWorkflow.checkUser( errors );
        assertNotNull( view );
        assertEquals( "1234", view.getPasscode() );
    }

    @Test
    public void testNotFoundUser() {
        requestUserContext.setUserId( 99L );
        WorkflowErrors errors = new WorkflowErrors();
        ApplicationUserView view = userWorkflow.checkUser( errors );
        assertNull( view );
        assertTrue( errors.getErrorCount() > 0 );
    }
}
