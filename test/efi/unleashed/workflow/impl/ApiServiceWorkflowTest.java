package efi.unleashed.workflow.impl;

import efi.platform.TestingUtils;
import efi.platform.service.ErrorMessageService;
import efi.platform.web.controller.WorkflowErrors;
import efi.unleashed.TestConstants;
import efi.unleashed.dao.DiningServiceDao;
import efi.unleashed.domain.DiningReservation;
import efi.unleashed.domain.DiningReservationStatusType;
import efi.unleashed.domain.TableService;
import efi.unleashed.domain.TableServiceStatusType;
import efi.unleashed.security.RequestUserContext;
import efi.unleashed.security.TestSecurityContext;
import efi.unleashed.view.api.ApiView;
import efi.unleashed.view.api.CloseServiceView;
import efi.unleashed.view.api.DiningReservationView;
import efi.unleashed.view.api.MessageContextView;
import efi.unleashed.view.api.PrepareForServiceView;
import efi.unleashed.view.api.ServerSectionView;
import efi.unleashed.view.api.ServiceUpdateView;
import efi.unleashed.view.api.TableServiceView;
import efi.unleashed.workflow.ApiServiceWorkflow;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class ApiServiceWorkflowTest {

    @Resource( name = "scopedTarget.requestUserContext" )
    RequestUserContext requestUserContext;

    @Resource
    ApiServiceWorkflow apiServiceWorkflow;

    @Resource
    DiningServiceDao diningServiceDao;

    @Resource
    ErrorMessageService errorMessageService;

    @Before
    public void setupUserContext() {
        requestUserContext.setUserId( TestConstants.TEST_USER_ID );
        requestUserContext.setMerchantId( TestConstants.TEST_MERCHANT_ID );
        SecurityContextHolder.setContext( new TestSecurityContext() ); // Bypass security checks.
    }

    @Test
    public void testCheckForDiningService() throws Exception {
        // Test data has today already prepared.
        WorkflowErrors errors = new WorkflowErrors();
        final Long siteId = TestConstants.TEST_SITE_ID;
        LocalDate date = LocalDate.now();
        PrepareForServiceView view = apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        assertNotNull( view );
        assertTrue( view.getInService() );

        // A year from now should not be prepared.
        date = LocalDate.now().plusYears( 1 );
        errors = new WorkflowErrors();
        view = apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        assertNull( view );
    }

    @Test
    public void testNotOwnedDuringCheck() throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        Long siteId = TestConstants.TEST_UNUSED_ID;
        LocalDate date = LocalDate.now();
        apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );
    }

    @Test
    public void testPrepareForService() throws Exception {
        PrepareForServiceView view = getPrepareForServiceViewShell();
        // Go out a month so as to not conflict with the test data.
        view.setServiceDate( LocalDate.now().plusMonths( 1 ) );
        final Long siteId = TestConstants.TEST_SITE_ID;
        WorkflowErrors errors = new WorkflowErrors();
        PrepareForServiceView newView = apiServiceWorkflow.prepareForService( view, siteId, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        Long newId = newView.getId();
        assertTrue( newId >= 10000L );
        assertEquals( 2, newView.getScheduledServers().size() );

        // Re-prepare, changing the server-table mapping.
        view.getScheduledServers().clear();
        view.getScheduledServers().add( new ServerSectionView( 100L, 100L ) );
        errors = new WorkflowErrors();
        newView = apiServiceWorkflow.prepareForService( view, siteId, errors );
        assertFalse( errors.hasErrors() );
        assertEquals( newId, newView.getId() );
        assertEquals( 1, newView.getScheduledServers().size() );
    }

    @Test
    public void testNotOwnedDuringPrepare() throws Exception {
        PrepareForServiceView view = getPrepareForServiceViewShell();
        // Go out a month so as to not conflict with the test data.
        view.setServiceDate( LocalDate.now().plusMonths( 1 ) );
        Long siteId = TestConstants.TEST_UNUSED_ID;
        WorkflowErrors errors = new WorkflowErrors();
        apiServiceWorkflow.prepareForService( view, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );

        siteId = TestConstants.TEST_SITE_ID;
        view.setAvailabilityMatrixId( TestConstants.TEST_UNUSED_ID );
        view.setFloorPlanId( TestConstants.TEST_UNUSED_ID );
        view.setWaitStationTemplateId( TestConstants.TEST_UNUSED_ID );
        errors = new WorkflowErrors();
        apiServiceWorkflow.prepareForService( view, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "availability matrix is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "floor plan is not owned" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "wait station template is not owned" ) );
    }

    private PrepareForServiceView getPrepareForServiceViewShell() {
        PrepareForServiceView view = new PrepareForServiceView();
        view.setAvailabilityMatrixId( TestConstants.TEST_AVAILABILITY_MATRIX_ID );
        view.setFloorPlanId( TestConstants.TEST_FLOOR_PLAN_ID );
        view.setServiceDate( LocalDate.now() );
        view.setWaitStationTemplateId( TestConstants.TEST_WAIT_STATION_TEMPLATE );
        List<ServerSectionView> ssView = new ArrayList<ServerSectionView>();
        ssView.add( new ServerSectionView( 100L, 100L ) );
        ssView.add( new ServerSectionView( 101L, 101L ) );
        view.setScheduledServers( ssView );
        return view;
    }

    @Test
    public void testStartService() throws Exception {
        // TODO This may change after the NRA show.
        final Long siteId = TestConstants.TEST_SITE_ID;
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.startService( siteId, errors );
        assertFalse( errors.hasErrors() );
        assertTrue( view.getTableServiceMap().size() > 0 );
    }

    @Test
    public void testNotOwnedDuringStart() throws Exception {
        final Long siteId = TestConstants.TEST_UNUSED_ID;
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.startService( siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );
    }

    @Test
    public void testGetServiceUpdates() throws Exception {
        final Long siteId = TestConstants.TEST_SITE_ID;
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.startService( siteId, errors );
        assertFalse( errors.hasErrors() );
        assertTrue( view.getTableServiceMap().size() > 0 );

        errors = new WorkflowErrors();
        DateTime dateTime = DateTime.now();
        view = apiServiceWorkflow.getServiceUpdates( siteId, dateTime, errors );
        assertFalse( errors.hasErrors() );
        assertTrue( view.getTableServiceMap().size() == 0 );  // Nothing new.

        DiningReservationView drView = getDiningReservationViewShell();
        errors = new WorkflowErrors();
        view = apiServiceWorkflow.addToWaitList( drView, siteId, errors );
        assertFalse( errors.hasErrors() );
        assertTrue( view.getTableServiceMap().size() > 0 );  // Our new reservation.

        errors = new WorkflowErrors();
        view = apiServiceWorkflow.getServiceUpdates( siteId, dateTime, errors );
        assertFalse( errors.hasErrors() );
        assertTrue( view.getTableServiceMap().size() > 0 );  // Should include our new reservation.
    }

    @Test
    public void testNotOwnedDuringGetUpdates() throws Exception {
        final Long siteId = TestConstants.TEST_UNUSED_ID;
        DateTime dateTime = DateTime.now();
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.getServiceUpdates( siteId, dateTime, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );
    }

    @Test
    public void testMoreAddToWaitList() throws Exception {
        final Long siteId = TestConstants.TEST_SITE_ID;
        DiningReservationView drView = getDiningReservationViewShell();
        drView.setStatus( "WALK_IN_OVERFLOW" );
        drView.setSpecialOccasion( false );
        WorkflowErrors errors = new WorkflowErrors();
        MessageContextView view = apiServiceWorkflow.addToWaitList( drView, siteId, errors );
        DiningReservationView resultView = view.getDiningReservations().entrySet().iterator().next().getValue();
        assertEquals( resultView.getRequestId(), "request-12345" );
        assertFalse( errors.hasErrors() );
    }

    @Test
    public void testBadDataDuringAddToWaitList() throws Exception {
        Long siteId = TestConstants.TEST_UNUSED_ID;
        DiningReservationView view = getDiningReservationViewShell();
        WorkflowErrors errors = new WorkflowErrors();
        apiServiceWorkflow.addToWaitList( view, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );

        siteId = TestConstants.TEST_SITE_ID;
        view.setReservationTime( "Bogus" );
        view.setStatus( TestConstants.TEST_BAD_ENUM_VALUE );
        view.setSpecialOccasion( true );
        errors = new WorkflowErrors();
        apiServiceWorkflow.addToWaitList( view, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "Failed to parse a reservation time" ) );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService,
                                                 "Dining reservation status type " +
                                                 TestConstants.TEST_BAD_ENUM_VALUE +
                                                 " not found" ) );
    }

    private DiningReservationView getDiningReservationViewShell() {
        DiningReservationView view = new DiningReservationView();
        view.setRequestId( "request-12345" );
        view.setLastName( "New reservation" );
        view.setPartySize( 4 );
        view.setReservationDate( LocalDate.now() );
        view.setReservationTime( "18:00" );
        return view;
    }

    @Test
    public void testCloseService() throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        final Long siteId = TestConstants.TEST_SITE_ID;
        LocalDate date = LocalDate.now();
        PrepareForServiceView view = apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        assertNotNull( view );
        Long serviceId = view.getId();
        CloseServiceView closeView = new CloseServiceView();
        closeView.setDiningServiceId( serviceId );
        PrepareForServiceView newView = apiServiceWorkflow.close( closeView, siteId, errors );
        assertFalse( errors.hasErrors() );
        assertNotNull( newView );
        assertFalse( newView.getInService() );

        // Try again.
        errors = new WorkflowErrors();
        newView = apiServiceWorkflow.close( closeView, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "dining service is already closed" ) );

        // Try with bogus ID.
        closeView.setDiningServiceId( TestConstants.TEST_UNUSED_ID );
        errors = new WorkflowErrors();
        newView = apiServiceWorkflow.close( closeView, siteId, errors );
        assertNull( newView );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "record was not found" ) );
    }

    @Test
    public void testNotOwnedDuringClose() throws Exception {
        WorkflowErrors errors = new WorkflowErrors();
        Long siteId = TestConstants.TEST_SITE_ID;
        LocalDate date = LocalDate.now();
        PrepareForServiceView view = apiServiceWorkflow.checkForDiningService( siteId, date, errors );
        assertNotNull( view );
        Long serviceId = view.getId();
        CloseServiceView closeView = new CloseServiceView();
        closeView.setDiningServiceId( serviceId );
        siteId = TestConstants.TEST_UNUSED_ID;
        PrepareForServiceView newView = apiServiceWorkflow.close( closeView, siteId, errors );
        assertTrue( TestingUtils.errorsContains( errors, errorMessageService, "site is not owned" ) );
    }

    @Test
    public void testServiceUpdate() throws Exception {
        ServiceUpdateView view = new ServiceUpdateView();
        List<ApiView> updateViews = new ArrayList<ApiView>();

        DiningReservation reso =
                diningServiceDao.findDiningReservationForMerchant( TestConstants.TEST_DINING_RESERVATION_ID,
                                                                   TestConstants.TEST_MERCHANT_ID );
        DiningReservationView drView = new DiningReservationView( reso );
        drView.setFirstName( "Bobby" );
        drView.setStatus( "PARTIALLY_SEATED" );
        updateViews.add( drView );

        TableService service =
                diningServiceDao.findTableServiceForMerchant( TestConstants.TEST_TABLE_SERVICE_ID,
                                                              TestConstants.TEST_MERCHANT_ID );
        TableServiceView tsView = new TableServiceView( service );
        tsView.setTimeGreeted( DateTime.now() );
        tsView.setStatus( "TABLE_GREETED" );
        updateViews.add( tsView );

        view.setUpdates( updateViews );
        WorkflowErrors errors = new WorkflowErrors();
        apiServiceWorkflow.update( view, errors );
        assertFalse( errors.hasErrors() );

        reso = diningServiceDao.getEntity( DiningReservation.class, TestConstants.TEST_DINING_RESERVATION_ID );
        assertNotNull( reso );
        assertTrue( reso.getFirstName().contains( "Bobby" ) );
        assertEquals( DiningReservationStatusType.PARTIALLY_SEATED, reso.getStatus() );

        service = diningServiceDao.getEntity( TableService.class, TestConstants.TEST_TABLE_SERVICE_ID );
        assertNotNull( service );
        assertNotNull( service.getTimeGreeted() );
        assertEquals( TableServiceStatusType.TABLE_GREETED, service.getStatus() );
    }
}
