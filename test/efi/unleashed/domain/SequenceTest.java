package efi.unleashed.domain;

import efi.unleashed.dao.MerchantDao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "/spring-context-testing.xml" } )
@TransactionConfiguration( defaultRollback = true )
@Transactional
public class SequenceTest {

    @Resource
    MerchantDao merchantDao;
    
    @Test
    public void testIdFromSequence() throws Exception {
        Merchant merchant = Merchant.newInstance();
        merchant.setName( "New Merchant" );
        merchant.setShortName( "New" );
        merchantDao.saveEntity( merchant );
        merchantDao.refreshEntity( merchant );
        assertTrue( merchant.getId() >= 10000L );
    }
}
