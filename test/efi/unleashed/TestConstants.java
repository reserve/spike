package efi.unleashed;

/**
 * This class contains constants used in many of the unit tests.
 */
public class TestConstants {

    public static final Long TEST_USER_ID = 101L;
    public static final Long TEST_MERCHANT_ID = 100L;
    public static final Long TEST_SITE_ID = 100L;
    public static final Long TEST_FLOOR_PLAN_ID = 100L;
    public static final Long TEST_FLOOR_PLAN_GRAPHIC_ID = 200L;
    public static final Long TEST_FLOOR_PLAN_LABEL_ID = 300L;
    public static final Long TEST_WAIT_STATION_TEMPLATE = 100L;
    public static final Long TEST_SERVER_ID = 100L;
    public static final Long TEST_SECTION_ID = 100L;
    public static final Long TEST_DINING_TABLE_ID = 100L;
    public static final Long TEST_TABLE_ATTRIBUTE_ID = 100L;
    public static final Long TEST_AVAILABILITY_MATRIX_ID = 100L;
    public static final Long TEST_DINING_RESERVATION_ID = 100L;
    public static final Long TEST_TABLE_SERVICE_ID = 101L;

    public static final Long TEST_UNUSED_ID = 500L;

    public static final String TEST_BAD_ENUM_VALUE = "BAD_BAD_TYPE";

    public static final String TEST_USERNAME = "tabitha";

    public static final Long TEST_EVENT_REPORT_ID = 1L;
    public static final Long TEST_FUNCTION_REPORT_ID = 2L;

    public static final String TEST_REPORT_TYPE = "Events";
}
