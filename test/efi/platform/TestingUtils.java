package efi.platform;

import efi.platform.service.ErrorMessageService;

import org.springframework.validation.Errors;

import java.util.List;

/**
 * This class provides common helper methods for unit testing.
 */
public class TestingUtils {

    private TestingUtils() { } // only static access

    public static boolean errorsContains( Errors errors, ErrorMessageService service, String messageFragment ) {
        if ( !errors.hasErrors() ) {
            return false;
        }
        List<String> messages = service.getDisplayableErrors( errors );
        boolean foundMessage = false;
        for ( String message : messages ) {
            if ( message.contains( messageFragment ) ) {
                foundMessage = true;
                break;
            }
        }
        return foundMessage;
    }
}
